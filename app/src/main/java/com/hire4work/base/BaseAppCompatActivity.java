package com.hire4work.base;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.google.android.gms.maps.model.LatLng;
import com.hire4work.R;
import com.hire4work.activity.LoginActivity;
import com.hire4work.bean.appenum.LoginWay;
import com.hire4work.customewidgets.ExpandableHeightListView;
import com.hire4work.customewidgets.TextDrawable;
import com.hire4work.fragment.EditProfileFragment;
import com.hire4work.fragment.FindJobFragment;
import com.hire4work.fragment.InvitesFragment;
import com.hire4work.fragment.JobDescriptionFragment;
import com.hire4work.fragment.MembershipFragment;
import com.hire4work.fragment.MyJobFragment;
import com.hire4work.fragment.NewsFragment;
import com.hire4work.fragment.PostJobFragment;
import com.hire4work.fragment.employer.EmployerDetailFragment;
import com.hire4work.fragment.employer.EmployerHomeFragment;
import com.hire4work.fragment.employer.EmployerInboxFragment;
import com.hire4work.fragment.employer.WorkerFragment;
import com.hire4work.fragment.worker.WorkerHomeFragment;
import com.hire4work.sliding.SlidingMenu;
import com.hire4work.utills.Constant;
import com.hire4work.utills.Debug;
import com.hire4work.utills.GPSTracker;
import com.hire4work.utills.Keys;
import com.hire4work.utills.SharedPrefrenceManager;
import com.hire4work.utills.TextUtils;
import com.hire4work.utills.Utils;
import com.parse.ParseUser;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import static com.hire4work.utills.UserConstant.PROFESSION;

/**
 * Created by Kailash Chouhan (Ratufa Technologies)
 * Email: kailash.chouhan@ratufa.com
 */
public class BaseAppCompatActivity extends BaseActivity {

    private ProgressDialog progressDialog;
    private SharedPrefrenceManager sharedPrefrenceManager = SharedPrefrenceManager.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!Utils.isInternetConnected(BaseAppCompatActivity.this)) {
            showToast(getString(R.string.internet));
        }

        progressDialog = new ProgressDialog(BaseAppCompatActivity.this, R.style.MyTheme);
        progressDialog.setIndeterminate(true);
        progressDialog.setIndeterminateDrawable(getResources().getDrawable(R.drawable.progress_style));
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        progressDialog.setCancelable(false);
    }

    public void showLoading(String message) {
        if (!TextUtils.isNullOrEmpty(message)) {
            progressDialog.setMessage(message);
        }
        progressDialog.show();
    }

    public void showLoading() {
        this.showLoading(null);
    }

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(R.layout.activity_base);
        ViewGroup contentView = (ViewGroup) findViewById(R.id.content_view);
        contentView.addView(getLayoutInflater().inflate(layoutResID, null));
    }

    public void showToast(String message) {
        Utils.showDataToast(message, BaseAppCompatActivity.this);
    }

    public void hideLoading() {
        progressDialog.dismiss();
    }

    public void hideKeyBoard(View v) {
        Utils.hideKeyBoard(BaseAppCompatActivity.this, v);
    }

    public Boolean isInternetConnected() {
        return Utils.isInternetConnected(BaseAppCompatActivity.this);
    }

    public void showAlertDialog(String message) {
        showAlertDialog(null, message);
    }

    public void showAlertDialog(String title, String message) {
        Utils.showAlertDialog(BaseAppCompatActivity.this, title, message);
    }

    public boolean isEmployer() {
        boolean b = false;
        ParseUser parseUser = ParseUser.getCurrentUser();
        List<String> stringCategoryList = parseUser.getList(PROFESSION);
        for (int i = 0; i < stringCategoryList.size(); i++) {
            if (stringCategoryList.get(i).equalsIgnoreCase(getString(R.string.employer))) {
                b = true;
                break;
            } else {
                b = false;
            }
        }
        return b;
    }

    public boolean isGuestEmployer() {
        boolean b = false;
        ArrayList<String> list = sharedPrefrenceManager.getSavedPrefrenceList(getActivity(), new ArrayList<String>(), "CATEGORY");
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).equalsIgnoreCase(getString(R.string.employer))) {
                b = true;
                break;
            } else {
                b = false;
            }
        }
        return b;
    }

    public void displayView(int position, SlidingMenu menu, ExpandableHeightListView mDrawerList, Boolean isBackstack) {
        // update the main content by replacing fragments

        Fragment fragment = null;
        switch (position) {
            case 0:
                fragment = new NewsFragment();
                break;
            case 1:
                if (sharedPrefrenceManager.getSavedPrefrenceValue(BaseAppCompatActivity.this, "", Keys.IS_LOGIN).equalsIgnoreCase("true")) {
                    fragment = new EditProfileFragment();
                } else {
                    showLoginAlert();
                }
                break;
            case 2:
                if (sharedPrefrenceManager.getSavedPrefrenceValue(BaseAppCompatActivity.this, "", Keys.IS_LOGIN).equalsIgnoreCase("true")) {
                    if (isEmployer()) {
                        showAlertDialog("Message", "Please update your category from the Edit profile menu to find jobs.");
                    } else {
                        fragment = new FindJobFragment();
                    }
                } else {
                    if (isGuestEmployer()) {
                        showAlertDialog("Message", "Only worker can find job.");
                    } else {
                        fragment = new FindJobFragment();
                    }
                }
                break;
            case 3:
                if (sharedPrefrenceManager.getSavedPrefrenceValue(BaseAppCompatActivity.this, "", Keys.IS_LOGIN).equalsIgnoreCase("true")) {
                    if (isEmployer()) {
                        showAlertDialog("Message", "Please update your category from Edit profile menu to see Job invitations");
                    } else {
                        fragment = new InvitesFragment();
                    }
                } else {
                    showLoginAlert();
                }
                break;
            case 4:
                if (sharedPrefrenceManager.getSavedPrefrenceValue(BaseAppCompatActivity.this, "", Keys.IS_LOGIN).equalsIgnoreCase("true")) {
                    if (isEmployer()) {
                        fragment = new PostJobFragment();
                    } else {
                        showAlertDialog("Message", "Please update Employer category from Edit profile menu to Post jobs.");
                    }
                } else {
                    if (isGuestEmployer()) {
                        fragment = new PostJobFragment();
                    } else {
//                        showAlertDialog("Message", "Employer can post job.");
                        showLoginAlert();
                    }
                }
                break;
            case 5:
                if (sharedPrefrenceManager.getSavedPrefrenceValue(BaseAppCompatActivity.this, "", Keys.IS_LOGIN).equalsIgnoreCase("true")) {
                    fragment = new MyJobFragment();
                } else {
                    showLoginAlert();
                }
                break;
            case 6:
                if (sharedPrefrenceManager.getSavedPrefrenceValue(BaseAppCompatActivity.this, "", Keys.IS_LOGIN).equalsIgnoreCase("true")) {
                    fragment = new MembershipFragment();
                } else {
                    showLoginAlert();
                }
                break;
            case 7:
                if (sharedPrefrenceManager.getSavedPrefrenceValue(BaseAppCompatActivity.this, "", Keys.IS_LOGIN).equalsIgnoreCase("true")) {
                    fragment = new JobDescriptionFragment();
                } else {
                    showLoginAlert();
                }
                break;
            case 8:
                if (sharedPrefrenceManager.getSavedPrefrenceValue(BaseAppCompatActivity.this, "", Keys.IS_LOGIN).equalsIgnoreCase("true")) {
                    fragment = new WorkerHomeFragment();
                } else {
                    showLoginAlert();
                }
                break;
            case 9:
                if (sharedPrefrenceManager.getSavedPrefrenceValue(BaseAppCompatActivity.this, "", Keys.IS_LOGIN).equalsIgnoreCase("true")) {
                    fragment = new EmployerHomeFragment();
                } else {
                    showLoginAlert();
                }
                break;
            case 10:
                if (sharedPrefrenceManager.getSavedPrefrenceValue(BaseAppCompatActivity.this, "", Keys.IS_LOGIN).equalsIgnoreCase("true")) {
                    fragment = new EmployerDetailFragment();
                } else {
                    showLoginAlert();
                }
                break;
            case 12:
                if (sharedPrefrenceManager.getSavedPrefrenceValue(BaseAppCompatActivity.this, "", Keys.IS_LOGIN).equalsIgnoreCase("true")) {
                    fragment = new WorkerFragment();
                } else {
                    showLoginAlert();
                }
                break;
            case 13:
                if (sharedPrefrenceManager.getSavedPrefrenceValue(BaseAppCompatActivity.this, "", Keys.IS_LOGIN).equalsIgnoreCase("true")) {
                    fragment = new EmployerInboxFragment();
                } else {
                    showLoginAlert();
                }
            default:
                break;
        }

        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            if (isBackstack) {
                fragmentManager.beginTransaction().replace(R.id.main, fragment).addToBackStack(null).commit();
            } else {
                fragmentManager.beginTransaction().replace(R.id.main, fragment).commit();
            }
            // update selected item and title, then close the drawer
            if (menu != null && menu.isMenuShowing()) {
                menu.toggle();
            }

            if (mDrawerList != null) {
                mDrawerList.setItemChecked(position, true);
                mDrawerList.setSelection(position);
            }
        } else {
            // error in creating fragment
            Debug.e("MainActivity", "Error in creating fragment");
        }
    }

    public LatLng getMyLocation() {
        GPSTracker gpsTracker = new GPSTracker(BaseAppCompatActivity.this);
        if (gpsTracker.canGetLocation()) {
            return gpsTracker.getMyLocation();
        } else {
            return null;
        }
    }

    public void displayImage(Context context, ImageView imageView, String url, String name) {
        TextDrawable.IBuilder mDrawableBuilder = TextDrawable.builder().beginConfig()
                .withBorder(4)
                .endConfig()
                .round();

        TextDrawable drawable;
        if (!TextUtils.isNullOrEmpty(name)) {
            drawable = mDrawableBuilder.build(String.valueOf(name.charAt(0)), getActivity().getResources().getColor(R.color.header_color));
            Picasso.with(context).load(url).placeholder(drawable).error(drawable).fit().into(imageView);
        } else {
            Picasso.with(context).load(url).placeholder(R.drawable.user_pic).error(R.drawable.user_pic).fit().into(imageView);
        }
    }

    public void displayImage(Context context, ImageView imageView, String url) {
        this.displayImage(context, imageView, url, null);
        // Picasso.with(context).load(url).placeholder(R.drawable.user_pic).error(R.drawable.user_pic).fit().into(imageView);
    }

    public void showLoginAlert() {
        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
        alert.setTitle("Login Required");
        alert.setMessage("Please register with STAFFING APP to explore new job opportunities and make a job profile for yourself.");
        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                startActivity(new Intent(BaseAppCompatActivity.this, LoginActivity.class).putExtra(Constant.DATA, LoginWay.IN_APP_LOGIN));
            }
        });
        alert.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        alert.show();
    }
}