package com.hire4work.base;

import android.support.v7.app.AppCompatActivity;

/**
 * Created by Kailash Chouhan (Ratufa Technologies)
 * Email: kailash.chouhan@ratufa.com
 */
public class BaseActivity extends AppCompatActivity {
    protected AppCompatActivity getActivity() {
        return this;
    }
}
