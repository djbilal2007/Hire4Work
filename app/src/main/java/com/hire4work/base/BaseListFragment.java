package com.hire4work.base;

import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.View;

import com.google.android.gms.maps.model.LatLng;
import com.hire4work.customewidgets.ExpandableHeightListView;
import com.hire4work.sliding.SlidingMenu;

/**
 * Created by Kailash Chouhan (Ratufa Technologies)
 * Email: kailash.chouhan@ratufa.com
 */
public class BaseListFragment extends ListFragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public void displayView(int position, Boolean isBackStack) {
        displayView(position, null, null, isBackStack);
    }

    public void displayView(int position, SlidingMenu menu, Boolean isBackStack) {
        displayView(position, menu, null, isBackStack);
    }

    public void displayView(int position, ExpandableHeightListView mDrawerList, Boolean isBackStack) {
        displayView(position, null, mDrawerList, isBackStack);
    }

    public void displayView(int position, SlidingMenu menu, ExpandableHeightListView mDrawerList, Boolean isBackStack) {
        ((BaseAppCompatActivity) getActivity()).displayView(position, menu, mDrawerList, isBackStack);
    }

    public LatLng getMyLocation() {
        return ((BaseAppCompatActivity) getActivity()).getMyLocation();
    }

    public void showLoading(String message) {
        ((BaseAppCompatActivity) getActivity()).showLoading(message);
    }

    public void showLoading() {
        ((BaseAppCompatActivity) getActivity()).showLoading();
    }

    public void showToast(String message) {
        ((BaseAppCompatActivity) getActivity()).showToast(message);
    }

    public void hideLoading() {
        try {
            ((BaseAppCompatActivity) getActivity()).hideLoading();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

//    public Date getParseTime() {
//        return ((BaseAppCompatActivity) getActivity()).getParseTime();
//    }

    public void hideKeyBoard(View v) {
        ((BaseAppCompatActivity) getActivity()).hideKeyBoard(v);
    }

    public Boolean isInternetConnected() {
        return ((BaseAppCompatActivity) getActivity()).isInternetConnected();
    }
//
//    public void displayImage(Context context, ImageView imageView, String url) {
//        this.displayImage(context, imageView, url, null);
//    }
//
//    public void displayImage(Context context, ImageView imageView, String url, String name) {
//        ((BaseAppCompatActivity) getActivity()).displayImage(context, imageView, url, name);
//    }

    public void showAlertDialog(String message) {
        showAlertDialog(null, message);
    }

    public void showAlertDialog(String title, String message) {
        ((BaseAppCompatActivity) getActivity()).showAlertDialog(title, message);
    }

    public Boolean isEmployer() {
        return ((BaseAppCompatActivity) getActivity()).isEmployer();
    }
}
