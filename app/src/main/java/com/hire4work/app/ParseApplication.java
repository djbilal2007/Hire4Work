package com.hire4work.app;

/**
 * Created by Kailash Chouhan (Ratufa Technologies)
 * Email: kailash.chouhan@ratufa.com
 */

import android.app.Application;

import com.facebook.FacebookSdk;
import com.hire4work.R;
import com.hire4work.bean.ChatMessage;
import com.hire4work.bean.Disputes;
import com.hire4work.bean.FundTransection;
import com.hire4work.bean.InvitesNotification;
import com.hire4work.bean.JobApply;
import com.hire4work.bean.News;
import com.hire4work.bean.NotificationCounter;
import com.hire4work.bean.PersonData;
import com.hire4work.bean.PostJob;
import com.hire4work.bean.PushNotification;
import com.hire4work.bean.UserRatting;
import com.hire4work.bean.WorkCategory;
import com.parse.Parse;
import com.parse.ParseACL;
import com.parse.ParseFacebookUtils;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParseTwitterUtils;
import com.parse.ParseUser;

public class ParseApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        FacebookSdk.sdkInitialize(getApplicationContext());

        // Initialize Crash Reporting.
//        ParseCrashReporting.enable(this);
        // Enable Local Datastore.
        //Parse.enableLocalDatastore(this);
        // Registers a custom subclass WorkCategory type with the Parse SDK
        ParseObject.registerSubclass(WorkCategory.class);
        // Registers a custom subclass News type with the Parse SDK
        ParseObject.registerSubclass(News.class);
        // Registers a custom subclass PostJob type with the Parse SDK
        ParseObject.registerSubclass(PostJob.class);
        // Registers a custom subclass JobApply type with the Parse SDK
        ParseObject.registerSubclass(JobApply.class);
        // Registers a custom subclass InvitesNotification type with the Parse SDK
        ParseObject.registerSubclass(InvitesNotification.class);

        // Registers a custom subclass PersonData type with the Parse SDK
        ParseObject.registerSubclass(PersonData.class);

        // Registers a custom subclass ChatMessage type with the Parse SDK
        ParseObject.registerSubclass(ChatMessage.class);
        // Registers a custom subclass FundTransection type with the Parse SDK
        ParseObject.registerSubclass(FundTransection.class);
        // Registers a custom subclass PushNotification type with the Parse SDK
        ParseObject.registerSubclass(PushNotification.class);
        // Registers a custom subclass Disputes type with the Parse SDK
        ParseObject.registerSubclass(Disputes.class);
        // Registers a custom subclass UserRatting type with the Parse SDK
        ParseObject.registerSubclass(UserRatting.class);
        // Registers a custom subclass NotificationCounter type with the Parse SDK
        ParseObject.registerSubclass(NotificationCounter.class);
        // Add your initialization code here

//        Parse.initialize(this, "KY6rs5HVOvnJjHOHiA8C3SLetBKUc4VsXdstGtzV", "Sfn8ZnHRZvAJ49KAU22umP3iGEeYmEOqGhPRPuhQ").;
        // ParseInstallation.


        Parse.initialize(new Parse.Configuration.Builder(this)
                .applicationId(getResources().getString(R.string.parse_app_id)) // should correspond to APP_ID env variable
                .clientKey(getResources().getString(R.string.parse_client_id))  // set explicitly blank unless clientKey is configured on Parse server
                .server(getResources().getString(R.string.parse_server_url))
                .build());

        ParseUser.enableAutomaticUser();

        ParseUser.getCurrentUser().saveInBackground();

        // Save the current installation.
        ParseInstallation installation = ParseInstallation.getCurrentInstallation();
        installation.put("GCMSenderId", "900998133870");
        installation.saveInBackground();

        ParseACL defaultACL = new ParseACL();
        // Optionally enable public read access.
        defaultACL.setPublicReadAccess(true);
        defaultACL.setPublicWriteAccess(true);
        ParseACL.setDefaultACL(defaultACL, true);

        // For Facebook
        ParseFacebookUtils.initialize(this);

        // For Twitter
        ParseTwitterUtils.initialize("Ztg6sAS3J1kynEQTuLNw7EsLI", "C4tDfWomNTSySYSfTIjDXKxBPKuQx2fjGHn5Dm33OZodC00oy7");

    }
}
