package com.hire4work.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.login.LoginManager;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.hire4work.R;
import com.hire4work.adapter.NavDrawerListAdapter;
import com.hire4work.app.ParseApplication;
import com.hire4work.base.BaseAppCompatActivity;
import com.hire4work.bean.NavDrawerItem;
import com.hire4work.bean.appenum.LoginWay;
import com.hire4work.customewidgets.BezelImageView;
import com.hire4work.customewidgets.ExpandableHeightListView;
import com.hire4work.fragment.employer.EmployerTimerFragment;
import com.hire4work.sliding.SlidingMenu;
import com.hire4work.utills.Constant;
import com.hire4work.utills.Debug;
import com.hire4work.utills.Keys;
import com.hire4work.utills.SharedPrefrenceManager;
import com.hire4work.utills.Utils;
import com.linkedin.platform.LISessionManager;
import com.parse.LogOutCallback;
import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.ParseInstallation;
import com.parse.ParsePush;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import org.json.JSONObject;

import java.util.ArrayList;

import static com.hire4work.utills.UserConstant.CURRENT_LOCATION;
import static com.hire4work.utills.UserConstant.NAME;
import static com.hire4work.utills.UserConstant.PICTURE;

/**
 * Created by Kailash Chouhan (Ratufa Technologies)
 * Email: kailash.chouhan@ratufa.com
 */

public class HomeActivity extends BaseAppCompatActivity implements View.OnClickListener {

    private SlidingMenu menu;
    private String TAG = getClass().getSimpleName();
    private TextView signOutBtn;
    private ExpandableHeightListView mDrawerList;
    private BezelImageView profileImage;
    private MyNotificationReciever myNotificationReciever;

    public Handler handler = new Handler();

    private GoogleApiClient googleApiClient;
    private GoogleSignInOptions gso;

    @Override
    protected void onStart() {
        gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                                .requestEmail()
                                .build();

        googleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
                        Log.d(TAG,""+connectionResult.getErrorMessage());
                    }
                })
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
        googleApiClient.connect();
        super.onStart();
    }

    @Override
    protected void onPause() {
        super.onPause();

        googleApiClient.stopAutoManage(this);
        googleApiClient.disconnect();
    }

    @Override
    protected void onStop() {
        super.onStop();

        if(googleApiClient != null && googleApiClient.isConnected()){
            googleApiClient.stopAutoManage(this);
            googleApiClient.disconnect();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        if (sharedPrefrenceManager.getSavedPrefrenceValue(getActivity(), "", Keys.IS_LOGIN).equalsIgnoreCase("true")) {
            ParseInstallation installation = ParseInstallation.getCurrentInstallation();
            installation.put("global", ParseUser.getCurrentUser());
            installation.put("installedUserId", ParseUser.getCurrentUser().getObjectId());
            installation.saveInBackground(new SaveCallback() {
                @Override
                public void done(ParseException e) {
                    //Subscribing to Push notifications
                    ParsePush.subscribeInBackground("global");
                }
            });
        }

        // load slide menu items
        String[] navMenuTitles = getResources().getStringArray(R.array.nav_drawer_items);

        // nav drawer icons from resources
        TypedArray navMenuIcons = getResources().obtainTypedArray(R.array.nav_drawer_icons);

        int width = (Utils.getDeviceWidth(HomeActivity.this) * 70) / 100;
        // configure the SlidingMenu
        menu = new SlidingMenu(this);
        menu.setMode(SlidingMenu.LEFT);
        menu.setTouchModeAbove(SlidingMenu.TOUCHMODE_MARGIN);
        menu.setFadeDegree(0.35f);
        menu.setBehindWidth(width);
        menu.attachToActivity(this, SlidingMenu.SLIDING_CONTENT);
        menu.setMenu(R.layout.left_slide_menu);

        mDrawerList = (ExpandableHeightListView) findViewById(R.id.list_slidermenu);

        ArrayList<NavDrawerItem> navDrawerItems = new ArrayList<>();

        // adding nav drawer items to array
        // News
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[0], navMenuIcons.getResourceId(0, -1)));
        //Edit profile
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[1], navMenuIcons.getResourceId(1, -1)));
        // Find Job
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[2], navMenuIcons.getResourceId(2, -1)));
        // Invites
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[3], navMenuIcons.getResourceId(3, -1)));
        // Post Job
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[4], navMenuIcons.getResourceId(4, -1)));
        // My Job
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[5], navMenuIcons.getResourceId(5, -1)));
        // Membership
        //navDrawerItems.add(new NavDrawerItem(navMenuTitles[6], navMenuIcons.getResourceId(6, -1)));

        // Recycle the typed array
        navMenuIcons.recycle();

        // setting the nav drawer list adapter
        NavDrawerListAdapter adapter = new NavDrawerListAdapter(getApplicationContext(), navDrawerItems);
        mDrawerList.setAdapter(adapter);
        mDrawerList.setExpanded(true);
        mDrawerList.setOnItemClickListener(new SlideMenuClickListener());

        ImageView left_button = (ImageView) findViewById(R.id.left_buton);
        left_button.setOnClickListener(this);

        ImageView locationBtn = (ImageView) findViewById(R.id.locationBtn);
        locationBtn.setOnClickListener(this);

        signOutBtn = (TextView) findViewById(R.id.signOutBtn);
        signOutBtn.setOnClickListener(this);

        // if user login then shoe SIGN OUT text with sign out icon
        if (sharedPrefrenceManager.getSavedPrefrenceValue(HomeActivity.this, "", Keys.IS_LOGIN).equalsIgnoreCase("true")) {
            signOutBtn.setText(getString(R.string.SignOut));
            signOutBtn.setCompoundDrawablesWithIntrinsicBounds(null,
                    null,
                    getResources().getDrawable(R.drawable.sign_out_icon),
                    null);
        } else {    // if user not login then shoe SIGN IN text with sign in icon
            signOutBtn.setText(getString(R.string.SignIn));
            signOutBtn.setCompoundDrawablesWithIntrinsicBounds(null,
                    null,
                    getResources().getDrawable(R.drawable.sign_in_icon),
                    null);
        }

        profileImage = (BezelImageView) findViewById(R.id.profileImage);
        // check user image is available or not
        if (ParseUser.getCurrentUser().getParseFile(PICTURE) != null) {
            // display image in imageView
            displayImage(HomeActivity.this, profileImage, ParseUser.getCurrentUser().getParseFile(PICTURE).getUrl(), ParseUser.getCurrentUser().getString(NAME));
        }

        if (savedInstanceState == null) {
            // on first time display view for first nav item
            displayView(0, menu, mDrawerList, false);
        }

        initNotification();
    }

    public void showUserImage() {
        if (ParseUser.getCurrentUser().getParseFile(PICTURE) != null) {
            // display image in imageView
            displayImage(HomeActivity.this, profileImage, ParseUser.getCurrentUser().getParseFile(PICTURE).getUrl(), ParseUser.getCurrentUser().getString(NAME));
        }

        if (sharedPrefrenceManager.getSavedPrefrenceValue(HomeActivity.this, "", Keys.IS_LOGIN).equalsIgnoreCase("true")) {
            signOutBtn.setText(getString(R.string.SignOut));
            signOutBtn.setCompoundDrawablesWithIntrinsicBounds(null,
                    null,
                    getResources().getDrawable(R.drawable.sign_out_icon),
                    null);
        } else {    // if user not login then shoe SIGN IN text with sign in icon
            signOutBtn.setText(getString(R.string.SignIn));
            signOutBtn.setCompoundDrawablesWithIntrinsicBounds(null,
                    null,
                    getResources().getDrawable(R.drawable.sign_in_icon),
                    null);
        }
    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        switch (v.getId()) {

            // location button
            case R.id.locationBtn:
                if (isInternetConnected()) {
                    if (sharedPrefrenceManager.getSavedPrefrenceValue(HomeActivity.this, "", Keys.IS_LOGIN).equalsIgnoreCase("true")) {
                        showLoading();
                        ParseUser parseUser = ParseUser.getCurrentUser();
                        try {
                            ParseGeoPoint parseGeoPoint = new ParseGeoPoint(getMyLocation().latitude, getMyLocation().longitude);
                            parseUser.put(CURRENT_LOCATION, parseGeoPoint);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        parseUser.saveInBackground(new SaveCallback() {
                            @Override
                            public void done(ParseException e) {
                                hideLoading();
                                if (e == null) {
                                    showToast("User location successfully updated.");
                                }
                            }
                        });
                    } else {
                        showLoginAlert();
                    }
                } else {
                    showAlertDialog(getString(R.string.internet));
                }
                break;

            // slider menu icon click
            case R.id.left_buton:
                showUserImage();
                menu.toggle();
                break;

            // sign in/out button click
            case R.id.signOutBtn:
                if (sharedPrefrenceManager.getSavedPrefrenceValue(HomeActivity.this, "", Keys.IS_LOGIN).equalsIgnoreCase("true")) {
                    showSignOutAlert();
                } else {
                    startActivity(new Intent(HomeActivity.this, LoginActivity.class).putExtra(Constant.DATA, LoginWay.IN_APP_LOGIN));
                }
                break;

            default:
                break;
        }
    }

    public void logoutTask() {
        showLoading();

        // for Facebook
        LoginManager.getInstance().logOut();
        // for LinkedIn
        LISessionManager.getInstance(getApplicationContext()).clearSession();
        // for twitter
        CookieSyncManager.createInstance(this);
        CookieManager cookieManager = CookieManager.getInstance();
        cookieManager.removeSessionCookie();
        cookieManager.removeAllCookie();

        //for Google
        signOutFromGoogle();
        //revokeAccessFromGoogle();

        gotoLoginScreen();
    }

    private void signOutFromGoogle(){
        Auth.GoogleSignInApi.signOut(googleApiClient).setResultCallback(new ResultCallback<Status>() {
            @Override
            public void onResult(@NonNull Status status) {
                Log.i("HomeActivity","HomeActivity: Logged Out Successfully");
            }
        });
    }

  /*  private void revokeAccessFromGoogle(){
        Auth.GoogleSignInApi.revokeAccess(googleApiClient).setResultCallback(new ResultCallback<Status>() {
            @Override
            public void onResult(@NonNull Status status) {
                Log.i("HomeActivity","HomeActivity: Revoked Successfully");
            }
        });
    }*/


    private SharedPrefrenceManager sharedPrefrenceManager = SharedPrefrenceManager.getInstance();

    private void gotoLoginScreen() {
        //ParseUser.getCurrentUser().deleteInBackground();
        ParseUser.logOutInBackground(new LogOutCallback() {
            @Override
            public void done(ParseException e) {
                ParsePush.unsubscribeInBackground("Global", new SaveCallback() {
                    @Override
                    public void done(ParseException e) {
                        hideLoading();
                        sharedPrefrenceManager.setPrefrenceValue(HomeActivity.this, "false", Keys.IS_LOGIN);
                        Intent intent = new Intent(HomeActivity.this, LoginActivity.class);
                        intent.putExtra(Constant.DATA, LoginWay.INITIAL_LOGIN);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        finish();
                    }
                });
            }
        });
    }

    private void showSignOutAlert() {
        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
        alert.setTitle("Logout");
        alert.setMessage("Do you want to logout?");
        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                logoutTask();
            }
        });
        alert.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        alert.show();
    }

    @Override
    public void onBackPressed() {
        exitApp();
    }

    public void exitApp() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_NEGATIVE:
                        dialog.dismiss();
                        Intent intent = new Intent();
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        finish();
                        break;
                    case DialogInterface.BUTTON_POSITIVE:
                        dialog.dismiss();
                        break;
                }
            }
        };
        builder.setTitle(getString(R.string.app_name)).setMessage(getResources().getString(R.string.do_you_want_to_exit)).setPositiveButton(getResources().getString(R.string.no), dialogClickListener).setNegativeButton(getResources().getString(R.string.yes), dialogClickListener).show();
    }

    /**
     * Slide menu item click listener
     */
    private class SlideMenuClickListener implements ExpandableHeightListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            // display view for selected nav drawer item
            displayView(position, menu, mDrawerList, false);
        }
    }

    public void initNotification() {
        myNotificationReciever = new MyNotificationReciever();
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(myNotificationReciever, new IntentFilter(Constant.NOTIFICATION));
    }

    public void UnregisterNotification() {
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(myNotificationReciever);
    }

    class MyNotificationReciever extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, final Intent data) {
            handler.post(new Runnable() {
                @Override
                public void run() {
                    try {
                        if (data != null && data.getExtras() != null) {
                            String resCateg = data.getStringExtra(Constant.DATA);
                            JSONObject jsonObject = new JSONObject(resCateg);
                            Debug.e(TAG, "" + jsonObject.toString());

                            EmployerTimerFragment employerTimerFragment = new EmployerTimerFragment();
                            employerTimerFragment.pushNotificationData(jsonObject);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        UnregisterNotification();
    }
}
