package com.hire4work.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.hire4work.R;
import com.hire4work.adapter.SelectCategoryAdapter;
import com.hire4work.base.BaseAppCompatActivity;
import com.hire4work.bean.WorkCategory;
import com.hire4work.customewidgets.ExpandableHeightListView;
import com.hire4work.utills.Constant;
import com.hire4work.utills.TextUtils;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kailash Chouhan (Ratufa Technologies)
 * Email: kailash.chouhan@ratufa.com
 */
public class SelectCategoryActivity extends BaseAppCompatActivity implements View.OnClickListener {

    private List<WorkCategory> categoryList = new ArrayList<WorkCategory>();
    private TextView tv_employee;
    private SelectCategoryAdapter objAdapter;
    private CheckBox emp_checkbox;
    private List<String> stringArrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_category);

        stringArrayList = (List<String>) getIntent().getSerializableExtra(Constant.DATA);

        getCategory();

        TextView tv_category_title = (TextView) findViewById(R.id.tv_category_title);
        tv_category_title.setText(getString(R.string.select_cate));

        ImageView iv_back = (ImageView) findViewById(R.id.iv_back);
        iv_back.setOnClickListener(this);

        ExpandableHeightListView listView = (ExpandableHeightListView) findViewById(R.id.listview);
        listView.setExpanded(true);

        TextView tv_done = (TextView) findViewById(R.id.tv_done);
        tv_done.setOnClickListener(this);

        tv_employee = (TextView) findViewById(R.id.tv_employee);
        tv_employee.setOnClickListener(this);

        emp_checkbox = (CheckBox) findViewById(R.id.emp_checkbox);
        emp_checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    for (int i = 0; i < categoryList.size(); i++) {
                        categoryList.get(i).setSelected(false);
                        objAdapter.notifyDataSetChanged();
                    }
                }
            }
        });

        objAdapter = new SelectCategoryAdapter(SelectCategoryActivity.this, categoryList);
        listView.setAdapter(objAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                emp_checkbox.setChecked(false);
                CheckBox chk = (CheckBox) view.findViewById(R.id.checkbox);
                WorkCategory bean = categoryList.get(position);
                if (bean.isSelected()) {
                    bean.setSelected(false);
                    chk.setChecked(false);
                } else {
                    bean.setSelected(true);
                    chk.setChecked(true);
                }
            }
        });
    }

    private int getCategoryPos(String category) {
        int k = 0;
        for (int i = 0; i < this.categoryList.size(); ++i) {
            if (this.categoryList.get(i).getjobCategory().equals(category)) {
                k = i;
                break;
            }
        }
        return k;
    }

    public void getCategory() {
        showLoading(getString(R.string.please_wait));
        ParseQuery<WorkCategory> query = new ParseQuery<WorkCategory>(WorkCategory.class);
        query.orderByAscending("jobCategory");
        query.findInBackground(new FindCallback<WorkCategory>() {
            @Override
            public void done(List<WorkCategory> list, ParseException e) {
                hideLoading();
                categoryList.clear();
                categoryList.addAll(list);
                for (int i = 0; i < stringArrayList.size(); i++) {
                    if (stringArrayList.get(i).trim().equalsIgnoreCase(getString(R.string.employer))) {
                        emp_checkbox.setChecked(true);
                    } else {
                        emp_checkbox.setChecked(false);
                        categoryList.get(getCategoryPos(stringArrayList.get(i).trim())).setSelected(true);
                    }
                }
                objAdapter.notifyDataSetChanged();
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back:
                onBackPressed();
                break;

            case R.id.tv_employee:
                if (emp_checkbox.isChecked()) {
                    emp_checkbox.setChecked(false);
                } else {
                    emp_checkbox.setChecked(true);
                    for (int i = 0; i < categoryList.size(); i++) {
                        categoryList.get(i).setSelected(false);
                        objAdapter.notifyDataSetChanged();
                    }
                }
                break;

            case R.id.tv_done:
                List<String> professionList = new ArrayList<String>();
                if (emp_checkbox.isChecked()) {
                    professionList.add(tv_employee.getText().toString().trim());
                } else {
                    // Retrive Data from list
                    for (WorkCategory bean : categoryList) {
                        if (bean.isSelected()) {
                            professionList.add(bean.getjobCategory());
                        }
                    }
                }

                if (TextUtils.isNullOrEmpty(professionList)) {
                    showToast(getString(R.string.select_cate));
                } else {
                    Intent intent = new Intent();
                    intent.putExtra(Constant.DATA, (Serializable) professionList);
                    setResult(RESULT_OK, intent);
                    finish();
                }
                break;

            default:
                break;
        }
    }
}
