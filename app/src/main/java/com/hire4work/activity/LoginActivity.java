package com.hire4work.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;

import com.hire4work.GPS.GpsService;
import com.hire4work.R;
import com.hire4work.app.ParseApplication;
import com.hire4work.base.BaseAppCompatActivity;
import com.hire4work.bean.PersonData;
import com.hire4work.bean.appenum.LoginType;
import com.hire4work.bean.appenum.LoginWay;
import com.hire4work.utills.Constant;
import com.hire4work.utills.Debug;
import com.hire4work.utills.Keys;
import com.hire4work.utills.PermissionsUtils;
import com.hire4work.utills.SharedPrefrenceManager;
import com.hire4work.utills.TextUtils;
import com.hire4work.utills.UserConstant;
import com.hire4work.utills.Utils;

import com.linkedin.platform.APIHelper;
import com.linkedin.platform.LISessionManager;
import com.linkedin.platform.errors.LIApiError;
import com.linkedin.platform.errors.LIAuthError;
import com.linkedin.platform.listeners.ApiListener;
import com.linkedin.platform.listeners.ApiResponse;
import com.linkedin.platform.listeners.AuthListener;
import com.linkedin.platform.utils.Scope;

import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.LogInCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseFacebookUtils;
import com.parse.ParseFile;
import com.parse.ParseInstallation;
import com.parse.ParseQuery;
import com.parse.ParseTwitterUtils;
import com.parse.ParseUser;
import com.parse.RequestPasswordResetCallback;
import com.parse.SaveCallback;
import com.parse.SignUpCallback;
import com.parse.twitter.Twitter;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by Kailash Chouhan (Ratufa Technologies)
 * Email: kailash.chouhan@ratufa.com
 */
public class LoginActivity extends BaseAppCompatActivity implements View.OnClickListener {

    private static final int RC_SIGN_IN = 1;
    private final String TAG = getClass().getSimpleName();
    private TextView forgotPassword;
    private Intent intent;
    private EditText emailEdt, passEdt;
    private CheckBox rememberMeCheckbox;
    private SharedPrefrenceManager sharedPrefrenceManager = SharedPrefrenceManager.getInstance();
    private LoginWay loginWay;
    Context context;

    //Google Sign in References
    private GoogleApiClient googleApiClient;
    private GoogleSignInOptions gso;

    //Instagram Sign In URLs
    private static final String AUTHURL = "https://api.instagram.com/oauth/authorize/";
    private static final String TOKENURL = "https://api.instagram.com/oauth/access_token";
    private static final String APIURL = "https://api.instagram.com/v1";
    private static final String CALLBACKURL = "http://www.ratufa.com";
    private static String request_token;
    private String tokenURLString;
    private String authURLString;


   /* @Override
    protected void onStart() {
        super.onStart();
        ParseQuery query = ParseQuery.getQuery(ParseUser.class);
    }*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        //Google Sign in
        gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .requestIdToken(getString(R.string.google_client_id))
                .requestServerAuthCode(getString(R.string.google_client_id))
                .build();

        // .requestIdToken(getString(R.string.google_client_id))
        //.requestServerAuthCode(getString(R.string.google_client_id))

        googleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
                        Log.d(TAG,""+connectionResult.getErrorMessage());
                    }
                })
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        context=this;
        Utils.initThreadPolicy();
        loginWay = (LoginWay) getIntent().getSerializableExtra(Constant.DATA);

        TextView signUp = (TextView) findViewById(R.id.signUp);
        signUp.setPaintFlags(signUp.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        signUp.setOnClickListener(this);

        forgotPassword = (TextView) findViewById(R.id.forgotPassword);
        forgotPassword.setPaintFlags(forgotPassword.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        forgotPassword.setOnClickListener(this);

        LinearLayout layout_check_box = (LinearLayout) findViewById(R.id.layout_check_box);
        layout_check_box.setOnClickListener(this);

        Button loginBtn = (Button) findViewById(R.id.loginBtn);
        loginBtn.setOnClickListener(this);

        ImageView googleBtn = (ImageView) findViewById(R.id.googleBtn);
        googleBtn.setOnClickListener(this);

        ImageView facebookBtn = (ImageView) findViewById(R.id.facebookBtn);
        facebookBtn.setOnClickListener(this);

        ImageView instagramBtn = (ImageView) findViewById(R.id.instagramBtn);
        instagramBtn.setOnClickListener(this);

        ImageView twitterBtn = (ImageView) findViewById(R.id.twitterBtn);
        twitterBtn.setOnClickListener(this);

        ImageView linkedInBtn = (ImageView) findViewById(R.id.linkedInBtn);
        linkedInBtn.setOnClickListener(this);

        Button loginAsGuestBtn = (Button) findViewById(R.id.loginAsGuestBtn);
        if (isInAppLogin()) {
            loginAsGuestBtn.setVisibility(View.INVISIBLE);
        } else {
            loginAsGuestBtn.setVisibility(View.VISIBLE);
        }
        loginAsGuestBtn.setOnClickListener(this);

        emailEdt = (EditText) findViewById(R.id.emailEdt);

        passEdt = (EditText) findViewById(R.id.passEdt);

        rememberMeCheckbox = (CheckBox) findViewById(R.id.rememberMeCheckbox);

        if (sharedPrefrenceManager.getSavedPrefrenceValue(LoginActivity.this, "", Keys.REMEMBER_ME) != null) {
            if (sharedPrefrenceManager.getSavedPrefrenceValue(LoginActivity.this, "", Keys.REMEMBER_ME).equalsIgnoreCase("checked")) {
                rememberMeCheckbox.setChecked(true);
                emailEdt.setText(sharedPrefrenceManager.getSavedPrefrenceValue(LoginActivity.this, "", Keys.EMAIL_REMEMBER));
                passEdt.setText(sharedPrefrenceManager.getSavedPrefrenceValue(LoginActivity.this, "", Keys.PASSWORD_REMEMBER));
            } else {
                rememberMeCheckbox.setChecked(false);
                emailEdt.setText("");
                passEdt.setText("");
            }
        } else {
            rememberMeCheckbox.setChecked(false);
            emailEdt.setText("");
            passEdt.setText("");
        }
    }



    private boolean isInAppLogin() {
        return loginWay == LoginWay.IN_APP_LOGIN;
    }

    public void rememberMeTask() {
        if (rememberMeCheckbox.isChecked()) {
            sharedPrefrenceManager.setPrefrenceValue(LoginActivity.this, emailEdt.getText().toString(), Keys.EMAIL_REMEMBER);
            sharedPrefrenceManager.setPrefrenceValue(LoginActivity.this, passEdt.getText().toString(), Keys.PASSWORD_REMEMBER);
            sharedPrefrenceManager.setPrefrenceValue(LoginActivity.this, "checked", Keys.REMEMBER_ME);
        } else {
            sharedPrefrenceManager.setPrefrenceValue(LoginActivity.this, "", Keys.EMAIL_REMEMBER);
            sharedPrefrenceManager.setPrefrenceValue(LoginActivity.this, "", Keys.PASSWORD_REMEMBER);
            sharedPrefrenceManager.setPrefrenceValue(LoginActivity.this, "unchecked", Keys.REMEMBER_ME);
        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.layout_check_box:
                if (rememberMeCheckbox.isChecked()) {
                    rememberMeCheckbox.setChecked(false);
                } else {
                    rememberMeCheckbox.setChecked(true);
                }
                break;

            case R.id.signUp:
                intent = new Intent(LoginActivity.this, RegistrationActivity.class);
                startActivity(intent);
                break;

            case R.id.loginBtn:
                hideKeyBoard(v);
                if (isInternetConnected()) {
                    loginTask();
                } else {
                    showToast(getString(R.string.internet));
                }
                break;

            case R.id.facebookBtn:
                if (isInternetConnected()) {
                    setPermission(1);
                } else {
                    showToast(getString(R.string.internet));
                }
                break;

            case R.id.googleBtn:
                if (isInternetConnected()) {
//                    setPermission(1);
                  signInWithGoogle();
                } else {
                    showToast(getString(R.string.internet));
                }
                break;

            case R.id.instagramBtn:
                if (isInternetConnected()) {
//                  setPermission(1);
                   signInWithInstagram();
                } else {
                    showToast(getString(R.string.internet));
                }
                break;

            case R.id.twitterBtn:
                if (isInternetConnected()) {
                    ParseTwitterLogin();
                } else {
                    showToast(getString(R.string.internet));
                }
                break;

            case R.id.linkedInBtn:
                if (isInternetConnected()) {
                    linkedInLogin();
                } else {
                    showToast(getString(R.string.internet));
                }
                break;

            case R.id.loginAsGuestBtn:
                if (isInternetConnected()) {
                    startActivity(new Intent(LoginActivity.this, CategoryActivity.class)
                            .putExtra(Constant.USER_TYPE, LoginType.LOGIN_AS_GUEST)
                            .putExtra(Constant.DATA, LoginWay.INITIAL_LOGIN)
                            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
                    finish();
                } else {
                    showToast(getString(R.string.internet));
                }
                break;

            case R.id.forgotPassword:
                if (isInternetConnected()) {
                    resetPasswordPopup(forgotPassword);
                } else {
                    showToast(getString(R.string.internet));
                }
                break;

            default:
                break;
        }
    }


    private void setPermission(int caseType) {
        if (Build.VERSION.SDK_INT >= PermissionsUtils.SDK_INT_MARSHMALLOW) {
            if (!PermissionsUtils.getInstance(context).isPermissionGranted(context, Manifest.permission.ACCESS_FINE_LOCATION, "Location")) {
                return;
            }
            else
            {
                onLocation(caseType);
            }
        }
    }

    GpsService location;
    private void onLocation(int caseType) {
        location = new GpsService(context);
        if (location.isGPSEnabled) {
            if (caseType==1)
            {
                ParseFacebookLogin();
            } else {

            }
        } else {
            showSettingsAlert();
        }
    }



    public void showSettingsAlert()
    {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        // Setting Dialog Title
        alertDialog.setTitle(context.getResources().getString(R.string.gps_tital));

        // Setting Dialog Message
        alertDialog.setMessage(context.getResources().getString(R.string.gps_content));
        // Setting Icon to Dialog
        //  alertDialog.setIcon(R.drawable.);
        alertDialog.setCancelable(false);
        // On pressing Settings button
        alertDialog.setPositiveButton(context.getResources().getString(R.string.settings), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivityForResult(intent, 1);
            }
        });

        // on pressing cancel button
        alertDialog.setNegativeButton(context.getResources().getString(R.string.msg_cancel), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }



    @SuppressLint("InflateParams")
    private void resetPasswordPopup(View view) {
        final PopupWindow popup = new PopupWindow(getActivity());
        View layout = getActivity().getLayoutInflater().inflate(R.layout.forgot_password, null);
        popup.setContentView(layout);
        // Set content width and height
        popup.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        popup.setWidth(Utils.getDeviceWidth(getActivity()) - 80);
        // Closes the popup window when touch outside of it - when looses focus
        popup.setOutsideTouchable(true);
        popup.setFocusable(true);
        // Show anchored to button
        popup.setBackgroundDrawable(new BitmapDrawable());
        popup.showAtLocation(view, Gravity.CENTER, 0, 0);

        final EditText edt_email = (EditText) layout.findViewById(R.id.edt_email);
        Button btn_reset_password = (Button) layout.findViewById(R.id.btn_reset_password);
        btn_reset_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isNullOrEmpty(edt_email.getText().toString())) {
                    showToast(getString(R.string.email_required));
                } else {
                    popup.dismiss();
                    resetPassword(edt_email.getText().toString());
                }
            }
        });
    }

    private void resetPassword(String s) {
        showLoading();
        ParseUser.requestPasswordResetInBackground(s, new RequestPasswordResetCallback() {
            public void done(ParseException e) {
                hideLoading();
                if (e == null) {
                    // An email was successfully sent with reset instructions.
                    showAlertDialog("Success", "Please check the Email for reset link.");
                } else {
                    // Something went wrong. Look at the ParseException to see what's up.
                    // String cap = e.getMessage().toString().substring(0, 1).toUpperCase() + e.getMessage().toString().substring(1);
                    showAlertDialog(getString(R.string.app_name), "The Email is not registered with the application");
                }
            }
        });
    }

    private static Scope buildScope() {
        return Scope.build(Scope.R_BASICPROFILE, Scope.W_SHARE);
    }

    private String LINKEDIN = "linkedin";
//    private String FACEBOOK = "facebook";
//    private String TWITTER = "twitter";



    public void loginTask() {
        String email = emailEdt.getText().toString();
        String password = passEdt.getText().toString();

        if (TextUtils.isNullOrEmpty(email)) {
            showAlertDialog(getResources().getString(R.string.email_required));
            emailEdt.setSelection(email.length());
            emailEdt.requestFocus();
        } else if (!TextUtils.isValidEmail(email)) {
            showAlertDialog(getResources().getString(R.string.invalid_email));
            emailEdt.setSelection(email.length());
            emailEdt.requestFocus();
        } else if (TextUtils.isNullOrEmpty(password)) {
            showAlertDialog("Please enter your password to proceed.");
            passEdt.setSelection(password.length());
            passEdt.requestFocus();
        } else if (!Utils.isValidPassword(password)) {
            showAlertDialog(getResources().getString(R.string.password_required));
            passEdt.setSelection(password.length());
            passEdt.requestFocus();
        } else {
            rememberMeTask();
            CredentialLogin(email, password);
        }
    }

    public void CredentialLogin(String email, String password) {
        showLoading();

        ParseUser.logInInBackground(email, password, new LogInCallback() {
                    public void done(ParseUser user, ParseException e) {
                        hideLoading();
                        if (e == null) {
                            if (user != null) {
                                Debug.d(getClass().getSimpleName(), user.toString());
                                // check
                                if (user.getBoolean("emailVerified")) {
                                    gotoLoginScreen(user);
                                    showToast(getResources().getString(R.string.successfully_login));
                                } else {
                                    ParseUser.logOut();
                                    showAlertDialog(getString(R.string.app_name), "Please verify your email address and try again.");
                                }
                            }
                        } else {
                            showAlertDialog(getString(R.string.app_name), "Sorry, no matching record found, please try again");
                        }
                    }
                }
        );
    }

    private void linkedInLogin() {
        //showLoading();
        //For LinkedIn login and get all information
        LISessionManager.getInstance(context).init(LoginActivity.this, buildScope(), new AuthListener() {
            @Override
            public void onAuthSuccess() {
                // Authentication was successful.  You can now do
                // other calls with the SDK.
                String url = "https://api.linkedin.com/v1/people/~:(id,first-name,last-name,pictureUrl)";

                APIHelper apiHelper = APIHelper.getInstance(context);
                apiHelper.getRequest(LoginActivity.this, url, new ApiListener() {
                    @Override
                    public void onApiSuccess(ApiResponse apiResponse) {
                        // Success!
                        //hideLoading();
                        try {
                            JSONObject jsonObject = apiResponse.getResponseDataAsJson();
                            //String pictureUrl = jsonObject.has("pictureUrl") ? jsonObject.getString("pictureUrl") : null;
                            //Log.d(getClass().getSimpleName(), pictureUrl);
                            Debug.d(getClass().getSimpleName(), apiResponse.getResponseDataAsString());
                            snsLogin(jsonObject, LINKEDIN);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onApiError(LIApiError liApiError) {
                        // Error making GET request!
                        //hideLoading();
                        showAlertDialog("failed " + liApiError.toString());
                    }
                });

            }

            @Override
            public void onAuthError(LIAuthError error) {
                // Handle authentication errors
                //hideLoading();
                showAlertDialog("failed " + error.toString());
                Debug.d(getClass().getSimpleName(), error.toString());
            }
        }, true);
    }

    private void snsLogin(final JSONObject jsonObject, final String snsType) {
        showLoading();
        String id = null;
        try {
            id = jsonObject.getString("id");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        ParseUser.logInInBackground(id, id, new LogInCallback() {
            public void done(ParseUser user, ParseException e) {
                hideLoading();
                if (e == null) {
                    if (user != null) {
                        Debug.d(getClass().getSimpleName(), user.toString());
                        gotoLoginScreen(user);
                        showToast(getResources().getString(R.string.successfully_login));
                    } else {
                        snsRegistrationTask(jsonObject, snsType);
                    }
                } else {
                    showAlertDialog(e.getMessage());
                }
            }
        });
    }

    public void snsRegistrationTask(final JSONObject jsonObject, final String SNSType) {
        showLoading();

        String id = null, name = null, imageUrl = null;
        try {
            id = jsonObject.getString("id");
            name = jsonObject.getString("firstName") + " " + jsonObject.getString("lastName");
            imageUrl = jsonObject.getString("pictureUrl");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        PersonData parseUser = new PersonData();
        parseUser.setUsername(id);
        parseUser.setPassword(id);
        parseUser.setName(name);
        parseUser.setDeviceType("Android");

        if (!TextUtils.isNullOrEmpty(imageUrl)) {
            parseUser.setImage(getParseFile(imageUrl));
        }

        parseUser.signUpInBackground(new SignUpCallback() {
            @Override
            public void done(ParseException e) {
                hideLoading();
                if (e == null) {
                    snsLogin(jsonObject, SNSType);
                } else {
                    showAlertDialog(e.getMessage());
                }
            }
        });
    }

    private ParseFile getParseFile(String path) {
        ParseFile file = null;
        try {
            URL img_value = new URL(path);
            Bitmap bitmap = BitmapFactory.decodeStream(img_value.openConnection().getInputStream());
            // Convert it to byte
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            // Compress image to lower quality scale 1 - 100
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
            byte[] image = stream.toByteArray();

            long time = System.currentTimeMillis();
            String sImageName = time + ".jpeg";
            // Create the ParseFile
            file = new ParseFile(sImageName, image);
            // Upload the image into Parse Cloud
            file.saveInBackground();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return file;
    }

    private void ParseFacebookLogin() {
        showLoading();
        List<String> permissions = Arrays.asList("public_profile", "email");

        ParseFacebookUtils.logInWithReadPermissionsInBackground(LoginActivity.this, permissions, new LogInCallback() {
            @Override
            public void done(ParseUser user, ParseException e) {
                hideLoading();
                if (e == null) {
                    if (user == null) {
                        showAlertDialog("Facebook Login Cancel");
                    }
//                    else if (user.isNew()) {}

                    else {
                        makeMeRequest(user);
//                        gotoLoginScreen(user);
                    }
                } else {
                    showAlertDialog(e.getMessage());
                }
            }
        });
    }
    private void makeMeRequest(final ParseUser user) {
        showLoading();
        GraphRequest request = GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(),
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject jsonObject, GraphResponse graphResponse) {
                        hideLoading();
                        if (jsonObject != null) {
                            try {
                                Log.d("FacebookLogin", "" + user.getUsername());
                                Debug.d(TAG, "userProfile" + graphResponse.toString());
                                String imageUrl = jsonObject.getJSONObject("picture").getJSONObject("data").getString("url");
                                user.put(UserConstant.NAME, jsonObject.getString("name"));
                                user.put("deviceType", "Android");
//
                                Log.d("FacebookImageCheck", "Facebook: " + imageUrl);
//                                Log.d(TAG, "FacebookLogin: Object id: " + user.getObjectId());

                                if (!TextUtils.isNullOrEmpty(imageUrl)) {
                                    user.put(UserConstant.PICTURE, getParseFile(imageUrl));
                                }
                                user.saveInBackground();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            gotoLoginScreen(user);
                        } else if (graphResponse.getError() != null) {
                            switch (graphResponse.getError().getCategory()) {
                                case LOGIN_RECOVERABLE:
                                    Debug.d(TAG, "Authentication error: " + graphResponse.getError());
                                    break;

                                case TRANSIENT:
                                    Debug.d(TAG, "Transient error. Try again. " + graphResponse.getError());
                                    break;

                                case OTHER:
                                    Debug.d(TAG, "Some other error: " + graphResponse.getError());
                                    break;
                            }
                        }
                    }
                });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,email,gender,name,picture.type(large)");
        request.setParameters(parameters);
        request.executeAsync();
    }


    private void ParseTwitterLogin() {
        showLoading();
        ParseTwitterUtils.logIn(LoginActivity.this, new LogInCallback() {
            @Override
            public void done(ParseUser user, ParseException e) {
                hideLoading();
                if (e == null) {
                    if (user == null) {
                        showAlertDialog("Twitter Login Cancel");
                    }
//                    else if (user.isNew()) {
//                    }
                    else {
                        new TwitterUserDetail(ParseTwitterUtils.getTwitter(), user).execute();
//                        gotoLoginScreen(user);
                    }
                } else {
                    showAlertDialog(e.getMessage());
                }
            }
        });
    }

    class TwitterUserDetail extends AsyncTask<String, Void, HttpResponse> {
        StringBuilder stringBuilder = new StringBuilder();
        HttpResponse response;
        Twitter twitter;
        ParseUser user;

        public TwitterUserDetail(Twitter twitter, ParseUser user) {
            this.user = user;
            this.twitter = twitter;
        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            showLoading();
        }

        @Override
        protected HttpResponse doInBackground(String... urls) {
            try {
                HttpClient client = new DefaultHttpClient();
                HttpGet verifyGet = new HttpGet("https://api.twitter.com/1.1/users/show.json?user_id=" + twitter.getUserId());
                ParseTwitterUtils.getTwitter().signRequest(verifyGet);
                response = client.execute(verifyGet);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (Exception e) {
                // TODO: handle exception
                e.printStackTrace();
                return null;
            }
            return response;
        }

        @Override
        protected void onPostExecute(HttpResponse httpResponse) {
            hideLoading();
            try {
                if (httpResponse != null) {
                    InputStream in = httpResponse.getEntity().getContent();

                    BufferedReader reader = new BufferedReader(new InputStreamReader(in));
                    String line;
                    while ((line = reader.readLine()) != null) {
                        stringBuilder.append(line);
                    }
                    in.close();
                    Debug.i("response", stringBuilder.toString());

                    try {
                        JSONObject jsonObject = (JSONObject) new JSONTokener(stringBuilder.toString()).nextValue();
                        user.put(UserConstant.NAME, jsonObject.getString("name"));
                        user.put("deviceType", "Android");
                        String imageUrl = jsonObject.getString("profile_image_url");
                        if (!TextUtils.isNullOrEmpty(imageUrl)) {
                            user.put(UserConstant.PICTURE, getParseFile(imageUrl));
                        }
                        user.saveInBackground();
                        gotoLoginScreen(user);

                    } catch (JSONException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void gotoLoginScreen(ParseUser user) {
        sharedPrefrenceManager.setPrefrenceValue(LoginActivity.this, "true", Keys.IS_LOGIN);
        if(TextUtils.isNullOrEmpty(user.getList(UserConstant.PROFESSION))) {
            intent = new Intent(LoginActivity.this, CategoryActivity.class);
            intent.putExtra(Constant.USER_TYPE, LoginType.LOGIN);
            intent.putExtra(Constant.DATA, loginWay);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
        } else {
            if (isInAppLogin()) {
                finish();
            } else {
                intent = new Intent(LoginActivity.this, HomeActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
            return;
        }

        ParseFacebookUtils.onActivityResult(requestCode, resultCode, data);
        LISessionManager.getInstance(context).onActivityResult(this, requestCode, resultCode, data);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    //Google Sign In
    private void signInWithGoogle() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    private void handleSignInResult(GoogleSignInResult result) {
        Log.i(TAG, "Result: " + result.isSuccess());
        if(result.isSuccess()){
            GoogleSignInAccount acct = result.getSignInAccount();

            Log.i(TAG, "Logged-In Successfully Name: " + acct.getDisplayName());
            Log.i(TAG, "Logged-In Successfully Email: " + acct.getEmail());
            Log.i(TAG, "Logged-In Successfully PhotoUrl: " + acct.getPhotoUrl().toString());
            Log.i(TAG, "Logged-In Successfully Id: " + acct.getId());
            Log.i(TAG, "Logged-In Successfully IdToken: " + acct.getIdToken());
            Log.i(TAG, "Logged-In Successfully ServerAuthToken: " + acct.getServerAuthCode());

            String google_displayName = acct.getDisplayName();
            String google_profileImageUrl = acct.getPhotoUrl().toString();
            String google_email = acct.getEmail();
            String google_id = acct.getId();

            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("id", google_id);
                jsonObject.put("name", google_displayName);
                jsonObject.put("pictureUrl", google_profileImageUrl);
                jsonObject.put("email", google_email);
                checkUserForLoginOrSignUp(jsonObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    //Instagram Login
    private void signInWithInstagram(){
        ParseUser user = ParseUser.getCurrentUser();
        if(user == null){
            showAlertDialog("Instagram Login Cancel");
        }
//        else if(user.isNew()){
//        }
        else {
            instagramLogin();
//            gotoLoginScreen(user);
        }
    }

    private AlertDialog ad;

    private void instagramLogin() {
        authURLString = AUTHURL + "?client_id=" + context.getResources().getString(R.string.instagram_client_id) + "&redirect_uri=" + CALLBACKURL + "&response_type=code&display=touch&scope=likes+comments+relationships";
        tokenURLString = TOKENURL + "?client_id=" + context.getResources().getString(R.string.instagram_client_id) + "&client_secret=" + context.getResources().getString(R.string.instagram_client_secret_key) + "&redirect_uri=" + CALLBACKURL + "&grant_type=authorization_code";

        Log.d("InstaURL", authURLString);
        Log.d("InstaURL", tokenURLString);

        AlertDialog.Builder alert = new AlertDialog.Builder(LoginActivity.this);
        alert.setTitle("Login into Instagram");

        LinearLayout linearLayout = new LinearLayout(LoginActivity.this);
        WebView webView = new WebView(LoginActivity.this);
        EditText editText = new EditText(LoginActivity.this);

        editText.setVisibility(View.GONE);

        webView.getSettings().setJavaScriptEnabled(true);
        webView.setHorizontalScrollBarEnabled(false);
        webView.setVerticalScrollBarEnabled(false);
        webView.setWebViewClient(new AuthWebViewClient());
        webView.loadUrl(authURLString);

        linearLayout.setOrientation(LinearLayout.VERTICAL);
        linearLayout.addView(webView, LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        linearLayout.addView(editText, LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        alert.setView(linearLayout);
     /*   alert.setNegativeButton("Close", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });*/
//        alert.create().show();
        ad = alert.show();
    }

    class AuthWebViewClient extends WebViewClient{
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            if(url.startsWith(CALLBACKURL)){
                Log.i("URL", url);
                String [] parts = url.split("=");
                request_token = parts[1];
                Log.d(TAG, request_token);
                new MyAsyncTask().execute();
                return true;
            }
            return false;
        }
    }

    class MyAsyncTask extends AsyncTask<Void, Void, String> {
        String response;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading();
        }

        @Override
        protected String doInBackground(Void... params) {
            try {
                URL url = new URL(tokenURLString);
                Log.d(TAG, "Url: " + url.toString());
                HttpsURLConnection httpsURLConnection = (HttpsURLConnection) url.openConnection();
                httpsURLConnection.setRequestMethod("POST");
                httpsURLConnection.setDoInput(true);
                httpsURLConnection.setDoOutput(true);
                OutputStreamWriter outputStreamWriter = new OutputStreamWriter(httpsURLConnection.getOutputStream());
                outputStreamWriter.write("client_id=" + context.getResources().getString(R.string.instagram_client_id)
                        + "&client_secret=" + context.getResources().getString(R.string.instagram_client_secret_key)
                        + "&grant_type=authorization_code"
                        + "&redirect_uri=" + CALLBACKURL
                        + "&code=" + request_token);

                outputStreamWriter.flush();
                response = streamToString(httpsURLConnection.getInputStream());

                Log.i(TAG,"Response: " + response);



            } catch (Exception e) {
                e.printStackTrace();
                return  null;
            }
            return response;
        }

        @Override
        protected void onPostExecute(String responseJSON) {
            hideLoading();
            try {
                JSONObject jsonObject = new JSONObject(response);
                String access_token = jsonObject.getString("access_token");
                JSONObject jsonObject1 = jsonObject.getJSONObject("user");
                String full_name = jsonObject1.getString("full_name");
                String username = jsonObject1.getString("username");
                String profileImageUrl = jsonObject1.getString("profile_picture");

                Log.d(TAG, "Access Token: " + access_token);
                Log.d(TAG, "Full Name: " + full_name);
                Log.d(TAG, "Username: " + username);
                Log.d(TAG, "Profile Image URL: " + profileImageUrl);

                String email = username + "@instagram.com";

                JSONObject jsonObject2 = new JSONObject();
                try {
                    jsonObject2.put("id", access_token);
                    jsonObject2.put("name", full_name);
                    jsonObject2.put("pictureUrl", profileImageUrl);
                    jsonObject2.put("email", email);
                    checkUserForLoginOrSignUp(jsonObject2);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                //checkUserForLoginOrSignUp(email, access_token, full_name, profileImageUrl);

       /*         //i++;
                ParseUser.enableAutomaticUser();
                ParseUser.getCurrentUser().saveInBackground();
                ParseInstallation.getCurrentInstallation().saveInBackground();

                ParseUser user = ParseUser.getCurrentUser();
             //   user.put("id", i);
                user.put(UserConstant.NAME, full_name);
                user.put("deviceType", "Android");
                user.put(UserConstant.PICTURE, getParseFile(profileImageUrl));
                user.saveInBackground();
                gotoLoginScreen(user);*/

            }catch (JSONException e){
                e.printStackTrace();
            }
        }
    }

    public static String streamToString(InputStream inputStream) {
        try{
            BufferedReader br;
            StringBuffer sbuffer = new StringBuffer();
            br = new BufferedReader(new InputStreamReader(inputStream));
            String str = br.readLine();

            while(str != null){
                sbuffer.append(str);
                str = br.readLine();
            }

            Log.d("Sbuffer", sbuffer.toString());
            return sbuffer.toString();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
    public static final String PACKAGE = "com.hire4work";
    public void generateHashkey() {
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    PACKAGE,
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());

//				((TextView) findViewById(R.id.hashKey))
//						.setText(Base64.encodeToString(md.digest(),
//								Base64.NO_WRAP));


                System.out.println("hash key " + Base64.encodeToString(md.digest(), Base64.NO_WRAP));
                System.out.println("0000000000000000000000000");

            }
        } catch (PackageManager.NameNotFoundException e) {
            Log.d("Name not found", e.getMessage(), e);

        } catch (NoSuchAlgorithmException e) {
            Log.d("Error", e.getMessage(), e);
        }
    }

    String login_email = "";
    String login_id = "";
    String login_displayName = "";
    String login_profileImageUrl;

      public void checkUserForLoginOrSignUp(final JSONObject jsonObject){
        showLoading();
        try
            {
                login_id = jsonObject.getString("id");
                login_email = jsonObject.getString("email");
                login_profileImageUrl = jsonObject.getString("pictureUrl");
                login_displayName = jsonObject.getString("name");
            } catch (JSONException e) {
            e.printStackTrace();
        }

        ParseQuery<ParseUser> query = ParseUser.getQuery();
        query.whereEqualTo("username", login_email);
        query.findInBackground(new FindCallback<ParseUser>() {
            @Override
            public void done(List<ParseUser> objects, ParseException e) {
                if(e == null){
                    if(objects.size() > 0){
                        //Login Process
                        final ParseUser user1 = objects.get(0);
                        Log.d(TAG, "User Found: " + objects.size());
                        Log.d(TAG, "User Name: " + user1.getUsername());
                        Log.d(TAG, "User1: " + user1.isNew());

                        ParseUser.logOut();
                        ParseUser.logInInBackground(login_email, login_id, new LogInCallback() {
                            @Override
                            public void done(ParseUser user, ParseException e) {
                                if(e == null){
                                    if(user == null){
                                        Log.d(TAG,"LoginInBackground: user is null");
                                    }else {
                                        hideLoading();
                                        Log.d(TAG,"LoginInBackground: user is not null");
                                        gotoLoginScreen(user);
                                    }
                                }else {
                                    hideLoading();
                                    Log.d(TAG,"LoginException: " + e.getMessage());
                                    showAlertDialog(e.getMessage());
                                    clearCache();
                                }
                            }
                        });
                    } else{
                        //New User
                        final ParseFile profileImageFile = getParseFile(login_profileImageUrl);
                        if(profileImageFile != null) {
                            Log.d(TAG,"profileImageFile is not null");
                            profileImageFile.saveInBackground(new SaveCallback() {
                                @Override
                                public void done(ParseException e) {
                                    if (e == null) {
                                        final PersonData user = new PersonData();
                                        user.setUsername(login_email);
                                        user.setPassword(login_id);
                                        user.setEmail(login_email);
                                        user.put("isActive", "Active");
                                        user.setName(login_displayName);
                                        user.setDeviceType("Android");
                                        user.setImage(profileImageFile);
                                        user.signUpInBackground(new SignUpCallback() {
                                            @Override
                                            public void done(ParseException e) {
                                                hideLoading();
                                                if (e == null) {
                                                    Log.d(TAG, "Successfully Registered");
                                                    ParseUser.logOut();
                                                    ParseUser.logInInBackground(login_email, login_id, new LogInCallback() {
                                                        @Override
                                                        public void done(ParseUser user, ParseException e) {
                                                            if(e == null){
                                                                if(user == null){
                                                                    Log.d(TAG,"LoginInBackground: user is null");
                                                                }else {
                                                                    hideLoading();
                                                                    Log.d(TAG,"LoginInBackground: user is not null");
                                                                    gotoLoginScreen(user);
                                                                }
                                                            }else {
                                                                showAlertDialog(e.getMessage());
                                                                Log.d(TAG, "Login After SignIn: " + e.getMessage());
                                                            }
                                                        }
                                                    });
                                                } else {
                                                    showAlertDialog(e.getMessage());
                                                    Log.d(TAG, "Exception in SignUp: " + e.getMessage());
                                                    clearCache();
                                                }
                                            }
                                        });
                                    }
                                }
                            });
                        }else {
                            Log.d(TAG,"profileImageFile is null");
                            final PersonData user = new PersonData();
                            user.setUsername(login_email);
                            user.setPassword(login_id);
                            user.setEmail(login_email);
                            user.put("isActive", "Active");
                            user.setName(login_displayName);
                            user.setDeviceType("Android");
                            user.signUpInBackground(new SignUpCallback() {
                                @Override
                                public void done(ParseException e) {
                                    hideLoading();
                                    if (e == null) {
                                        Log.d(TAG, "Successfully Registered");
                                        ParseUser.logOut();
                                        ParseUser.logInInBackground(login_email, login_id, new LogInCallback() {
                                            @Override
                                            public void done(ParseUser user, ParseException e) {
                                                if(e == null){
                                                    if(user == null){
                                                        Log.d(TAG,"LoginInBackground: user is null");
                                                    }else {
                                                        hideLoading();
                                                        Log.d(TAG,"LoginInBackground: user is not null");
                                                        gotoLoginScreen(user);
                                                    }
                                                }else {
                                                    showAlertDialog(e.getMessage());
                                                    Log.d(TAG, "Login After SignIn: " + e.getMessage());
                                                }
                                            }
                                        });
                                    } else {
                                        showAlertDialog(e.getMessage());
                                        Log.d(TAG, "Exception in SignUp: " + e.getMessage());
                                        clearCache();
                                    }
                                }
                            });
                        }
                    }
                }else {
                    Log.d(TAG, "Exception: " + e.getMessage());
                }
            }
        });
    }

    private void clearCache(){
        if(ad != null){
            ad.dismiss();
        }
        CookieSyncManager.createInstance(LoginActivity.this);
        CookieManager cookieManager = CookieManager.getInstance();
        cookieManager.removeSessionCookie();
        cookieManager.removeAllCookie();
    }
}