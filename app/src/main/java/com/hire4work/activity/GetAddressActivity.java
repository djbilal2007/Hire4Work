package com.hire4work.activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.hire4work.R;
import com.hire4work.base.BaseAppCompatActivity;
import com.hire4work.bean.PlaceLocation;
import com.hire4work.bean.PlacesSuggestionsBean;
import com.hire4work.customewidgets.CustomAutoCompleteTextView;
import com.hire4work.placeapi.PlaceDetailsJSONParser;
import com.hire4work.utills.Debug;
import com.hire4work.utills.PermissionsUtils;
import com.hire4work.utills.TextUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kailash Chouhan (Ratufa Technologies)
 * Email: kailash.chouhan@ratufa.com
 */
public class GetAddressActivity extends BaseAppCompatActivity implements View.OnClickListener {

    private GoogleMap googleMap;
    private LatLng mapCentreLocation;
    private CustomAutoCompleteTextView tv_address;
    private DownloadTask downloadTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_address);

        TextView tv_select_address = (TextView) findViewById(R.id.tv_select_address);
        tv_select_address.setOnClickListener(this);

        tv_address = (CustomAutoCompleteTextView) findViewById(R.id.tv_address);
        tv_address.setThreshold(1);
        tv_address.setAdapter(new GooglePlacesAutocompleteAdapter(this, R.layout.spinner_text));
        tv_address.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                hideKeyBoard(tv_address);
                String reference = resultList.get(position).getReference();
                String url = getPlaceDetailsUrl(reference);
                // Creating ParserTask for parsing Google Places
                downloadTask = new DownloadTask();

                // Starting Parsing the JSON string
                // This causes to execute doInBackground() of ParserTask class
                downloadTask.execute(url);
            }
        });

        initializeMap();
    }

    /**
     * function to load map If map is not created it will create it for you
     */
    private void initializeMap() {
        final GoogleMap map;
        if (googleMap == null) {

            ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(GoogleMap map) {
                     googleMap = map;
                }
            });

            if(googleMap != null) {
                googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                // googleMap.setMyLocationEnabled(true);
                googleMap.getUiSettings().setZoomControlsEnabled(false);
                googleMap.getUiSettings().setCompassEnabled(true);
                googleMap.getUiSettings().setMyLocationButtonEnabled(false);

                CameraPosition cameraPosition = new CameraPosition.Builder().target(getMyLocation()).zoom(17).build();

                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

                mapCentreLocation = googleMap.getCameraPosition().target;

                new ReverseGeocodingTask(GetAddressActivity.this).execute(mapCentreLocation);

                googleMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {

                    @Override
                    public void onCameraChange(CameraPosition position) {
                        mapCentreLocation = googleMap.getCameraPosition().target;

                        // Adding Marker on the touched location with address
                        //new ReverseGeocodingTask(GetAddressActivity.this).execute(mapCentreLocation);
                    }
                });
            }
            if (googleMap == null) {
                showToast("Sorry! unable to create maps");
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_select_address:
                if (TextUtils.isNullOrEmpty(tv_address.getText().toString())) {
                    showAlertDialog("Select correct address");
                } else {
                    Intent intent = new Intent();
                    intent.putExtra("ADDRESS", tv_address.getText().toString());
                    if (mapCentreLocation != null) {
                        intent.putExtra("LATITUDE", mapCentreLocation.latitude);
                        intent.putExtra("LONGITUDE", mapCentreLocation.longitude);
                    }
                    setResult(RESULT_OK, intent);
                    finish();
                }
                break;

            default:
                break;
        }
    }

    class GooglePlacesAutocompleteAdapter extends ArrayAdapter<PlacesSuggestionsBean> implements Filterable {
        private ArrayList<PlacesSuggestionsBean> resultList;

        public GooglePlacesAutocompleteAdapter(Context context, int textViewResourceId) {
            super(context, textViewResourceId);
        }

        @Override
        public int getCount() {
            return resultList.size();
        }

        @Override
        public PlacesSuggestionsBean getItem(int index) {
            return resultList.get(index);
        }

        @Override
        public Filter getFilter() {
            return new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence constraint) {
                    FilterResults filterResults = new FilterResults();
                    if (constraint != null) {
                        // Retrieve the autocomplete results.
                        resultList = autocomplete(constraint.toString());

                        // Assign the data to the FilterResults
                        filterResults.values = resultList;
                        filterResults.count = resultList.size();
                    }
                    return filterResults;
                }

                @Override
                protected void publishResults(CharSequence constraint, FilterResults results) {
                    if (results != null && results.count > 0) {
                        notifyDataSetChanged();
                    } else {
                        notifyDataSetInvalidated();
                    }
                }
            };
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            final ViewHolder holder;
            PlacesSuggestionsBean bean = getItem(position);

            if (convertView == null) {

                holder = new ViewHolder();

                convertView = LayoutInflater.from(getContext()).inflate(R.layout.spinner_text, parent, false);

                holder.tvFullName = (TextView) convertView.findViewById(R.id.spinner);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            holder.tvFullName.setText(bean.getDescription());
            return convertView;
        }

        class ViewHolder {
            private TextView tvFullName;
        }
    }

    private static final String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place";
    private static final String TYPE_AUTOCOMPLETE = "/autocomplete";
    private static final String OUT_JSON = "/json";
    //------------ make your specific key ------------
    private static final String API_KEY = "AIzaSyCwYbBlV3qSpiiMj4Q9THL0yMikokVAaWM";
    ArrayList<PlacesSuggestionsBean> resultList = null;

    public ArrayList<PlacesSuggestionsBean> autocomplete(String input) {
        HttpURLConnection conn = null;
        StringBuilder jsonResults = new StringBuilder();
        try {
            URL url = new URL(PLACES_API_BASE + TYPE_AUTOCOMPLETE + OUT_JSON + "?key=" + API_KEY + "&input=" + URLEncoder.encode(input, "utf8"));

            System.out.println("URL: " + url);
            conn = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader(conn.getInputStream());

            // Load the results into a StringBuilder
            int read;
            char[] buff = new char[1024];
            while ((read = in.read(buff)) != -1) {
                jsonResults.append(buff, 0, read);
            }
        } catch (MalformedURLException e) {
            Debug.e("sdf", "Error processing Places API URL" + e);
            return resultList;
        } catch (IOException e) {
            Debug.e("dsd", "Error connecting to Places API" + e);
            return resultList;
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }

        try {
            // Create a JSON object hierarchy from the results
            JSONObject jsonObj = new JSONObject(jsonResults.toString());
            JSONArray predsJsonArray = jsonObj.getJSONArray("predictions");

            // Extract the Place descriptions from the results
            resultList = new ArrayList<>(predsJsonArray.length());
            for (int i = 0; i < predsJsonArray.length(); i++) {
                System.out.println(predsJsonArray.getJSONObject(i).getString("description"));
                System.out.println("============================================================");
                PlacesSuggestionsBean placesSuggestionsBean = new PlacesSuggestionsBean();
                placesSuggestionsBean.setDescription(predsJsonArray.getJSONObject(i).getString("description"));
                placesSuggestionsBean.setId(predsJsonArray.getJSONObject(i).getString("id"));
                placesSuggestionsBean.setReference(predsJsonArray.getJSONObject(i).getString("reference"));
                resultList.add(placesSuggestionsBean);
            }
        } catch (JSONException e) {
            Debug.e("adsd", "Cannot process JSON results" + e);
        }

        return resultList;
    }

    public static String getPlaceDetailsUrl(String ref) {

        // Obtain browser key from https://code.google.com/apis/console Extra key for test : AIzaSyCdi7F8PV02m13lhPm3gRQEmsEhWHB_iXk
        String key = "key=AIzaSyCwYbBlV3qSpiiMj4Q9THL0yMikokVAaWM";

        // reference of place
        String reference = "reference=" + ref;

        // Sensor enabled
        String sensor = "sensor=false";

        // Building the parameters to the web service
        String parameters = reference + "&" + sensor + "&" + key;

        // Output format
        String output = "json";

        // Building the url to the web service

        return "https://maps.googleapis.com/maps/api/place/details/" + output + "?" + parameters;
    }

    /**
     * A method to download json data from url
     */
    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuilder sb = new StringBuilder();

            String line;
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();
//            Log.d("downloadUrl : ", data);

            br.close();

        } catch (Exception e) {
            Debug.d("Exception download url", e.toString());
        } finally {
            if (iStream != null) {
                iStream.close();
                urlConnection.disconnect();
            }
        }
        return data;
    }

    // Fetches data from url passed
    private class DownloadTask extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... url) {

            // For storing data from web service
            String data = "";

            try {
                // Fetching the data from web service
                data = downloadUrl(url[0]);
            } catch (Exception e) {
                Debug.d("Background Task", e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            //setProgress(false);
            Debug.d("DownloadTask : ", result);
            // Creating ParserTask for parsing Google Places
            DetailParserTask detailParserTask = new DetailParserTask();

            // Starting Parsing the JSON string
            // This causes to execute doInBackground() of ParserTask class
            detailParserTask.execute(result);
        }
    }


    ArrayList<PlaceLocation> locationList = null;

    private class DetailParserTask extends AsyncTask<String, Integer, ArrayList<PlaceLocation>> {

        @Override
        protected ArrayList<PlaceLocation> doInBackground(String... jsonData) {

            JSONObject jObject;

            try {
                jObject = new JSONObject(jsonData[0]);

                PlaceDetailsJSONParser placeDetailsJsonParser = new PlaceDetailsJSONParser();
                // Getting the parsed data as a List construct
                locationList = placeDetailsJsonParser.getLocation(jObject);

            } catch (Exception e) {
                Debug.d("Exception", e.toString());
            }
            return locationList;
        }

        @Override
        protected void onPostExecute(ArrayList<PlaceLocation> result) {
            PlaceLocation hm = result.get(0);
            Debug.d(getClass().getSimpleName(), "" + hm.getLatitude());
            setUpMapIfNeeded(hm.getLatitude(), hm.getLongitude());
        }
    }

    private void setUpMapIfNeeded(Double latitude, Double longitude) {
        if (googleMap != null) {
            googleMap.clear();
            googleMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(latitude, longitude)));
            googleMap.animateCamera(CameraUpdateFactory.zoomTo(12), 2000, null);
        }
    }

    /**
     * ReverseGeocodingTask used to get Address from latitude and longitude, take context and lat lng
     */
    private class ReverseGeocodingTask extends AsyncTask<LatLng, Void, String> {
        Context mContext;

        public ReverseGeocodingTask(Context context) {
            super();
            mContext = context;
        }

        // Finding address using reverse Geo Coding
        @Override
        protected String doInBackground(LatLng... params) {
            StringBuilder result = new StringBuilder();
            Geocoder geocoder = new Geocoder(mContext);
            double latitude = params[0].latitude;
            double longitude = params[0].longitude;

            Debug.i("latitude is : ", "" + latitude + "longitude is :" + longitude);

            List<Address> addresses;
            try {
                addresses = geocoder.getFromLocation(latitude, longitude, 1);
                if (addresses.size() > 0) {
                    Address address = addresses.get(0);

                    String city = address.getLocality();
                    String postal_code = address.getPostalCode();
                    String country = address.getCountryName();
                    String data = address.getMaxAddressLineIndex() > 0 ? address.getAddressLine(0) : "";
                    String state = address.getAdminArea();

                    if (!TextUtils.isNullOrEmpty(data)) {
                        result.append(data).append(", ");
                    }

                    if (!TextUtils.isNullOrEmpty(city)) {
                        result.append(city).append(", ");
                    }

                    if (!TextUtils.isNullOrEmpty(country)) {
                        result.append(country).append(", ");
                    }

                    if (!TextUtils.isNullOrEmpty(state)) {
                        result.append(state).append(", ");
                    }

                    if (!TextUtils.isNullOrEmpty(postal_code)) {
                        result.append(postal_code);
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return result.toString();
        }

        @Override
        protected void onPostExecute(String addressText) {
            Debug.d("post Address", addressText);
            tv_address.setText(addressText);
            tv_address.setSelection(tv_address.getText().length());
        }
    }
}
