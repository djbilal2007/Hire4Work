package com.hire4work.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;

import com.hire4work.R;
import com.hire4work.base.BaseAppCompatActivity;
import com.hire4work.customewidgets.TouchImageView;
import com.hire4work.utills.Constant;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

/**
 * Created by Kailash Chouhan (Ratufa Technologies)
 * Email: kailash.chouhan@ratufa.com
 */
public class ImageDisplayActivity extends BaseAppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_display);

        TouchImageView touchImageView = (TouchImageView) findViewById(R.id.touchImageView);

        /*final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.show();
*/
        showLoading();
        String imageURL = getIntent().getStringExtra(Constant.DATA);

        Log.d("PostJobImageUrl", "" + imageURL);

        //displayImage(getActivity(), touchImageView, imageURL);
        if (imageURL != null && imageURL.length() != 0) {
            Picasso.with(this)
                    .load(imageURL)
                    .into(touchImageView, new Callback() {
                        @Override
                        public void onSuccess() {
                            //progressDialog.dismiss();
                            hideLoading();
                        }
                        @Override
                        public void onError() {
                        }
                    });
        }
    }
}
