package com.hire4work.activity;

import android.app.NotificationManager;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.hire4work.R;
import com.hire4work.adapter.ChatMessageAdapter;
import com.hire4work.base.BaseAppCompatActivity;
import com.hire4work.bean.ChatMessage;
import com.hire4work.bean.NotificationCounter;
import com.hire4work.utills.Constant;
import com.hire4work.utills.Debug;
import com.hire4work.utills.TextUtils;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.util.ArrayList;
import java.util.List;

import static com.hire4work.utills.UserConstant.NAME;

/**
 * Created by Kailash Chouhan (Ratufa Technologies)
 * Email: kailash.chouhan@ratufa.com
 */
public class ChatActivity extends BaseAppCompatActivity {

    private static final String TAG = ChatActivity.class.getName();
    private ListView lvChat;
    private TextView btn_send, tv_chat_user_name;
    private EditText edt_message;
    private ArrayList<ChatMessage> mMessagesList = new ArrayList<>();
    private ChatMessageAdapter mAdapter;
    // Create a handler which can run code periodically
    private Handler handler = new Handler();
    // Keep track of initial load to scroll to the bottom of the ListView
    private boolean mFirstLoad;
    private ParseUser worker, sUserId;
    private String jobId;

    private int REFRESH_INTERVAL = 1000;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        String sWorkerId = getIntent().getStringExtra(Constant.ID);
        Debug.d(TAG, sWorkerId);

        sUserId = ParseUser.getCurrentUser();
        jobId = getIntent().getStringExtra(Constant.NAME);

        ParseQuery<ParseUser> query = ParseUser.getQuery();
        query.getInBackground(sWorkerId, new GetCallback<ParseUser>() {
            @Override
            public void done(ParseUser parseUser, ParseException e) {
                if (e == null) {
                    showLoading();
                    worker = parseUser;
                    tv_chat_user_name = (TextView) findViewById(R.id.tv_chat_user_name);
                    tv_chat_user_name.setText(worker.getString(NAME));

                    receiveMessage();
                    mAdapter = new ChatMessageAdapter(ChatActivity.this, worker, mMessagesList);

                    lvChat = (ListView) findViewById(R.id.lvChat);
                    lvChat.setTranscriptMode(1);
                    lvChat.setAdapter(mAdapter);

                    // Run the runnable object defined every 1000ms
                    handler.postDelayed(runnable, REFRESH_INTERVAL);
                }
            }
        });

        mFirstLoad = true;

        edt_message = (EditText) findViewById(R.id.edt_message);

        btn_send = (TextView) findViewById(R.id.btn_send);
        btn_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyBoard(btn_send);
                if (isInternetConnected()) {
                    String message = edt_message.getText().toString().trim();
                    if (TextUtils.isNullOrEmpty(message)) {
                        return;
                    }

                    sendMessage(message, sUserId, worker);
                    edt_message.setText("");
                } else {
                    showToast(getString(R.string.internet));
                }
            }
        });

        ImageView left_buton = (ImageView) findViewById(R.id.left_buton);
        left_buton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    // Defines a runnable which is run every 1000ms
    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            receiveMessage();
            handler.postDelayed(this, REFRESH_INTERVAL);
        }
    };
    // Query messages from Parse so we can load them into the chat adapter
    private void receiveMessage() {
        ArrayList<ParseUser> strings = new ArrayList<>();
        strings.add(sUserId);
        strings.add(worker);

        ParseQuery<ChatMessage> query = new ParseQuery<>(ChatMessage.class);
        query.orderByAscending("createdAt");
        query.whereEqualTo("jobId", jobId);
        query.whereContainedIn("receiverId", strings);
        query.whereContainedIn("senderId", strings);
        query.findInBackground(new FindCallback<ChatMessage>() {
            @Override
            public void done(List<ChatMessage> list, ParseException e) {
                hideLoading();
                if (e == null) {
                    if (!TextUtils.isNullOrEmpty(list)) {
                        mMessagesList.clear();
                        mMessagesList.addAll(list);
                        mAdapter.notifyDataSetChanged();
                        if (mFirstLoad) {
                            lvChat.setSelection(mAdapter.getCount() - 1);
                            mFirstLoad = false;
                        }
                    }
                }
            }
        });
    }

    private void sendMessage(final String message, final ParseUser senderId, final ParseUser receiverId) {
        showLoading();
        ChatMessage chatMessage = new ChatMessage();
        chatMessage.setCommentText(message);
        chatMessage.setSenderId(senderId);
        chatMessage.setJobId(jobId);
        chatMessage.setReceiverId(receiverId);
        chatMessage.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null) {
                    //Log.d("ChatActivity", " e is null");
                    updateNotificationCounter();
                }
                else {
                    Log.d("ChatActivity", "" + e.getMessage());
                }
            }
        });
    }

    private void updateNotificationCounter() {
        ParseQuery<NotificationCounter> query = new ParseQuery<>(NotificationCounter.class);
        query.whereEqualTo("jobId", jobId);
        query.whereEqualTo("receiverId", worker.getObjectId());
        query.findInBackground(new FindCallback<NotificationCounter>() {
            @Override
            public void done(List<NotificationCounter> objects, ParseException e) {
                hideLoading();
                if (e == null) {
                    //objects is null
                    if (!TextUtils.isNullOrEmpty(objects)) {
                        try {
                            NotificationCounter counter = objects.get(0);
                            counter.setBedgeCount((Integer.valueOf(counter.getBedgeCount()) + 1) + "");
                            counter.saveInBackground();
                            receiveMessage();
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }
                }
            }
        });
    }
}
