package com.hire4work.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import com.hire4work.R;
import com.hire4work.base.BaseAppCompatActivity;
import com.hire4work.bean.PersonData;
import com.hire4work.utills.TextUtils;
import com.hire4work.utills.Utils;
import com.parse.ParseAnalytics;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SignUpCallback;

/**
 * Created by Kailash Chouhan (Ratufa Technologies)
 * Email: kailash.chouhan@ratufa.com
 */
public class RegistrationActivity extends BaseAppCompatActivity implements View.OnClickListener, TextWatcher {

    private EditText nameEdt, emailEdt, passwordEdt, confirmPasswordEdt, edt_city, edt_country;
    private String name;
    private String email;
    private String password;
    private String city;
    private String country;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        ParseAnalytics.trackAppOpenedInBackground(getIntent());

        ImageButton backBtn = (ImageButton) findViewById(R.id.backBtn);
        backBtn.setOnClickListener(this);

        Button registerBtn = (Button) findViewById(R.id.registerBtn);
        registerBtn.setOnClickListener(this);

        emailEdt = (EditText) findViewById(R.id.emailEdt);
        emailEdt.addTextChangedListener(this);

        passwordEdt = (EditText) findViewById(R.id.passwordEdt);
        passwordEdt.addTextChangedListener(this);

        confirmPasswordEdt = (EditText) findViewById(R.id.confirmPasswordEdt);
        confirmPasswordEdt.addTextChangedListener(this);

        nameEdt = (EditText) findViewById(R.id.nameEdt);
        nameEdt.addTextChangedListener(this);

        edt_city = (EditText) findViewById(R.id.edt_city);
        edt_city.addTextChangedListener(this);

        edt_country = (EditText) findViewById(R.id.edt_country);
        edt_country.addTextChangedListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.backBtn:
                onBackPressed();
                break;

            case R.id.registerBtn:
                hideKeyBoard(v);
                if (isInternetConnected()) {
                    checkForRegistration();
                } else {
                    showAlertDialog(getString(R.string.internet));
                }
                break;

            default:
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    public void checkForRegistration() {
        name = nameEdt.getText().toString();
        email = emailEdt.getText().toString();
        password = passwordEdt.getText().toString();
        String confirmPassword = confirmPasswordEdt.getText().toString();
        city = edt_city.getText().toString();
        country = edt_country.getText().toString();

        if (TextUtils.isNullOrEmpty(name)) {
            nameEdt.setSelection(name.length());
            nameEdt.requestFocus();
            nameEdt.setError(getResources().getString(R.string.username_required));
        } else if (TextUtils.isNullOrEmpty(email)) {
            emailEdt.setSelection(email.length());
            emailEdt.requestFocus();
            emailEdt.setError(getResources().getString(R.string.email_required));
        } else if (!Utils.isValidEmailAddress(email)) {
            emailEdt.setSelection(email.length());
            emailEdt.requestFocus();
            emailEdt.setError(getResources().getString(R.string.invalid_email));
        } else if (!Utils.isValidPassword(password)) {
            passwordEdt.setSelection(password.length());
            passwordEdt.requestFocus();
            passwordEdt.setError(getResources().getString(R.string.password_required));
        } else if (TextUtils.isNullOrEmpty(confirmPassword)) {
            confirmPasswordEdt.setSelection(confirmPassword.length());
            confirmPasswordEdt.requestFocus();
            confirmPasswordEdt.setError(getResources().getString(R.string.confirm_password_required));
        } else if (!password.equals(confirmPassword)) {
            confirmPasswordEdt.setSelection(confirmPassword.length());
            confirmPasswordEdt.requestFocus();
            confirmPasswordEdt.setError(getResources().getString(R.string.password_does_not_match));
        } else if (TextUtils.isNullOrEmpty(country)) {
            edt_country.setSelection(country.length());
            edt_country.requestFocus();
            edt_country.setError(getResources().getString(R.string.country_required));
        } else if (TextUtils.isNullOrEmpty(city)) {
            edt_city.setSelection(city.length());
            edt_city.requestFocus();
            edt_city.setError(getResources().getString(R.string.city_required));
        } else {
            if (isInternetConnected()) {
                registrationTask();
            } else {
                showAlertDialog("Internet Connection", getResources().getString(R.string.chk_your_network_connection));
            }
        }
    }

    public void registrationTask() {
        showLoading(getString(R.string.please_wait));

        PersonData user = new PersonData();
        user.setUsername(email);
        user.setPassword(password);
        user.setEmail(email);
        user.put("isActive", "Active");
//        user.put("name", name);
//        user.put("deviceType", "android");
//        user.put("city", city);
//        user.put("country", country);
        user.setName(name);
        user.setDeviceType("Android");
        user.setCity(city);
        user.setCountry(country);

        user.signUpInBackground(new SignUpCallback() {
            @Override
            public void done(ParseException e) {
                hideLoading();
                if (e == null) {
                    ParseUser.logOut();
                    showRegAlertDialog("Registration Success", getString(R.string.reg_succ_message));
                } else {
                    showAlertDialog(e.getMessage());
                }
            }
        });
    }

    public void showRegAlertDialog(String title, String message) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(RegistrationActivity.this);
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setPositiveButton("Ok",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                        finish();
                    }
                });
        alertDialog.show();
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        if (nameEdt.getText().length() > 0) {
            nameEdt.setError(null);
        }

        if (emailEdt.getText().length() > 0) {
            emailEdt.setError(null);
        }

        if (passwordEdt.getText().length() > 0) {
            passwordEdt.setError(null);
        }

        if (confirmPasswordEdt.getText().length() > 0) {
            confirmPasswordEdt.setError(null);
        }

        if (edt_country.getText().length() > 0) {
            edt_country.setError(null);
        }

        if (edt_city.getText().length() > 0) {
            edt_city.setError(null);
        }
    }

    @Override
    public void afterTextChanged(Editable s) {

    }
}
