package com.hire4work.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.hire4work.R;
import com.hire4work.base.BaseAppCompatActivity;
import com.hire4work.utills.Debug;
import com.hire4work.utills.Utils;

/**
 * Created by Kailash Chouhan (Ratufa Technologies)
 * Email: kailash.chouhan@ratufa.com
 */
public class TwitterAuth extends BaseAppCompatActivity {

    static final String TWITTER_CALLBACK_URL = "oauth://Hire4HourLogin";

    private static final String URL_TWITTER_OAUTH_VERIFIER = "oauth_verifier";

    public static String EXTRA_URL = "extra_url";

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.twitter_auth);
        Utils.initThreadPolicy();

        final String url = this.getIntent().getStringExtra(TwitterAuth.EXTRA_URL);
        if (url == null) {
            Debug.e("Twitter", "URL cannot be null");
            finish();
        }

        WebView webView = (WebView) findViewById(R.id.webView);
        webView.setWebViewClient(new MyWebViewClient());
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        webView.loadUrl(url);
    }

    class MyWebViewClient extends WebViewClient {

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            showLoading();
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {

            if (url.startsWith(TwitterAuth.TWITTER_CALLBACK_URL)) {
                Uri uri = Uri.parse(url);
                /* Sending results back */
                String verifier = uri.getQueryParameter(TwitterAuth.URL_TWITTER_OAUTH_VERIFIER);
                Intent resultIntent = new Intent();
                resultIntent.putExtra(TwitterAuth.URL_TWITTER_OAUTH_VERIFIER, verifier);
                setResult(RESULT_OK, resultIntent);
                /* closing WebView */
                finish();

                return true;
            }
            return false;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            hideLoading();
        }
    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        super.onBackPressed();
//        overridePendingTransition(R.anim.trans_right_in,
//                R.anim.trans_right_out);
        finish();

    }

}
