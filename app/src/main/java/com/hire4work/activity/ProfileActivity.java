package com.hire4work.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.RatingBar;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;
import com.hire4work.R;
import com.hire4work.base.BaseAppCompatActivity;
import com.hire4work.bean.JobApply;
import com.hire4work.bean.PersonData;
import com.hire4work.bean.PostJob;
import com.hire4work.bean.UserRatting;
import com.hire4work.customewidgets.BezelImageView;
import com.hire4work.customewidgets.ExpandableHeightListView;
import com.hire4work.utills.Constant;
import com.hire4work.utills.Debug;
import com.hire4work.utills.TextUtils;
import com.hire4work.utills.UserConstant;
import com.hire4work.utills.Utils;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kailash Chouhan (Ratufa Technologies)
 * Email: kailash.chouhan@ratufa.com
 */
public class ProfileActivity extends BaseAppCompatActivity {

    private BezelImageView iv_user_image;
    private TextView tv_user_name;
    private TextView tv_user_category;
    private TextView tv_jobs;
    private TextView tv_clients;
    private TextView tv_reviews;
    private TextView tv_skills;
    private TextView tv_address;
    private RatingBar rb_user_rating;

    private List<UserRatting> ratingList = new ArrayList<>();
    private UserRatingAdapter adapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_profile);

        String workerId = getIntent().getStringExtra(Constant.DATA);

        ImageView iv_back = (ImageView) findViewById(R.id.iv_back);
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        iv_user_image = (BezelImageView) findViewById(R.id.iv_user_image);

        tv_user_name = (TextView) findViewById(R.id.tv_user_name);

        tv_user_category = (TextView) findViewById(R.id.tv_user_category);

//        TextView tv_earnings = (TextView) findViewById(R.id.tv_earnings);

        tv_jobs = (TextView) findViewById(R.id.tv_jobs);

        tv_clients = (TextView) findViewById(R.id.tv_clients);

        tv_reviews = (TextView) findViewById(R.id.tv_reviews);

        tv_skills = (TextView) findViewById(R.id.tv_skills);

        tv_address = (TextView) findViewById(R.id.tv_address);

        rb_user_rating = (RatingBar) findViewById(R.id.rb_user_rating);

        ExpandableHeightListView list_review = (ExpandableHeightListView) findViewById(R.id.list_review);
        list_review.setExpanded(true);
        adapter = new UserRatingAdapter(ProfileActivity.this, ratingList);
        list_review.setAdapter(adapter);
        list_review.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                UserRatting userRatting = ratingList.get(position);
                reviewPopup(view, userRatting);
            }
        });

        getUserDetail(workerId);
        getUserRating(workerId);
        getUserJobCount(workerId);
        getUserClientCount(workerId);
        hideLoading();
    }

    @SuppressLint("InflateParams")
    private void reviewPopup(View view, UserRatting ratting) {
        final PopupWindow popup = new PopupWindow(getActivity());
        View layout = getActivity().getLayoutInflater().inflate(R.layout.popup_review, null);
        popup.setContentView(layout);
        // Set content width and height
        popup.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        popup.setWidth(Utils.getDeviceWidth(getActivity()) - 80);
        // Closes the popup window when touch outside of it - when looses focus
        popup.setOutsideTouchable(true);
        popup.setFocusable(true);
        // Show anchored to button
        popup.setBackgroundDrawable(new BitmapDrawable());
        popup.showAtLocation(view, Gravity.CENTER, 0, 0);

        RatingBar ratingBar = (RatingBar) layout.findViewById(R.id.rating_bar);
        ratingBar.setRating(ratting.getRatingPoint().floatValue());

        BezelImageView iv_user_image = (BezelImageView) layout.findViewById(R.id.iv_user_image);
        if (ratting.getuserId().getParseFile(UserConstant.PICTURE) != null) {
            displayImage(getActivity(), iv_user_image, ratting.getuserId().getParseFile(UserConstant.PICTURE).getUrl(), ratting.getuserId().getString(UserConstant.NAME));
        }

        TextView tv_user_name = (TextView) layout.findViewById(R.id.tv_user_name);
        tv_user_name.setText(ratting.getuserId().getString(UserConstant.NAME));

        TextView tv_review = (TextView) layout.findViewById(R.id.tv_review);
        tv_review.setText(ratting.getRatingDescription());

        Button btn_close = (Button) layout.findViewById(R.id.btn_close);
        btn_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
            }
        });
    }

    private void getUserDetail(String id) {
        showLoading();
        ParseQuery<PersonData> query = ParseQuery.getQuery(PersonData.class);
        query.getInBackground(id, new GetCallback<PersonData>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void done(PersonData personData, com.parse.ParseException e) {
                //hideLoading();
                if (e == null) {
                    if (!TextUtils.isNullOrEmpty(personData.getImage())) {
                        displayImage(getActivity(), iv_user_image, personData.getImage(), personData.getName());
                    }

                    List<String> stringArrayList = personData.getProffesion();
                    if (!TextUtils.isNullOrEmpty(stringArrayList)) {
                        String s = "";
                        for (int i = 0; i < stringArrayList.size(); i++) {
                            s = s + stringArrayList.get(i) + ", ";
                        }
                        tv_user_category.setText(s);
                    }

                    tv_user_name.setText(personData.getName());
                    tv_clients.setText(getActivity().getString(R.string.clients) + "\n");
                    tv_skills.setText(personData.getSkills());

                    // Adding Marker on the touched location with address
                    new ReverseGeocodingTask(personData.getCurrentLocation()).execute();
                }
            }
        });
    }

    private void getUserRating(String workerId) {
        ParseQuery<UserRatting> rattingParseQuery = new ParseQuery<>(UserRatting.class);
        rattingParseQuery.whereEqualTo("workerId", workerId);
        rattingParseQuery.include("userId");
        rattingParseQuery.findInBackground(new FindCallback<UserRatting>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void done(List<UserRatting> list, ParseException e) {
                if (e == null) {
                    int ratingNumber = 0;
                    if (!TextUtils.isNullOrEmpty(list)) {
                        for (int i = 0; i < list.size(); i++) {
                            ratingNumber = ratingNumber + list.get(i).getRatingPoint().intValue();
                        }

                        ratingNumber = ratingNumber / list.size();
                        ratingList.addAll(list);
                        adapter.notifyDataSetChanged();
                    }
                    rb_user_rating.setRating(ratingNumber);
                    tv_reviews.setText(getActivity().getString(R.string.reviews) + "\n" + list.size());
                }
            }
        });
    }

    private void getUserJobCount(String workerId) {
        ParseQuery<ParseUser> parseQuery = ParseUser.getQuery();
        parseQuery.getInBackground(workerId, new GetCallback<ParseUser>() {
            @Override
            public void done(ParseUser parseUser, ParseException e) {
                ParseQuery<PostJob> query = new ParseQuery<>(PostJob.class);
                query.whereEqualTo("userId", parseUser);
                query.findInBackground(new FindCallback<PostJob>() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void done(List<PostJob> list, ParseException e) {
                        if (e == null) {
                            tv_jobs.setText(getActivity().getString(R.string.jobs) + "\n" + list.size());
                        }
                    }
                });
            }
        });
    }

    private void getUserClientCount(String workerId) {
        ParseQuery<ParseUser> parseQuery = ParseUser.getQuery();
        parseQuery.getInBackground(workerId, new GetCallback<ParseUser>() {
            @Override
            public void done(ParseUser parseUser, ParseException e) {
                List<ParseUser> userList = new ArrayList<>();
                userList.add(parseUser);
                ParseQuery<JobApply> query = new ParseQuery<>(JobApply.class);
                query.whereContainedIn("applyUserId", userList);
                query.whereEqualTo("assignedUserStatus", "YES");
                query.findInBackground(new FindCallback<JobApply>() {
                    @Override
                    public void done(List<JobApply> objects, ParseException e) {
                        tv_clients.setText(getActivity().getString(R.string.clients) + "\n" + removeDuplicateFromList(objects).size());

                    }
                });
            }
        });
    }

    public List<JobApply> removeDuplicateFromList(List<JobApply> list) {
        int s = 0;
        List<JobApply> users = new ArrayList<>();
        for (JobApply us1 : list) {
            for (JobApply us2 : users) {
                if (us1.getPostUserId().getObjectId().equals(us2.getPostUserId().getObjectId())) {
                    s = 1;
                } else {
                    s = 0;
                }
            }
            if (s == 0) {
                users.add(us1);
            }
        }
        return users;
    }

    public class UserRatingAdapter extends ArrayAdapter<UserRatting> {
        List<UserRatting> ratingList;

        public UserRatingAdapter(Context context, List<UserRatting> list) {
            super(context, R.layout.item_user_rating, list);
            this.ratingList = new ArrayList<>();
            this.ratingList.addAll(list);
        }

        @Override
        public View getView(int pos, View convertView, ViewGroup parent) {
            final ViewHolder holder;

            if (convertView == null) {
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_user_rating, parent, false);

                holder = new ViewHolder();

                holder.iv_user_image = (BezelImageView) convertView.findViewById(R.id.iv_user_image);

                holder.tv_user_name = (TextView) convertView.findViewById(R.id.tv_user_name);

                holder.tv_user_review = (TextView) convertView.findViewById(R.id.tv_user_review);

                holder.rating_bar = (RatingBar) convertView.findViewById(R.id.rating_bar);

                convertView.setTag(holder);

            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            try {
                UserRatting userRatting = getItem(pos);

                if (userRatting.getuserId().getParseFile(UserConstant.PICTURE) != null) {
                    displayImage(getActivity(), holder.iv_user_image, userRatting.getuserId().getParseFile(UserConstant.PICTURE).getUrl(), userRatting.getuserId().getString(UserConstant.NAME));
                }

                holder.tv_user_name.setText(userRatting.getuserId().getString(UserConstant.NAME));

                if (!TextUtils.isNullOrEmpty(userRatting.getRatingDescription())) {
                    holder.tv_user_review.setText(userRatting.getRatingDescription());
                }

                if (userRatting.getRatingPoint() != null) {
                    holder.rating_bar.setRating(userRatting.getRatingPoint().floatValue());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return convertView;
        }

        class ViewHolder {
            BezelImageView iv_user_image;
            TextView tv_user_name, tv_user_review;
            RatingBar rating_bar;
        }
    }

    /**
     * ReverseGeocodingTask used to get Address from latitude and longitude, take context and lat lng
     */
    private class ReverseGeocodingTask extends AsyncTask<LatLng, Void, String> {

        double latitude;
        double longitude;

        public ReverseGeocodingTask(ParseGeoPoint currentLocation) {
            this.latitude = currentLocation.getLatitude();
            this.longitude = currentLocation.getLongitude();
        }

        // Finding address using reverse Geo Coding
        @Override
        protected String doInBackground(LatLng... params) {
            StringBuilder result = new StringBuilder();
            Geocoder geocoder = new Geocoder(getActivity());

            Debug.i("latitude is : ", "" + latitude + "longitude is :" + longitude);

            List<Address> addresses;
            try {
                addresses = geocoder.getFromLocation(latitude, longitude, 1);
                if (addresses.size() > 0) {
                    Address address = addresses.get(0);

                    String city = address.getLocality();
                    String postal_code = address.getPostalCode();
                    String country = address.getCountryName();
                    String data = address.getMaxAddressLineIndex() > 0 ? address.getAddressLine(0) : "";
                    String state = address.getAdminArea();

                    if (!TextUtils.isNullOrEmpty(data)) {
                        result.append(data).append(", ");
                    }

                    if (!TextUtils.isNullOrEmpty(city)) {
                        result.append(city).append(", ");
                    }

                    if (!TextUtils.isNullOrEmpty(country)) {
                        result.append(country).append(", ");
                    }

                    if (!TextUtils.isNullOrEmpty(state)) {
                        result.append(state).append(", ");
                    }

                    if (!TextUtils.isNullOrEmpty(postal_code)) {
                        result.append(postal_code);
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return result.toString();
        }

        @Override
        protected void onPostExecute(String addressText) {
            Debug.d("post Address", addressText);
            tv_address.setText(addressText);
        }
    }
}
