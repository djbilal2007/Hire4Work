package com.hire4work.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.hire4work.R;
import com.hire4work.base.BaseAppCompatActivity;
import com.hire4work.bean.WorkCategory;
import com.hire4work.bean.appenum.LoginType;
import com.hire4work.bean.appenum.LoginWay;
import com.hire4work.utills.Constant;
import com.hire4work.utills.SharedPrefrenceManager;
import com.hire4work.utills.TextUtils;
import com.hire4work.utills.UserConstant;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kailash Chouhan (Ratufa Technologies)
 * Email: kailash.chouhan@ratufa.com
 */
public class CategoryActivity extends BaseAppCompatActivity implements View.OnClickListener {

    private TextView tv_employer, tv_employee;
    private LoginType mLoginType;
    private LoginWay loginWay;
    private CharSequence[] myList;
    private ArrayList<Integer> selList = new ArrayList();
    private ArrayList<String> list = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);

        mLoginType = (LoginType) getIntent().getSerializableExtra(Constant.USER_TYPE);
        loginWay = (LoginWay) getIntent().getSerializableExtra(Constant.DATA);

        tv_employer = (TextView) findViewById(R.id.tv_employer);
        tv_employer.setOnClickListener(this);

        tv_employee = (TextView) findViewById(R.id.tv_employee);
        tv_employee.setOnClickListener(this);

        getCategory();
    }

    private void showCategory() {
        try {

            if(myList!=null) {
                if (myList.length != 0) {
                    final AlertDialog.Builder ad = new AlertDialog.Builder(this);
                    ad.setTitle(getString(R.string.select_cate));
                    ad.setMultiChoiceItems(myList, new boolean[myList.length], new DialogInterface.OnMultiChoiceClickListener() {
                        @Override
                        public void onClick(DialogInterface arg0, int position, boolean isChecked) {
                            // TODO Auto-generated method stub
                            if (isChecked) {
                                // If user select a item then add it in selected items
                                selList.add(position);
                            } else if (selList.contains(position)) {
                                // if the item is already selected then remove it
                                selList.remove(Integer.valueOf(position));
                            }
                        }
                    });
                    ad.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // TODO Auto-generated method stub
                            for (int i = 0; i < selList.size(); i++) {
                                list.add(i, myList[selList.get(i)].toString());
                            }

                            if (TextUtils.isNullOrEmpty(selList)) {
                                showToast("Select at least one category.");
                            } else {
                                UpdateView(list);
                            }
                        }
                    });
                    ad.show();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private SharedPrefrenceManager sharedPrefrenceManager = SharedPrefrenceManager.getInstance();

    private void UpdateView(ArrayList<String> sJobCategory) {
        if (isInternetConnected()) {
            if (isLogin()) {
                updateJobCategory(sJobCategory);
            } else {
                sharedPrefrenceManager.setPrefrenceList(CategoryActivity.this, sJobCategory, "CATEGORY");
                if (isInAppLogin()) {
                    finish();
                } else {
                    startActivity(new Intent(CategoryActivity.this, HomeActivity.class));
                    finish();
                }
            }
        } else {
            showToast(getString(R.string.internet));
        }
    }

    public void updateJobCategory(final ArrayList<String> category) {
        showLoading(getString(R.string.please_wait));
        final ParseUser user = ParseUser.getCurrentUser();
        user.put(UserConstant.PROFESSION, category);
        user.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                hideLoading();
                if (e == null) {
                    Log.i("CategoryActivity","CategoryActivity: This block is running. Username: " + user.getUsername());
                    startActivity(new Intent(CategoryActivity.this, HomeActivity.class));
                    finish();
                }
                else {
                    Log.i("CategoryActivity","CategoryActivity: This block is running. " + e.getMessage());
                }
            }
        });
    }

    private boolean isLogin() {
        return mLoginType == LoginType.LOGIN;
    }

    private boolean isInAppLogin() {
        return loginWay == LoginWay.IN_APP_LOGIN;
    }

    public void getCategory() {
        showLoading(getString(R.string.please_wait));
        ParseQuery<WorkCategory> query = new ParseQuery<WorkCategory>(WorkCategory.class);
        query.orderByAscending("jobCategory");
        query.findInBackground(new FindCallback<WorkCategory>() {
            @Override
            public void done(List<WorkCategory> list, ParseException e) {
                hideLoading();
                if (e == null) {
                    List<String> list1 = new ArrayList<String>();
                    for (int i = 0; i < list.size(); i++) {
                        list1.add(list.get(i).getjobCategory());
                    }
                    myList = list1.toArray(new CharSequence[list1.size()]);
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_employee:
                showCategory();
                break;

            case R.id.tv_employer:
                list.clear();
                list.add(getString(R.string.employer));
                UpdateView(list);
                break;

            default:
                break;
        }
    }
}