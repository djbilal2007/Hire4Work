package com.hire4work.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;

import com.hire4work.R;
import com.hire4work.base.BaseAppCompatActivity;
import com.hire4work.bean.appenum.LoginType;
import com.hire4work.bean.appenum.LoginWay;
import com.hire4work.utills.Constant;
import com.hire4work.utills.Debug;
import com.hire4work.utills.Keys;
import com.hire4work.utills.SharedPrefrenceManager;
import com.hire4work.utills.TextUtils;
import com.hire4work.utills.UserConstant;
import com.hire4work.utills.Utils;
import com.parse.ParseUser;

import org.apache.commons.net.ntp.NTPUDPClient;
import org.apache.commons.net.ntp.TimeInfo;

import java.io.IOException;
import java.net.InetAddress;
import java.util.Date;

/**
 * Created by Kailash Chouhan (Ratufa Technologies)
 * Email: kailash.chouhan@ratufa.com
 */
public class SplashActivity extends BaseAppCompatActivity {

    private SharedPrefrenceManager sharedPrefrenceManager = SharedPrefrenceManager.getInstance();
    private LocationManager manager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        Utils.initThreadPolicy();
//        getParseTime();
        Debug.d(getClass().getSimpleName(), Utils.printKeyHash(SplashActivity.this));
//        getCurrentNetworkTime();
        manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            buildAlertMessageNoGps();
        } else {
            goAhead();
        }
    }

    private void goAhead() {
        Handler mHandler = new Handler();
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent;
                if (sharedPrefrenceManager.getSavedPrefrenceValue(SplashActivity.this, "false", Keys.IS_LOGIN).equalsIgnoreCase("false")) {
                    intent = new Intent(SplashActivity.this, LoginActivity.class);
                    intent.putExtra(Constant.DATA, LoginWay.INITIAL_LOGIN);
                } else {
                    ParseUser user = ParseUser.getCurrentUser();
                    if (TextUtils.isNullOrEmpty(user.getList(UserConstant.PROFESSION))) {
                        intent = new Intent(SplashActivity.this, CategoryActivity.class);
                        intent.putExtra(Constant.USER_TYPE, LoginType.LOGIN);
                        intent.putExtra(Constant.DATA, LoginWay.INITIAL_LOGIN);
                    } else {
                        intent = new Intent(SplashActivity.this, HomeActivity.class);
                    }
                }
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
            }
        }, 1000);
    }

    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.app_name));
        builder.setMessage("Your location service seems to be disabled. Please enable location service")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        dialog.cancel();
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {

                        dialog.cancel();
                        finish();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            buildAlertMessageNoGps();
        } else {
            goAhead();
        }
    }

    //NTP server list: http://tf.nist.gov/tf-cgi/servers.cgi
    public static final String TIME_SERVER = "time-a.nist.gov";

    public static void getCurrentNetworkTime() {
        long returnTime;
        try {
            NTPUDPClient timeClient = new NTPUDPClient();
            InetAddress inetAddress;
            inetAddress = InetAddress.getByName(TIME_SERVER);
            TimeInfo timeInfo = timeClient.getTime(inetAddress);
            //long returnTime = timeInfo.getReturnTime();   //local device time
            returnTime = timeInfo.getMessage().getTransmitTimeStamp().getTime();   //server time

            Date time = new Date(returnTime);
            Debug.d("SplashActivity", "Time from Server : " + TIME_SERVER + ": " + time);

            Debug.d("SplashActivity", "Time from Local : " + timeInfo.getReturnTime());
        } catch (IOException e) {
            e.printStackTrace();
        }
        //return returnTime;
    }
}
