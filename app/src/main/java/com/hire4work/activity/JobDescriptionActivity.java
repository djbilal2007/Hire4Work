package com.hire4work.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.hire4work.R;
import com.hire4work.base.BaseAppCompatActivity;
import com.hire4work.bean.JobApply;
import com.hire4work.bean.PostJob;
import com.hire4work.bean.PushNotification;
import com.hire4work.customewidgets.BezelImageView;
import com.hire4work.utills.Constant;
import com.hire4work.utills.Keys;
import com.hire4work.utills.SharedPrefrenceManager;
import com.hire4work.utills.TextUtils;
import com.hire4work.utills.UserConstant;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParsePush;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.parse.SendCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.hire4work.utills.Constant.JOB_APPLY;
import static com.hire4work.utills.UserConstant.NAME;
import static com.hire4work.utills.UserConstant.PICTURE;

/**
 * Created by Kailash Chouhan (Ratufa Technologies)
 * Email: kailash.chouhan@ratufa.com
 */
public class JobDescriptionActivity extends BaseAppCompatActivity implements View.OnClickListener {

    private GoogleMap googleMap;
    private TextView tv_job_title;
    private TextView job_category_address;
    private TextView tv_job_description;
    private TextView tv_postjob_title;
    private PostJob postJob;
    private EditText edt_job_price;
    private BezelImageView profileImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_job_description);

        getPostJobDetail(getIntent().getStringExtra(Constant.DATA));

        TextView cancelBtn = (TextView) findViewById(R.id.tvLeft);
        cancelBtn.setText(getActivity().getResources().getString(R.string.cancel));
        cancelBtn.setOnClickListener(this);

        TextView applyBtn = (TextView) findViewById(R.id.tvRight);
        applyBtn.setText(getActivity().getResources().getString(R.string.apply));
        applyBtn.setOnClickListener(this);

        ImageView left_buton = (ImageView) findViewById(R.id.left_buton);
        left_buton.setOnClickListener(this);

        tv_postjob_title = (TextView) findViewById(R.id.tv_postjob_title);

        tv_job_title = (TextView) findViewById(R.id.tv_job_title);

        job_category_address = (TextView) findViewById(R.id.job_category_address);

        tv_job_description = (TextView) findViewById(R.id.tv_job_description);

        TextView tv_image_attached = (TextView) findViewById(R.id.tv_image_attached);
        tv_image_attached.setOnClickListener(this);

        edt_job_price = (EditText) findViewById(R.id.edt_job_price);

        String charges;

//        if (ParseUser.getCurrentUser().getString(UserConstant.RATE).contains("$")) {
//            charges = ParseUser.getCurrentUser().getString(UserConstant.RATE).replace("$", "").trim();
//    }
        if(TextUtils.isNullOrEmpty(ParseUser.getCurrentUser().getString(UserConstant.RATE))){
              charges = "15";
        } else {
            charges = ParseUser.getCurrentUser().getString(UserConstant.RATE).trim();
        }

        edt_job_price.setText(charges);
//        edt_job_price.setSelection(charges.length());
        edt_job_price.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable mEdit) {
                if (mEdit.toString().length() > 0 && mEdit.toString().charAt(0) == '0') {
                    showToast("First letter must not be 0.");
                    edt_job_price.setText("");
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });

        profileImage = (BezelImageView) findViewById(R.id.profileImage);
    }

    private void getPostJobDetail(String iPostJob) {
        showLoading();
        ParseQuery<PostJob> postJobParseQuery = new ParseQuery<>(PostJob.class);
        postJobParseQuery.include("userId");
        postJobParseQuery.getInBackground(iPostJob, new GetCallback<PostJob>() {
            @Override
            public void done(PostJob object, ParseException e) {
                hideLoading();
                if (e == null) {
                    postJob = object;
                    tv_postjob_title.setText(postJob.getjobTitle());
                    tv_job_title.setText(postJob.getjobTitle());
                    job_category_address.setText(postJob.getjobCategory() + " | " + postJob.getjobAddress());
                    tv_job_description.setText(postJob.getjobDescription());
                    try {
                        if (postJob.getuserId().getParseFile(PICTURE) != null) {
                            displayImage(getActivity(), profileImage, postJob.getuserId().getParseFile(PICTURE).getUrl());
                        }
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                    UpdateViewsCount();
                    addGoogleMap();
                }
            }
        });
    }

    private void UpdateViewsCount() {
        //int count = Integer.valueOf(postJob.getviewsCount()) + 1;
        postJob.increment("viewsCount");
        postJob.saveInBackground();
    }

    private void addGoogleMap() {
        // check if we have got the googleMap already
        if (googleMap == null) {
            ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(GoogleMap map) {
                    googleMap = map;
                }
            });

            if (googleMap != null) {
                LatLng latLng = new LatLng(postJob.getjobLatLong().getLatitude(), postJob.getjobLatLong().getLongitude());
                googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                googleMap.animateCamera(CameraUpdateFactory.zoomTo(14), 2000, null);
                googleMap.getUiSettings().setAllGesturesEnabled(false);
                googleMap.addMarker(new MarkerOptions()
                        .position(new LatLng(postJob.getjobLatLong().getLatitude(), postJob.getjobLatLong().getLongitude()))
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.location_icon))
                        .title(postJob.getjobTitle()));
            }
        }
    }

    private SharedPrefrenceManager sharedPrefrenceManager = SharedPrefrenceManager.getInstance();

    @Override
    public void onClick(View v) {
        hideKeyBoard(v);
        switch (v.getId()) {

            case R.id.left_buton:
                finish();
                break;

            // Cancel Button
            case R.id.tvLeft:
                finish();
                break;

            // Apply Job Button
            case R.id.tvRight:
                if (isInternetConnected()) {
                    if (sharedPrefrenceManager.getSavedPrefrenceValue(getActivity(), "", Keys.IS_LOGIN).equalsIgnoreCase("true")) {
                        if (!TextUtils.isNullOrEmpty(ParseUser.getCurrentUser().getString(UserConstant.PAYPAL_USER_ID))) {
                            if (TextUtils.isNullOrEmpty(postJob.getAppliedUser())) {
                                applyJob();
                            } else {
                                if (isAlreadyApplied(postJob.getAppliedUser())) {
                                    showAlertDialog("You have already applied for this job.");
                                    return;
                                }
                                applyJob();
                            }
                        } else {
                            showAlertDialog("The user profile is incomplete.");
                        }
                    } else {
                        showLoginAlert();
                    }
                } else {
                    showAlertDialog(getString(R.string.internet));
                }

                break;

            case R.id.tv_image_attached:
                if (isInternetConnected()) {
                    if (postJob.getImage() != null) {
                        startActivity(new Intent(getActivity(), ImageDisplayActivity.class).putExtra(Constant.DATA, postJob.getImage()));
                    }else {
                        showToast("Image not available.");
                    }
                } else {
                    showAlertDialog(getString(R.string.internet));
                }
                break;

            default:
                break;
        }
    }

    public boolean isAlreadyApplied(List<ParseUser> appliedUser) {
        boolean b = false;
        for (int i = 0; i < appliedUser.size(); i++) {
            if (appliedUser.get(i).getObjectId().equalsIgnoreCase(ParseUser.getCurrentUser().getObjectId())) {
                b = true;
                break;
            } else {
                b = false;
            }
        }
        return b;
    }

    private void applyJob() {
        if (TextUtils.isNullOrEmpty(edt_job_price.getText().toString().trim())) {
            showAlertDialog(getString(R.string.app_name), "Please enter job price.");
        } else if (Integer.valueOf(edt_job_price.getText().toString().trim()) <= 15) {
            showAlertDialog(getString(R.string.app_name), "Job price should be more than $15.");
        } else if (edt_job_price.getText().toString().trim().length() > 4) {
            showAlertDialog("Job price should not exceed to 4 digits.");
        } else {
            showLoading();

            ParseUser parseUser = ParseUser.getCurrentUser();

            final List<ParseUser> userList = new ArrayList<>();
            userList.add(parseUser);

            final List<String> appliedUserNist = new ArrayList<>();
            appliedUserNist.add(parseUser.getString(NAME));

            JobApply jobApply = new JobApply();
            jobApply.setApplyUserId(parseUser);
            jobApply.setJobAppliedStatus("YES");
            jobApply.setAssignedUserStatus("NO");
            jobApply.setWorkRate(edt_job_price.getText().toString().trim());
            jobApply.setPostJobId(postJob);
            jobApply.setPostUserId(postJob.getuserId());
            jobApply.saveInBackground(new SaveCallback() {
                @Override
                public void done(ParseException e) {
                    if (e == null) {
                        postJob.setAppliedUser(userList);
                        postJob.setAppliedUserName(appliedUserNist);
                        postJob.saveInBackground();
                        sendNotification();
                    }
                }
            });
        }
    }

    private void sendNotification() {
        ParseQuery pushQuery = ParseInstallation.getQuery();
        pushQuery.whereEqualTo("channels", "global");
        pushQuery.whereEqualTo("installedUserId", postJob.getuserId().getObjectId());

        JSONObject jsonObject = new JSONObject();
        try {
            JSONObject data = new JSONObject();
            data.put("badge", "0");
            data.put("alert", ParseUser.getCurrentUser().getString(UserConstant.NAME) + " applied on job " + postJob.getjobTitle());

            jsonObject.put("type", JOB_APPLY);
            jsonObject.put("jobId", postJob.getObjectId());
            jsonObject.put("aps", data);
            jsonObject.put("workerId", ParseUser.getCurrentUser().getObjectId());
            jsonObject.put("userId", postJob.getuserId().getObjectId());
        } catch (JSONException e) {
            e.printStackTrace();
            Log.i("JobDescriptionActivity","SendNotification: Exception: " + e.getMessage());
        }

        // Send push notification to query
        ParsePush push = new ParsePush();
        push.setQuery(pushQuery); // Set our Installation query
        push.setData(jsonObject);
        push.sendInBackground(new SendCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null) {
                    updatePushToServer();
                }
            }
        });
    }

    private void updatePushToServer() {
        PushNotification pushNotification = new PushNotification();
        pushNotification.setJobId(postJob.getObjectId());
        pushNotification.setAlertMessage(ParseUser.getCurrentUser().getString(UserConstant.NAME) + " applied on job " + postJob.getjobTitle());
        pushNotification.setEmployerId(ParseUser.getCurrentUser().getObjectId());
        pushNotification.setType(JOB_APPLY);
        if (isEmployer()) {
            pushNotification.setuserType("employee");
        } else {
            pushNotification.setuserType("employer");
        }
        pushNotification.setWorkerId(ParseUser.getCurrentUser().getObjectId());
        pushNotification.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null) {
                    hideLoading();
                    showAlertDialog("You have successfully applied.");

                    //Finish Activity after 1.5 seconds after applying job
       //             Handler handler = new Handler();
       /*             handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            finish();
                        }
                    }, 1500);*/
                    //
                }
            }
        });
    }
}