package com.hire4work.utills;

import twitter4j.Twitter;
import twitter4j.auth.RequestToken;

/**
 * Created by Ratufa.Mahesh on 8/24/2015.
 */
public class Constant {

    //    public static boolean mainExitScreen = true;
    public static String loginWay = "";

    public static String USER_TYPE = "user_type";

    public static String DATA = "data";

    public static String NAME = "name";

    public static String ID = "id";

    public static String IMAGE = "image";

    public static final String NOTIFICATION = "notification";
    public static final String MESSAGE = "message";

    public static String TIMER_REQUEST = "Timer Request";
    public static String STOP_TIMER_REQUEST = "Stop Timer Request";
    public static String PAUSE_TIMER_REQUEST = "Pause Timer Request";
    public static String TIMER_START = "Timer Start";
    public static String RESUME_TIMER_REQUEST = "Resume Timer Request";
    public static String INVITATION_REQUEST = "Invitation Request";
    public static String JOB_APPLY = "Job Apply";


    public static String STARTS_WORK_TIMER = " Started work timer on the job - ";
    public static String PAUSED_WORK_TIMER = " Paused work timer on the job - ";
    public static String RESUMED_WORK_TIMER = " Resumed work timer on the job - ";
    public static String STOPPED_WORK_TIMER = " Stopped work timer on the job - ";

    public static int CATEGORY_FRAGMENT = 11;
    public static int NEWS_FRAGMENT = 0;
    public static int EDIT_PROFILE_FRAGMENT = 1;
    public static int FIND_JOB_FRAGMENT = 2;
    public static int INVITES_FRAGMENT = 3;
    public static int POST_JOB_FRAGMENT = 4;
    public static int MY_JOB_FRAGMENT = 5;
    public static int MEMBERSHIP_FRAGMENT = 6;
    public static int JOB_DESCRIPTION_FRAGMENT = 7;
    public static int EMPLOYER_JOB_DETAIL_FRAGMENT = 9;
    public static int WORKER_JOB_DETAIL_FRAGMENT = 8;
    public static int EmployerDetailFragment = 10;
    public static int WorkerFragment = 12;
    public static int InboxFragment = 13;

    public static Twitter twitter;
    public static String TWITTER_CALLBACK_URL = "oauth://Hire4HourLogin";
    public static RequestToken requestToken;
    public static String TWITTER_CONSUMER_KEY = "Ztg6sAS3J1kynEQTuLNw7EsLI";

    public static String TWITTER_CONSUMER_SECRET = "C4tDfWomNTSySYSfTIjDXKxBPKuQx2fjGHn5Dm33OZodC00oy7";
}
