package com.hire4work.utills;

/**
 * Created by Ratufa.Mahesh on 8/27/2015.
 */
public class Keys {

    // Preference keys
    public static String IS_LOGIN = "isLogin";

    public static String LOGIN_WAY = "login_way";
    public static String PROF_USER_NAME = "name";
    public static String PROF_EMAIL = "email";
    public static String PROF_CONTACT_NUMBER = "contact_number";
    public static String PROF_ADDRESS = "address";
    public static String PROF_PICTURE = "picture";
    public static String PROF_RATE = "rate";
    public static String PROF_PROFFESION = "proffesion";
    public static String PROF_UNIQUE_USER_NAME = "user_name";

    public static String REMEMBER_ME = "remember_me";
    public static String EMAIL_REMEMBER = "email_remember";
    public static String PASSWORD_REMEMBER = "password_remember";
}
