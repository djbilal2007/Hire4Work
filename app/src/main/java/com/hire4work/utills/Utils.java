package com.hire4work.utills;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.StrictMode;
import android.support.v7.app.AlertDialog;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.hire4work.R;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Ratufa.Mahesh on 8/17/2015.
 */
public class Utils {

    private static Toast myToast;

    @TargetApi(Build.VERSION_CODES.GINGERBREAD)
    @SuppressLint("NewApi")
    public static void initThreadPolicy()
    {
//        // TODO Auto-generated method stub
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.GINGERBREAD) {
            StrictMode.ThreadPolicy tp = new StrictMode.ThreadPolicy.Builder()
                        .permitAll().build();
            StrictMode.setThreadPolicy(tp);
        }
    }

    public static boolean isValidEmailAddress(String emailAddress)
    {
        String expression = "[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})";
        CharSequence inputStr = emailAddress;
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        return matcher.matches();
    }

    // validating password with retype password
    public static boolean isValidPassword(String pass) {
        if (pass != null && pass.length() >= 6) {
            return true;
        }
        return false;
    }

    public static long getTimeDifference(Date date) {
        Calendar c = Calendar.getInstance();
        // SimpleDateFormat sdf = new SimpleDateFormat("hh:mm");
        // String currentTime = sdf.format(c.getTime());
        Date currentDate = c.getTime();
        return currentDate.getTime() - date.getTime();
    }

    public static long getTimeDifference(Date currentDate, Date date) {
        //Calendar c = Calendar.getInstance();
        // SimpleDateFormat sdf = new SimpleDateFormat("hh:mm");
        // String currentTime = sdf.format(c.getTime());
        //Date currentDate = c.getTime();
        return currentDate.getTime() - date.getTime();
    }

    public static boolean isInternetConnected(Context mContext) {

        try {
            ConnectivityManager connect = null;
            connect = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);

            if (connect != null) {
                NetworkInfo resultMobile = connect.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

                NetworkInfo resultWifi = connect.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

                if ((resultMobile != null && resultMobile.isConnectedOrConnecting()) || (resultWifi != null && resultWifi.isConnectedOrConnecting())) {
                    return true;
                } else {
                    return false;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    public static int getDeviceWidth(Context context) {
        try {
            DisplayMetrics metrics = context.getResources().getDisplayMetrics();
            return metrics.widthPixels;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 480;
    }

    public static int getDeviceHeight(Context context) {
        try {
            DisplayMetrics metrics = context.getResources().getDisplayMetrics();
            return metrics.heightPixels;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 480;
    }

    public static void hideKeyBoard(Context context, View view) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static void showAlertDialog(Context context, String title, String message) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        if (!TextUtils.isNullOrEmpty(title)) {
            alertDialog.setTitle(title);
        }
        alertDialog.setTitle(context.getResources().getString(R.string.app_name));
        alertDialog.setMessage(message);
        alertDialog.setPositiveButton("Ok",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

    public static void showDataToast(String data, Context context) {
        String toastMsg = data;
        if (myToast != null) {
            myToast.cancel();
            myToast = null;
        }
        if (myToast == null) {
            myToast = Toast.makeText(context, toastMsg, Toast.LENGTH_SHORT);
        }
        if (myToast != null) {
            myToast.setText(toastMsg);
        }
        myToast.show();
    }

    public static void viewVisible(View... view) {
        if (view == null) return;
        for (View v : view) {
            v.setVisibility(View.VISIBLE);
        }
    }

    public static void viewInvisible(View... view) {
        if (view == null) return;
        for (View v : view) {
            v.setVisibility(View.INVISIBLE);
        }
    }

    public static void viewGone(View... view) {
        if (view == null) return;
        for (View v : view) {
            v.setVisibility(View.GONE);
        }
    }

    public static String printKeyHash(Context context) {
        PackageInfo packageInfo;
        String key = null;
        try {
            //getting application package name, as defined in manifest
            String packageName = context.getApplicationContext().getPackageName();

            //Retriving package info
            packageInfo = context.getPackageManager().getPackageInfo(packageName, PackageManager.GET_SIGNATURES);

            Debug.e("Package Name=", context.getApplicationContext().getPackageName());

            for (Signature signature : packageInfo.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                key = new String(Base64.encode(md.digest(), 0));

                // String key = new String(Base64.encodeBytes(md.digest()));
                Debug.e("Key Hash=", key);
            }
        } catch (PackageManager.NameNotFoundException e1) {
            Debug.e("Name not found", e1.toString());
        } catch (NoSuchAlgorithmException e) {
            Debug.e("No such an algorithm", e.toString());
        } catch (Exception e) {
            Debug.e("Exception", e.toString());
        }

        return key;
    }

}
