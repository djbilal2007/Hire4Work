package com.hire4work.utills;

/**
 * Created by Ratufa.Mahesh on 8/24/2015.
 */

import android.content.Context;
import android.content.SharedPreferences;

import java.io.IOException;
import java.util.ArrayList;

public class SharedPrefrenceManager {

    private static SharedPrefrenceManager refrence = null;
    private final String PREFRENCE_NAME = "Hire4Hour";

    public static SharedPrefrenceManager getInstance() {
        if (refrence == null) {
            refrence = new SharedPrefrenceManager();
        }
        return refrence;
    }

    public String getSavedPrefrenceValue(Context context, String defaultValue, String prefKey) {
        String storedLanguage = "";
        try {
            SharedPreferences myPrefs = context.getSharedPreferences(PREFRENCE_NAME, 0);
            storedLanguage = myPrefs.getString(prefKey, defaultValue);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return storedLanguage;
    }

    public void setPrefrenceValue(Context context, String newValue, String prefKey) {
        SharedPreferences myPrefs = context.getSharedPreferences(PREFRENCE_NAME, 0);
        SharedPreferences.Editor prefsEditor = myPrefs.edit();
        prefsEditor.putString(prefKey, newValue);
        prefsEditor.commit();
    }


    public void setPrefrenceList(Context context, ArrayList<String> sJobCategory, String prefKey) {
        SharedPreferences myPrefs = context.getSharedPreferences(PREFRENCE_NAME, 0);
        SharedPreferences.Editor prefsEditor = myPrefs.edit();
        try {
            prefsEditor.putString(prefKey, ObjectSerializer.serialize(sJobCategory));
        } catch (IOException e) {
            e.printStackTrace();
        }
        prefsEditor.commit();
    }

    public ArrayList<String> getSavedPrefrenceList(Context context, ArrayList<String> sJobCategory, String prefKey) {
        ArrayList<String> storedLanguage = null;
        try {
            SharedPreferences myPrefs = context.getSharedPreferences(PREFRENCE_NAME, 0);
//            storedLanguage = myPrefs.getString(prefKey, sJobCategory);
            storedLanguage = (ArrayList<String>) ObjectSerializer.deserialize(myPrefs.getString(prefKey, ObjectSerializer.serialize(new ArrayList<String>())));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return storedLanguage;
    }

    public void clear(Context context, String prefKey) {
        SharedPreferences myPrefs = context.getSharedPreferences(PREFRENCE_NAME, 0);
        SharedPreferences.Editor prefsEditor = myPrefs.edit();
        prefsEditor.remove(prefKey);
        // prefsEditor.clear();
        prefsEditor.commit();
    }

    public void clearAll(Context context) {
        SharedPreferences myPrefs = context.getSharedPreferences(PREFRENCE_NAME, 0);
        SharedPreferences.Editor prefsEditor = myPrefs.edit();
        // prefsEditor.remove(prefKey);
        prefsEditor.clear();
        prefsEditor.commit();
    }

}
