package com.hire4work.utills;

import android.annotation.SuppressLint;
import android.util.Log;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;

/**
 * Created by Kailash on 12/14/2015.
 */

@SuppressLint("SdCardPath")
public class Debug {
    public static final boolean DEBUG = true;

    public static void e(String tag, String msg) {
        if (DEBUG) {
            Log.e(tag, msg);
        }
    }

    public static void i(String tag, String msg) {
        if (DEBUG) {
            Log.i(tag, msg);
        }
    }

    public static void w(String tag, String msg) {
        if (DEBUG) {
            Log.w(tag, msg);
        }
    }

    public static void d(String tag, String msg) {
        if (DEBUG) {
            Log.d(tag, msg);
        }
    }

    public static void v(String tag, String msg) {
        if (DEBUG) {
            Log.v(tag, msg);
        }
    }

    public static void saveToFile(String data) {
        if (DEBUG)
            try {
                File myFile = new File("/sdcard/in_app_testing.txt");
                if (!myFile.exists())
                    myFile.createNewFile();
                BufferedWriter writer = new BufferedWriter(new FileWriter(myFile, true));
                writer.write(data);
                writer.newLine();
                writer.newLine();
                writer.flush();
                writer.close();

            } catch (Exception e) {
                // TODO: handle exception
                e.toString();
            }
    }
}