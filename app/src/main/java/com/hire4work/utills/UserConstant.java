package com.hire4work.utills;

/**
 * Created by Kailash on 05-Oct-15.
 */
public class UserConstant {

    public static String EMAIL = "email";
    public static String NAME = "name";
    public static String CONTACT_NUMBER = "contactnumber";
    public static String PICTURE = "picture";
    public static String RATE = "rate";
    public static String IS_ACTIVE = "isActive";
    public static String CITY = "city";
    public static String COUNTRY = "country";
    public static String ZIP_CODE = "zipCode";
    public static String SKILLS = "skills";
    public static String ADDRESS = "address";
    public static String PAYPAL_USER_ID = "paypalUserId";
    public static String PROFESSION = "profession";
    public static String CURRENT_LOCATION = "currentLocation";
}
