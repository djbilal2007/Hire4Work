package com.hire4work.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.hire4work.R;
import com.hire4work.activity.GetAddressActivity;
import com.hire4work.activity.JobDescriptionActivity;
import com.hire4work.adapter.WorkCategoryAdapter;
import com.hire4work.base.BaseFragment;
import com.hire4work.bean.PlaceLocation;
import com.hire4work.bean.PlacesSuggestionsBean;
import com.hire4work.bean.PostJob;
import com.hire4work.bean.WorkCategory;
import com.hire4work.customewidgets.CustomAutoCompleteTextView;
import com.hire4work.placeapi.PlaceDetailsJSONParser;
import com.hire4work.utills.Constant;
import com.hire4work.utills.Debug;
import com.hire4work.utills.Keys;
import com.hire4work.utills.SharedPrefrenceManager;
import com.hire4work.utills.UserConstant;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kailash Chouhan (Ratufa Technologies)
 * Email: kailash.chouhan@ratufa.com.
 */
public class FindJobFragment extends BaseFragment implements View.OnClickListener {

    private String TAG = getClass().getSimpleName();
    private Spinner workCategorySpinner;
    private GoogleMap googleMap;
    private ViewFlipper viewFlipper;
    private List<PostJob> postJobList = new ArrayList<PostJob>();
    private ArrayList<String> list = new ArrayList<String>();
    private FindJobAdapter adapter;
    private List<WorkCategory> categoryList = new ArrayList<WorkCategory>();
    private CustomAutoCompleteTextView tv_address;
    private DownloadTask downloadTask;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup root = (ViewGroup) inflater.inflate(R.layout.fragment_find_job, null);

        viewFlipper = (ViewFlipper) root.findViewById(R.id.view_flipper);

        ImageView iv_my_location = (ImageView) root.findViewById(R.id.iv_my_location);
        iv_my_location.setOnClickListener(this);

        TextView tv_get_address = (TextView) root.findViewById(R.id.tv_get_address);
        tv_get_address.setOnClickListener(this);

        ImageButton iv_list = (ImageButton) root.findViewById(R.id.iv_list);
        iv_list.setOnClickListener(this);

        ListView listFindJob = (ListView) root.findViewById(R.id.listFindJob);
        adapter = new FindJobAdapter(getActivity(), postJobList);
        listFindJob.setAdapter(adapter);
        listFindJob.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                PostJob postJob = postJobList.get(position);
                Intent intent = new Intent(getActivity(), JobDescriptionActivity.class);
                intent.putExtra(Constant.DATA, postJob.getObjectId());
                startActivity(intent);
            }
        });

        tv_address = (CustomAutoCompleteTextView) root.findViewById(R.id.tv_address);
        tv_address.setThreshold(1);
        tv_address.setAdapter(new GooglePlacesAutocompleteAdapter(getActivity(), R.layout.spinner_text));
        tv_address.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                hideKeyBoard(tv_address);
                String reference = resultList.get(position).getReference();
                String url = getPlaceDetailsUrl(reference);
                // Creating ParserTask for parsing Google Places
                downloadTask = new DownloadTask();

                // Starting Parsing the JSON string
                // This causes to execute doInBackground() of ParserTask class
                downloadTask.execute(url);
            }
        });

        workCategorySpinner = (Spinner) root.findViewById(R.id.workCategorySpinner);

        displayView();
        try {
            // Getting status
            int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity());

            // Showing status
            if (status == ConnectionResult.SUCCESS) {
                addGoogleMap();
            } else {
                int requestCode = 10;
                Dialog dialog = GooglePlayServicesUtil.getErrorDialog(status, getActivity(), requestCode);
                dialog.show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return root;
    }

    private SharedPrefrenceManager sharedPrefrenceManager = SharedPrefrenceManager.getInstance();

    private void displayView() {
        if (sharedPrefrenceManager.getSavedPrefrenceValue(getActivity(), "", Keys.IS_LOGIN).equalsIgnoreCase("true")) {
            list.clear();
            list.add(0, "All");
            List<String> stringList = ParseUser.getCurrentUser().getList(UserConstant.PROFESSION);
            list.addAll(stringList);
            Log.d("FindJob", "Total Proff: " + list.size());
            ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_dropdown_item, list);
            workCategorySpinner.setAdapter(adapter);
            workCategorySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if (isInternetConnected()) {
                        findJob(list.get(position), true);
                    } else {
                        showAlertDialog(getString(R.string.internet));
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        } else {
            //getCategory(workCategorySpinner);

            ArrayList<String> list = sharedPrefrenceManager.getSavedPrefrenceList(getActivity(), new ArrayList<String>(), "CATEGORY");
            WorkCategory workCategory = new WorkCategory();
            workCategory.setjobCategory("All");
            categoryList.add(0, workCategory);

            //list.clear();
            for (int i = 0; i < list.size(); i++) {
                Debug.d(TAG, list.get(i));
                WorkCategory category = new WorkCategory();
                category.setjobCategory(list.get(i));
                categoryList.add(category);
            }
            workCategorySpinner.setAdapter(new WorkCategoryAdapter(getActivity(), R.layout.spinner_text, categoryList));

            workCategorySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if (isInternetConnected()) {
                        findJob(categoryList.get(position).getjobCategory(), false);
                    } else {
                        showAlertDialog(getString(R.string.internet));
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }
    }

    public void findJob(String category, boolean isLogin) {
        Debug.d(TAG, "Category Name :" + category);
        showLoading();
        ParseQuery<PostJob> postJobParseQuery = new ParseQuery<>(PostJob.class);
        postJobParseQuery.include("userId");

        if (isLogin) {
            // where condition to check my jobs are not in list
            postJobParseQuery.whereNotEqualTo("userId", ParseUser.getCurrentUser());

            if (category.equalsIgnoreCase("All")) {
                postJobParseQuery.whereContainedIn("jobCategory", list);
            } else {
                postJobParseQuery.whereEqualTo("jobCategory", category);
            }
        } else {
            List<String> stringList = sharedPrefrenceManager.getSavedPrefrenceList(getActivity(), new ArrayList<String>(), "CATEGORY");
            if (category.equalsIgnoreCase("All")) {
                postJobParseQuery.whereContainedIn("jobCategory", stringList);
            } else {
                postJobParseQuery.whereEqualTo("jobCategory", category);
            }
        }

        postJobParseQuery.findInBackground(new FindCallback<PostJob>() {
            @Override
            public void done(List<PostJob> list, ParseException e) {
                hideLoading();
                if (e == null) {
                    if (googleMap != null)
                        googleMap.clear();

                    postJobList.clear();
                    if (list.size() != 0) {
                        postJobList.addAll(list);
                        adapter.notifyDataSetChanged();
                        for (int i = 0; i < postJobList.size(); i++) {
                            displayMarker(postJobList.get(i));
                        }
                    } else {
                        showAlertDialog(getResources().getString(R.string.app_name), "No Jobs regarding to selected categories");
                    }
                }
            }
        });
    }

    private void addGoogleMap() {
        // check if we have got the googleMap already
        if (googleMap == null) {
            ((SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map)).getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(GoogleMap map) {
                    googleMap = map;
                }
            });

            try {
                MapsInitializer.initialize(getActivity());
            } catch (Exception e) {
                Debug.e(TAG, "Have GoogleMap but then error" + e);
                return;
            }

            googleMap.moveCamera(CameraUpdateFactory.newLatLng(getMyLocation()));
            googleMap.animateCamera(CameraUpdateFactory.zoomTo(10), 2000, null);
            googleMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                @Override
                public void onInfoWindowClick(Marker marker) {
                    Debug.d(TAG, marker.getTitle());
                    Debug.d(TAG, getJodId(marker.getTitle()));
                    Intent intent = new Intent(getActivity(), JobDescriptionActivity.class);
                    intent.putExtra(Constant.DATA, getJodId(marker.getTitle()));
                    startActivity(intent);
                }
            });
        }
    }

//    private PostJob getJodDetail(String title) {
//        PostJob searchHotel = new PostJob();
//        for (PostJob wp : postJobList) {
//            if (wp.getjobTitle().contains(title)) {
//                searchHotel = wp;
//            }
//        }
//        return searchHotel;
//    }

    private String getJodId(String title) {
        PostJob searchHotel = new PostJob();
        for (PostJob wp : postJobList) {
            if (wp.getjobTitle().contains(title)) {
                searchHotel = wp;
            }
        }
        return searchHotel.getObjectId();
    }

    private void displayMarker(PostJob postJob) {
        if (googleMap != null) {
            try {
                googleMap.addMarker(new MarkerOptions()
                        .position(new LatLng(postJob.getjobLatLong().getLatitude(), postJob.getjobLatLong().getLongitude()))
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.location_icon))
                        .title(postJob.getjobTitle()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.iv_list:
                viewFlipper.showNext();
                break;

            case R.id.iv_my_location:
                if (googleMap != null) {
                    googleMap.moveCamera(CameraUpdateFactory.newLatLng(getMyLocation()));
                    googleMap.animateCamera(CameraUpdateFactory.zoomTo(12), 2000, null);
                }
                break;

            case R.id.tv_get_address:
                if (isInternetConnected()) {
                    startActivityForResult(new Intent(getActivity(), GetAddressActivity.class), 111);
                } else {
                    showAlertDialog(getString(R.string.internet));
                }
                break;

            default:
                break;
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // Check which request we're responding to
        if (requestCode == 111) {
            // Make sure the request was successful
            if (resultCode == Activity.RESULT_OK) {
                // The user picked a contact.
                // The Intent's data Uri identifies which contact was selected.

                Debug.d(getClass().getSimpleName(), data.getStringExtra("ADDRESS"));

                if (googleMap != null) {
                    googleMap.clear();
                    googleMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(data.getDoubleExtra("LATITUDE", 0.00), data.getDoubleExtra("LONGITUDE", 0.00))));
                    googleMap.animateCamera(CameraUpdateFactory.zoomTo(12), 2000, null);
                }
            }
        }
    }

    public class FindJobAdapter extends ArrayAdapter<PostJob> {

        public FindJobAdapter(Context context, List<PostJob> list) {
            super(context, R.layout.item_employee_list, list);
        }

        @Override
        public View getView(int pos, View convertView, ViewGroup parent) {
            final ViewHolder holder;

            if (convertView == null) {
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_employee_list, parent, false);

                holder = new ViewHolder();

                holder.tv_user_name = (TextView) convertView.findViewById(R.id.tv_user_name);

                holder.tv_category = (TextView) convertView.findViewById(R.id.tv_category);

                holder.tv_description = (TextView) convertView.findViewById(R.id.tv_description);
                holder.tv_description.setVisibility(View.GONE);

                holder.rating_bar = (RatingBar) convertView.findViewById(R.id.rating_bar);
                holder.rating_bar.setVisibility(View.GONE);

                holder.checkBox = (CheckBox) convertView.findViewById(R.id.checkBox);
                holder.checkBox.setVisibility(View.GONE);

                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            PostJob postJob = getItem(pos);

            holder.tv_user_name.setText(postJob.getjobTitle());

            holder.tv_category.setText(postJob.getjobCategory());

            return convertView;
        }

        class ViewHolder {
            TextView tv_user_name, tv_category, tv_description;
            RatingBar rating_bar;
            CheckBox checkBox;
        }
    }

    class GooglePlacesAutocompleteAdapter extends ArrayAdapter<PlacesSuggestionsBean> implements Filterable {
        private ArrayList<PlacesSuggestionsBean> resultList;

        public GooglePlacesAutocompleteAdapter(Context context, int textViewResourceId) {
            super(context, textViewResourceId);
        }

        @Override
        public int getCount() {
            return resultList.size();
        }

        @Override
        public PlacesSuggestionsBean getItem(int index) {
            return resultList.get(index);
        }

        @Override
        public Filter getFilter() {
            Filter filter = new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence constraint) {
                    FilterResults filterResults = new FilterResults();
                    if (constraint != null) {
                        // Retrieve the autocomplete results.
                        resultList = autocomplete(constraint.toString());

                        // Assign the data to the FilterResults
                        filterResults.values = resultList;
                        filterResults.count = resultList.size();
                    }
                    return filterResults;
                }

                @Override
                protected void publishResults(CharSequence constraint, FilterResults results) {
                    if (results != null && results.count > 0) {
                        notifyDataSetChanged();
                    } else {
                        notifyDataSetInvalidated();
                    }
                }
            };
            return filter;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            final ViewHolder holder;
            PlacesSuggestionsBean bean = getItem(position);

            if (convertView == null) {

                holder = new ViewHolder();

                convertView = LayoutInflater.from(getContext()).inflate(R.layout.spinner_text, parent, false);

                holder.tvFullName = (TextView) convertView.findViewById(R.id.spinner);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            holder.tvFullName.setText(bean.getDescription());
            return convertView;
        }

        class ViewHolder {
            private TextView tvFullName;
        }
    }

    private static final String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place";
    private static final String TYPE_AUTOCOMPLETE = "/autocomplete";
    private static final String OUT_JSON = "/json";
    //------------ make your specific key ------------
    private static final String API_KEY = "AIzaSyCwYbBlV3qSpiiMj4Q9THL0yMikokVAaWM";
    ArrayList<PlacesSuggestionsBean> resultList = null;

    public ArrayList<PlacesSuggestionsBean> autocomplete(String input) {
        HttpURLConnection conn = null;
        StringBuilder jsonResults = new StringBuilder();
        try {
            StringBuilder sb = new StringBuilder(PLACES_API_BASE + TYPE_AUTOCOMPLETE + OUT_JSON);
            sb.append("?key=" + API_KEY);
            sb.append("&input=" + URLEncoder.encode(input, "utf8"));

            URL url = new URL(sb.toString());

            System.out.println("URL: " + url);
            conn = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader(conn.getInputStream());

            // Load the results into a StringBuilder
            int read;
            char[] buff = new char[1024];
            while ((read = in.read(buff)) != -1) {
                jsonResults.append(buff, 0, read);
            }
        } catch (MalformedURLException e) {
            Debug.e("sdf", "Error processing Places API URL " + e);
            return resultList;
        } catch (IOException e) {
            Debug.e("dsd", "Error connecting to Places API " + e);
            return resultList;
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }

        try {
            // Create a JSON object hierarchy from the results
            JSONObject jsonObj = new JSONObject(jsonResults.toString());
            JSONArray predsJsonArray = jsonObj.getJSONArray("predictions");

            // Extract the Place descriptions from the results
            resultList = new ArrayList<>(predsJsonArray.length());
            for (int i = 0; i < predsJsonArray.length(); i++) {
                System.out.println(predsJsonArray.getJSONObject(i).getString("description"));
                System.out.println("============================================================");
                PlacesSuggestionsBean placesSuggestionsBean = new PlacesSuggestionsBean();
                placesSuggestionsBean.setDescription(predsJsonArray.getJSONObject(i).getString("description"));
                placesSuggestionsBean.setId(predsJsonArray.getJSONObject(i).getString("id"));
                placesSuggestionsBean.setReference(predsJsonArray.getJSONObject(i).getString("reference"));
                resultList.add(placesSuggestionsBean);
            }
        } catch (JSONException e) {
            Debug.e("adsd", "Cannot process JSON results " + e);
        }

        return resultList;
    }

    public static String getPlaceDetailsUrl(String ref) {

        // Obtain browser key from https://code.google.com/apis/console Extra key for test : AIzaSyCdi7F8PV02m13lhPm3gRQEmsEhWHB_iXk
        String key = "key=AIzaSyCwYbBlV3qSpiiMj4Q9THL0yMikokVAaWM";

        // reference of place
        String reference = "reference=" + ref;

        // Sensor enabled
        String sensor = "sensor=false";

        // Building the parameters to the web service
        String parameters = reference + "&" + sensor + "&" + key;

        // Output format
        String output = "json";

        // Building the url to the web service
//        String url = "https://maps.googleapis.com/maps/api/place/details/" + output + "?" + parameters;

        return "https://maps.googleapis.com/maps/api/place/details/" + output + "?" + parameters;
    }

    /**
     * A method to download json data from url
     */
    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line;
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();
//            Log.d("downloadUrl : ", data);

            br.close();

        } catch (Exception e) {
            Debug.d("Exception download url", e.toString());
        } finally {
            if (iStream != null) {
                iStream.close();
                urlConnection.disconnect();
            }
        }
        return data;
    }

    // Fetches data from url passed
    private class DownloadTask extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... url) {

            // For storing data from web service
            String data = "";

            try {
                // Fetching the data from web service
                data = downloadUrl(url[0]);
            } catch (Exception e) {
                Debug.d("Background Task", e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            //setProgress(false);
            Debug.d("DownloadTask : ", result);
            // Creating ParserTask for parsing Google Places
            DetailParserTask detailParserTask = new DetailParserTask();

            // Starting Parsing the JSON string
            // This causes to execute doInBackground() of ParserTask class
            detailParserTask.execute(result);
        }
    }


    ArrayList<PlaceLocation> locationList = null;

    private class DetailParserTask extends AsyncTask<String, Integer, ArrayList<PlaceLocation>> {

        @Override
        protected ArrayList<PlaceLocation> doInBackground(String... jsonData) {

            JSONObject jObject;

            try {
                jObject = new JSONObject(jsonData[0]);

                PlaceDetailsJSONParser placeDetailsJsonParser = new PlaceDetailsJSONParser();
                // Getting the parsed data as a List construct
                locationList = placeDetailsJsonParser.getLocation(jObject);

            } catch (Exception e) {
                Debug.d("Exception", e.toString());
            }
            return locationList;
        }

        @Override
        protected void onPostExecute(ArrayList<PlaceLocation> result) {
            PlaceLocation hm = result.get(0);
            Debug.d(getClass().getSimpleName(), "" + hm.getLatitude());
            setUpMapIfNeeded(hm.getLatitude(), hm.getLongitude());
        }
    }

    private void setUpMapIfNeeded(Double latitude, Double longitude) {
        if (googleMap != null) {
            googleMap.clear();
            googleMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(latitude, longitude)));
            googleMap.animateCamera(CameraUpdateFactory.zoomTo(12), 2000, null);
        }
    }
}

