package com.hire4work.fragment;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.hire4work.R;
import com.hire4work.activity.GetAddressActivity;
import com.hire4work.activity.LoginActivity;
import com.hire4work.adapter.WorkCategoryAdapter;
import com.hire4work.base.BaseFragment;
import com.hire4work.bean.PostJob;
import com.hire4work.bean.WorkCategory;
import com.hire4work.bean.appenum.LoginWay;
import com.hire4work.utills.AppUtils;
import com.hire4work.utills.Constant;
import com.hire4work.utills.Debug;
import com.hire4work.utills.Keys;
import com.hire4work.utills.PermissionsUtils;
import com.hire4work.utills.SharedPrefrenceManager;
import com.hire4work.utills.TextUtils;
import com.hire4work.utills.image.ImageIntentHandler;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseGeoPoint;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import static android.content.DialogInterface.OnClickListener;

/**
 * Created by Ratufa.Mahesh on 8/19/2015.
 */
public class PostJobFragment extends BaseFragment implements View.OnClickListener {

    private String TAG = getClass().getSimpleName();
    private TextView tv_post_job, tv_title, tv_post_job_image;
    private EditText tv_job_address;
    private EditText edt_job_title, edt_job_description;
    private Spinner spinnerWorkCategory;
    private String category;
    private List<WorkCategory> categoryList = new ArrayList<WorkCategory>();
    private ParseUser parseUser;
    private ImageView image, map_icon;
    private ImageView iv_back;
    // for handling image
    private ImageIntentHandler.ImagePair mImagePair;
    private ParseGeoPoint parseGeoPoint;
    LocationManager manager;
    private boolean cameraPermissionAccepted = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup root = (ViewGroup) inflater.inflate(R.layout.fragment_post_job, null);
        manager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);

        tv_title = (TextView) root.findViewById(R.id.tv_title);
        tv_title.setText(getActivity().getResources().getString(R.string.PostJob));

        iv_back = (ImageView) root.findViewById(R.id.iv_back);
        iv_back.setVisibility(View.GONE);

        tv_post_job = (TextView) root.findViewById(R.id.tv_post_job);
        tv_post_job.setOnClickListener(this);

        tv_post_job_image = (TextView) root.findViewById(R.id.tv_post_job_image);
        tv_post_job_image.setOnClickListener(this);

        image = (ImageView) root.findViewById(R.id.image);

        edt_job_title = (EditText) root.findViewById(R.id.edt_job_title);

        tv_job_address = (EditText) root.findViewById(R.id.tv_job_address);

        map_icon = (ImageView) root.findViewById(R.id.map_icon);
        map_icon.setOnClickListener(this);

        edt_job_description = (EditText) root.findViewById(R.id.edt_job_description);

        spinnerWorkCategory = (Spinner) root.findViewById(R.id.spinnerWorkCategory);
        getCategory();

        spinnerWorkCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                category = categoryList.get(position).getjobCategory();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                category = categoryList.get(0).getjobCategory();
            }
        });

        return root;
    }

    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("Your GPS seems to be disabled, do you want to enable it?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public void onClick(View v) {
        hideKeyBoard(v);
        switch (v.getId()) {
            case R.id.map_icon:
                if (isInternetConnected()) {
                    if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                        buildAlertMessageNoGps();
                    } else {
                        startActivityForResult(new Intent(getActivity(), GetAddressActivity.class), 111);
                    }
                } else {
                    showAlertDialog(getString(R.string.internet));
                }
                break;

            // Post Job Click
            case R.id.tv_post_job:
                if (isInternetConnected()) {
                    postJobDataValidation();
                } else {
                    showAlertDialog(getString(R.string.internet));
                }
                break;

            case R.id.tv_post_job_image:
                if(checkPermissionforReadWrite()){
                    showImageDialog();
                }
                break;

            case R.id.tv_job_address:
                if (isInternetConnected()) {
                    startActivityForResult(new Intent(getActivity(), GetAddressActivity.class), 111);
                } else {
                    showAlertDialog(getString(R.string.internet));
                }
                break;

            default:
                break;
        }
    }

    public void getCategory() {
        showLoading();
        ParseQuery<WorkCategory> query = new ParseQuery<WorkCategory>(WorkCategory.class);
        query.orderByAscending("jobCategory");
        query.findInBackground(new FindCallback<WorkCategory>() {
            @Override
            public void done(List<WorkCategory> list, ParseException e) {
                hideLoading();
                if (e == null) {
                    categoryList.addAll(list);
                    spinnerWorkCategory.setAdapter(new WorkCategoryAdapter(getActivity(), R.layout.spinner_text, categoryList));
                }
            }
        });
    }

    private SharedPrefrenceManager sharedPrefrenceManager = SharedPrefrenceManager.getInstance();

    public void postJobDataValidation() {
        if (TextUtils.isNullOrEmpty(edt_job_title.getText().toString().trim())) {
            showAlertDialog("Job title can't be empty.");
        } else if (TextUtils.isNullOrEmpty(tv_job_address.getText().toString().trim())) {
            showAlertDialog("Enter Job Address.");
        } else if (parseGeoPoint == null) {
            showAlertDialog("Please choose Exact job location with the help of the map icon.");
        } else if (TextUtils.isNullOrEmpty(edt_job_description.getText().toString().trim())) {
            showAlertDialog("Enter Job Description.");
        } else {
            if (isInternetConnected()) {
                if (sharedPrefrenceManager.getSavedPrefrenceValue(getActivity(), "", Keys.IS_LOGIN).equalsIgnoreCase("true")) {
                    postJobTask();
                } else {
                    showLoginAlert();
                }
            } else {
                showAlertDialog("Alert", getResources().getString(R.string.chk_your_network_connection));
            }
        }
    }

    private void showLoginAlert() {
        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
        alert.setTitle("Login Required");
        alert.setMessage("Please register with Hire4Hour App to explore new job opportunities and make a job profile for yourself.");
        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                startActivity(new Intent(getActivity(), LoginActivity.class).putExtra(Constant.DATA, LoginWay.IN_APP_LOGIN));
            }
        });
        alert.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        alert.show();
    }

    public void postJobTask() {
        showLoading(getString(R.string.please_wait));

        final String imagePath = mImagePair == null ? null : mImagePair.imagePath;
        if (imagePath == null) {
            Debug.d(TAG, "Image was null.");
        } else {
            Debug.d(TAG, "Image Path " + imagePath);
        }

        parseUser = ParseUser.getCurrentUser();
        final PostJob postJob = new PostJob();
        postJob.setuserId(ParseUser.getCurrentUser());
        postJob.setjobTitle(edt_job_title.getText().toString().trim());
        postJob.setjobAddress(tv_job_address.getText().toString().trim());
        postJob.setjobCategory(category);
        postJob.setjobDescription(edt_job_description.getText().toString().trim());
        if (parseGeoPoint != null) {
            postJob.setjobLatLong(parseGeoPoint);
        }
        postJob.setLiveJobStatus("NO");
        postJob.setjobDone("NO");
        postJob.setjobAssignStatus("NO");
        postJob.setviewsCount(0);
        postJob.setRated("NO");

        if (imagePath != null) {
            postJob.setImage(getParseFile(imagePath));
        }

        postJob.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                hideLoading();
                if (e == null) {
                    Log.d(TAG, "e is null");
                    showJobPostSuccessAlert(postJob);
                }
            }
        });
    }

    private void showJobPostSuccessAlert(final PostJob postJob) {
        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
        alert.setTitle("Success");
        alert.setMessage("Job posted successfully.");
        alert.setPositiveButton("OK", new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                EmployeeListFragment fragment = new EmployeeListFragment();
                Bundle bundle = new Bundle();
                bundle.putSerializable(Constant.DATA, postJob);
                fragment.setArguments(bundle);
                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.main, fragment).addToBackStack(null).commit();// no back
            }
        });
        alert.show();
    }

    private byte[] readInFile(String path) throws IOException {
        // TODO Auto-generated method stub
        byte[] data = null;
        File file = new File(path);
        InputStream input_stream = new BufferedInputStream(new FileInputStream(file));
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
        data = new byte[16384]; // 16K
        int bytes_read;
        while ((bytes_read = input_stream.read(data, 0, data.length)) != -1) {
            buffer.write(data, 0, bytes_read);
        }
        input_stream.close();
        return buffer.toByteArray();
    }

    private ParseFile getParseFile(String path) {
        ParseFile file = null;
        try {
            byte[] imageByte = readInFile(path);
            long time = System.currentTimeMillis();
            String sImageName = parseUser.getObjectId() + time + ".jpeg";
            // Create the ParseFile
            file = new ParseFile(sImageName, imageByte);
            // Upload the image into Parse Cloud
            file.saveInBackground();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return file;
    }

    private void showImageDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(getString(R.string.add_image_from));
        builder.setItems(new String[]{getString(R.string.capture_image), getString(R.string.from_gallery)}, new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (which == 0) {
                    Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    File f = AppUtils.createImageFile();
                    if ((f != null) && f.exists()) {
                        mImagePair = new ImageIntentHandler.ImagePair(image, f.getAbsolutePath());
                        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
                        startActivityForResult(takePictureIntent, ImageIntentHandler.REQUEST_CAPTURE);
                    } else {
                        Toast.makeText(getActivity(), "Camera error", Toast.LENGTH_LONG).show();
                    }
                    dialog.dismiss();
                } else if (which == 1) {
                    mImagePair = new ImageIntentHandler.ImagePair(image, null);
                    Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(i, ImageIntentHandler.REQUEST_GALLERY);
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        WindowManager wm = (WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point point = new Point();
        display.getSize(point);
        int wid = point.x;
        int ht = point.y;


        ImageIntentHandler intentHandler =
                new ImageIntentHandler(getActivity(), mImagePair)
                        .folder(getString(R.string.app_name))
                        .sizePx(wid, ht);

        intentHandler.handleIntent(requestCode, resultCode, data);

        // Check which request we're responding to
        if (requestCode == 111) {
            // Make sure the request was successful
            if (resultCode == Activity.RESULT_OK) {
                // The user picked a contact.
                // The Intent's data Uri identifies which contact was selected.

                Debug.d(getClass().getSimpleName(), data.getStringExtra("ADDRESS"));
                tv_job_address.setText(data.getStringExtra("ADDRESS"));
                tv_job_address.setSelection(data.getStringExtra("ADDRESS").length());
                parseGeoPoint = new ParseGeoPoint(data.getDoubleExtra("LATITUDE", 0.00), data.getDoubleExtra("LONGITUDE", 0.00));
            }
        }
    }

    private boolean checkPermissionforReadWrite() {
        if(Build.VERSION.SDK_INT >= PermissionsUtils.SDK_INT_MARSHMALLOW){
            if(!PermissionsUtils.getInstance(getContext()).isPermissionGranted(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE, "Read External Storage")){
                return false;
            }
        }
        if(Build.VERSION.SDK_INT >= PermissionsUtils.SDK_INT_MARSHMALLOW) {
            if (!PermissionsUtils.getInstance(getContext()).isPermissionGranted(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE, "Write External Storage")) {
                return false;
            }
        }
        if(Build.VERSION.SDK_INT >= PermissionsUtils.SDK_INT_MARSHMALLOW) {
            if (!PermissionsUtils.getInstance(getContext()).isPermissionGranted(getContext(), Manifest.permission.CAMERA, "Camera")) {
                return false;
            }
        }
        return true;
    }
}