package com.hire4work.fragment.worker;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.hire4work.R;
import com.hire4work.base.BaseFragment;
import com.hire4work.bean.JobApply;
import com.hire4work.bean.NotificationCounter;
import com.hire4work.customewidgets.BadgeView;
import com.hire4work.customewidgets.TabLayout;
import com.hire4work.utills.Constant;
import com.hire4work.utills.TextUtils;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.List;

/**
 * Created by Kailash on 14-Sep-15.
 */
public class WorkerHomeFragment extends BaseFragment {

    private String Titles[] = {"Detail", "Inbox"};
    private int Numboftabs = 2;
    private TextView tv_title;
    private TabLayout mTabLayout;
    private ImageView iv_back;
    private BadgeView badgeView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup root = (ViewGroup) inflater.inflate(R.layout.fragment_my_job_detail, null);

        tv_title = (TextView) root.findViewById(R.id.tv_title);
        tv_title.setText(postJob.getPostJobId().getString("jobTitle"));

        iv_back = (ImageView) root.findViewById(R.id.iv_back);
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                displayView(Constant.MY_JOB_FRAGMENT, false);
            }
        });

        displayFragment(0);

        mTabLayout = (TabLayout) root.findViewById(R.id.bottom_layout);
        mTabLayout.setDistributeEvenly(true);
        mTabLayout.setSelectedIndicatorColors(ContextCompat.getColor(getActivity(), R.color.tab_selected_color));
        mTabLayout.setBottomTabAdapter(new TabLayout.BottomTabAdapter() {
            @Override
            public int getCount() {
                return Numboftabs;
            }

            @Override
            public String getTitle(int pos) {
                return Titles[pos];
            }

            @Override
            public void onTabClick(int pos) {
                displayFragment(pos);
            }
        });

        getNotificationCounter();

        badgeView = new BadgeView(getActivity(), mTabLayout, 1);
        badgeView.setBackgroundResource(R.drawable.ic_badge);
        badgeView.setBadgePosition(BadgeView.POSITION_TOP_RIGHT);
        badgeView.setGravity(Gravity.CENTER);
        badgeView.setPadding(0, 0, 0, 3);
        badgeView.setText("0");
        badgeView.show();

        return root;
    }

    private void getNotificationCounter() {
        ParseQuery<NotificationCounter> query = new ParseQuery<NotificationCounter>(NotificationCounter.class);
        query.whereEqualTo("jobId", postJob.getPostJobId().getObjectId());
        query.whereEqualTo("receiverId", ParseUser.getCurrentUser().getObjectId());
        query.findInBackground(new FindCallback<NotificationCounter>() {
            @Override
            public void done(List<NotificationCounter> objects, ParseException e) {
                int count = 0;
                if (e == null) {
                    try {
                        if (!TextUtils.isNullOrEmpty(objects)) {
                            count = Integer.valueOf(objects.get(0).getBedgeCount());
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
                badgeView.setText("" + count);
                badgeView.show();
            }
        });
    }

    public void resetBadgeCount() {
        badgeView.setText("0");
    }

    private void displayFragment(int position) {

        Fragment fragment = null;
        switch (position) {
            case 0:
                fragment = new WorkerDetailFragment();
                break;
            case 1:
                updateNotificationCounter();
                fragment = new WorkerInboxFragment();
                break;
            default:
                break;
        }

        if (fragment != null) {
            Bundle bundle = new Bundle();
            bundle.putSerializable(Constant.DATA, postJob);
            fragment.setArguments(bundle);
            FragmentManager fragmentManager = getFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.frameCointainer, fragment).addToBackStack(null).commit();
        }
    }

    JobApply postJob;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = this.getArguments();
        postJob = (JobApply) bundle.getSerializable(Constant.DATA);
    }

    private void updateNotificationCounter() {
        ParseQuery<NotificationCounter> query = new ParseQuery<NotificationCounter>(NotificationCounter.class);
        query.whereEqualTo("jobId", postJob.getPostJobId().getObjectId());
        query.whereEqualTo("receiverId", ParseUser.getCurrentUser().getObjectId());
        query.findInBackground(new FindCallback<NotificationCounter>() {
            @Override
            public void done(List<NotificationCounter> objects, ParseException e) {
                if (e == null) {
                    try {
                        if (!TextUtils.isNullOrEmpty(objects)) {
                            NotificationCounter counter = objects.get(0);
                            counter.setBedgeCount("0");
                            counter.saveInBackground();
                            resetBadgeCount();
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }
        });
    }
}
