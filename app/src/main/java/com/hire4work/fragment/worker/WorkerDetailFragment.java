package com.hire4work.fragment.worker;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.hire4work.R;
import com.hire4work.activity.ImageDisplayActivity;
import com.hire4work.base.BaseFragment;
import com.hire4work.bean.JobApply;
import com.hire4work.bean.PostJob;
import com.hire4work.utills.Constant;
import com.hire4work.utills.Debug;
import com.hire4work.utills.UserConstant;
import com.hire4work.utills.Utils;
import com.parse.DeleteCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kailash on 14/09/2015.
 */
public class WorkerDetailFragment extends BaseFragment implements OnMapReadyCallback, View.OnClickListener {

    private TextView tvWithdraw, tvGo, tv_attached_image, tv_price, tv_history, tv_job_desc, tv_job_address;
    private JobApply jobApply;
    private String jobFullDesc;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = this.getArguments();
        jobApply = (JobApply) bundle.getSerializable(Constant.DATA);
        Debug.d(getClass().getSimpleName(), jobApply.getPostJobId().getString("jobTitle"));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_job_detail, container, false);

        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        tvGo = (TextView) v.findViewById(R.id.tvRight);
        tvGo.setText(getActivity().getResources().getString(R.string.go));
        tvGo.setOnClickListener(this);

        tvWithdraw = (TextView) v.findViewById(R.id.tvLeft);
        tvWithdraw.setText(getActivity().getResources().getString(R.string.withdraw));
        tvWithdraw.setOnClickListener(this);

        tv_job_desc = (TextView) v.findViewById(R.id.tv_job_desc);
        jobFullDesc = jobApply.getPostJobId().getString("jobDescription");
        String[] DescParts = jobFullDesc.split("\\?@");
        if (DescParts != null) {
            if (DescParts.length > 0) {
                tv_job_desc.setText(DescParts[DescParts.length - 1]);
            }
        }
        //tv_job_desc.setText(jobApply.getPostJobId().getString("jobDescription"));

        tv_job_address = (TextView) v.findViewById(R.id.tv_job_address);
        tv_job_address.setText(jobApply.getPostJobId().getString("jobAddress"));

        tv_price = (TextView) v.findViewById(R.id.tv_price);
        tv_price.setText(jobApply.getPostJobId().getString("jobPrice"));

        tv_attached_image = (TextView) v.findViewById(R.id.tv_attached_image);
        tv_attached_image.setOnClickListener(this);

        tv_history = (TextView) v.findViewById(R.id.tv_history);
        // underline text
        tv_history.setPaintFlags(tv_history.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        tv_history.setOnClickListener(this);

        return v;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(jobApply.getPostJobId().getParseGeoPoint("jobLatLong").getLatitude(), jobApply.getPostJobId().getParseGeoPoint("jobLatLong").getLongitude())));
        googleMap.animateCamera(CameraUpdateFactory.zoomTo(14), 2000, null);
        googleMap.getUiSettings().setAllGesturesEnabled(false);
        googleMap.addMarker(new MarkerOptions().position(new LatLng(jobApply.getPostJobId().getParseGeoPoint("jobLatLong").getLatitude(), jobApply.getPostJobId().getParseGeoPoint("jobLatLong").getLongitude()))
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.location_icon))
                .title(jobApply.getPostJobId().getString("jobTitle")));
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            // go button
            case R.id.tvRight:
                if (isInternetConnected()) {
                    Uri gmmIntentUri = Uri.parse("google.navigation:q=" + jobApply.getPostJobId().getString("jobAddress"));
                    Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                    mapIntent.setPackage("com.google.android.apps.maps");
                    if (mapIntent.resolveActivity(getActivity().getPackageManager()) != null) {
                        startActivity(mapIntent);
                    }
                } else {
                    showToast(getString(R.string.internet));
                }
                break;

            // withdraw button
            case R.id.tvLeft:
                if (isInternetConnected()) {
                    ConfirmationAlert();
                } else {
                    showAlertDialog(getString(R.string.internet));
                }
                break;

            case R.id.tv_attached_image:
                if (isInternetConnected()) {
                    if (jobApply.getPostJobId().getParseFile("imageOrFile") != null) {
                        startActivity(new Intent(getActivity(), ImageDisplayActivity.class).putExtra(Constant.DATA, jobApply.getPostJobId().getParseFile("imageOrFile").getUrl()));
                    } else {
                        showToast("Image not available.");
                    }
                } else {
                    showToast(getString(R.string.internet));
                }
                break;

            case R.id.tv_history:
                displayPopupWindow(tv_history);
                break;

            default:
                break;
        }
    }

    public void ConfirmationAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(final DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_NEGATIVE:
                        showLoading();
                        getPostJobDetail(jobApply.getPostJobId().getObjectId());
                        jobApply.setJobAppliedStatus("NO");
                        jobApply.saveInBackground();
                        jobApply.deleteInBackground(new DeleteCallback() {
                            @Override
                            public void done(ParseException e) {
                                if (e == null) {
                                    dialog.dismiss();
                                    hideLoading();
                                    showToast("You have successfully withdrawn from this job.");
                                    displayView(Constant.MY_JOB_FRAGMENT, false);
                                }
                            }
                        });
                        break;
                    case DialogInterface.BUTTON_POSITIVE:
                        dialog.dismiss();
                        break;
                }
            }
        };
        builder.setTitle(getString(R.string.app_name))
                .setMessage("Do you want to withdraw from this job?")
                .setPositiveButton(getResources().getString(R.string.no), dialogClickListener)
                .setNegativeButton(getResources().getString(R.string.yes), dialogClickListener)
                .show();
    }

    private void getPostJobDetail(String iPostJob) {
        showLoading();
        ParseQuery<PostJob> postJobParseQuery = new ParseQuery<PostJob>(PostJob.class);
        postJobParseQuery.include("userId");
        postJobParseQuery.getInBackground(iPostJob, new GetCallback<PostJob>() {
            @Override
            public void done(PostJob object, ParseException e) {
                hideLoading();
                if (e == null) {
                    List<ParseUser> list = object.getAppliedUser();

                    for (int i = 0; i < list.size(); i++) {
                        if (list.get(i).getObjectId().equalsIgnoreCase(ParseUser.getCurrentUser().getObjectId())) {
                            list.remove(i);
                        }
                    }
                    object.getAppliedUserName().remove(ParseUser.getCurrentUser().getString(UserConstant.NAME));
                    object.setAppliedUser(list);
                    object.saveInBackground();
                }
            }
        });
    }

    public boolean isAlreadyApplied(List<ParseUser> appliedUser) {
        boolean b = false;
        for (int i = 0; i < appliedUser.size(); i++) {
            if (appliedUser.get(i).getObjectId().equalsIgnoreCase(ParseUser.getCurrentUser().getObjectId())) {
                b = true;
                break;
            } else {
                b = false;
            }
        }
        return b;
    }

    private void displayPopupWindow(View anchorView) {
        final PopupWindow popup = new PopupWindow(getActivity());
        View layout = getActivity().getLayoutInflater().inflate(R.layout.popup_content, null);
        popup.setContentView(layout);
        // Set content width and height
        popup.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        popup.setWidth(Utils.getDeviceWidth(getActivity()) - 80);
        // Closes the popup window when touch outside of it - when looses focus
        popup.setOutsideTouchable(true);
        popup.setFocusable(true);
        // Show anchored to button
        popup.setBackgroundDrawable(new BitmapDrawable());
        popup.showAtLocation(anchorView, Gravity.CENTER, 0, 0);

        ListView list = (ListView) layout.findViewById(R.id.popupList);

        ArrayList<String> descList = new ArrayList<String>();
        String[] DescParts = jobFullDesc.split("\\?@");
        if (DescParts != null) {
            if (DescParts.length > 0) {
                for (int i = 0; i < DescParts.length; i++) {
                    descList.add(DescParts[i]);
                }
            }
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, descList);
        list.setAdapter(adapter);

        TextView btnClosePopup = (TextView) layout.findViewById(R.id.btnClosePopup);
        btnClosePopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
            }
        });
//        resetPasswordPopup(anchorView);
    }

    private void resetPasswordPopup(View view) {
        final PopupWindow popup = new PopupWindow(getActivity());
        View layout = getActivity().getLayoutInflater().inflate(R.layout.popup_content, null);
        popup.setContentView(layout);
        // Set content width and height
        popup.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        popup.setWidth(Utils.getDeviceWidth(getActivity()) - 80);
        // Closes the popup window when touch outside of it - when looses focus
        popup.setOutsideTouchable(true);
        popup.setFocusable(true);
        // Show anchored to button
        popup.setBackgroundDrawable(new BitmapDrawable());
        popup.showAtLocation(view, Gravity.CENTER, 0, 0);

        ListView list = (ListView) layout.findViewById(R.id.popupList);

        ArrayList<String> descList = new ArrayList<String>();
        String[] DescParts = jobFullDesc.split("\\?@");
        if (DescParts != null) {
            if (DescParts.length > 0) {
                for (int i = 0; i < DescParts.length; i++) {
                    descList.add(DescParts[i]);
                }
            }
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, descList);
        list.setAdapter(adapter);

        TextView btnClosePopup = (TextView) layout.findViewById(R.id.btnClosePopup);
        btnClosePopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
            }
        });
    }
}
