package com.hire4work.fragment.worker;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.hire4work.R;
import com.hire4work.activity.ChatActivity;
import com.hire4work.adapter.ChatMessageAdapter;
import com.hire4work.base.BaseFragment;
import com.hire4work.bean.ChatMessage;
import com.hire4work.bean.JobApply;
import com.hire4work.bean.NotificationCounter;
import com.hire4work.utills.Constant;
import com.hire4work.utills.Debug;
import com.hire4work.utills.TextUtils;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kailash on 15/09/2015.
 */
public class WorkerInboxFragment extends BaseFragment {

    private static final String TAG = ChatActivity.class.getName();
    private JobApply jobApply;
    private ListView lvChat;
    private EditText edt_message;
    private TextView btn_send;
    private ArrayList<ChatMessage> mMessagesList = new ArrayList<ChatMessage>();
    private ChatMessageAdapter mAdapter;
    // Create a handler which can run code periodically
    private Handler handler = new Handler();
    // Keep track of initial load to scroll to the bottom of the ListView
    private boolean mFirstLoad;

    private int REFRESH_INTERVAL = 1000;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = this.getArguments();
        jobApply = (JobApply) bundle.getSerializable(Constant.DATA);
        Debug.d(getClass().getSimpleName(), jobApply.getPostJobId().getString("jobTitle"));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_worker_inbox, container, false);

        updateNotificationCounter();
        receiveMessage();

        mAdapter = new ChatMessageAdapter(getActivity(), jobApply.getPostUserId(), mMessagesList);

        lvChat = (ListView) v.findViewById(R.id.lvChat);
        lvChat.setTranscriptMode(1);
        lvChat.setAdapter(mAdapter);

        mFirstLoad = true;

        edt_message = (EditText) v.findViewById(R.id.edt_message);

        btn_send = (TextView) v.findViewById(R.id.btn_send);
        btn_send.setEnabled(false);
        btn_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyBoard(btn_send);
                if (isInternetConnected()) {
                    String message = edt_message.getText().toString().trim();
                    if (TextUtils.isNullOrEmpty(message)) {
                        return;
                    }

                    sendMessage(message, ParseUser.getCurrentUser(), jobApply.getPostJobId().getParseUser("userId"));
                    edt_message.setText("");
                } else {
                    showToast(getString(R.string.internet));
                }
            }
        });

        // Run the runnable object defined every 1000ms
         handler.postDelayed(runnable, REFRESH_INTERVAL);

        return v;
    }

    private boolean chatData = true;
    // Defines a runnable which is run every 100ms
    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            if(chatData){
                receiveMessage();
                handler.postDelayed(this, REFRESH_INTERVAL);
            }
        }
    };

    // Query messages from Parse so we can load them into the chat adapter
    private void receiveMessage() {
      //  showLoading();
        ArrayList<ParseUser> strings = new ArrayList<ParseUser>();
        strings.add(ParseUser.getCurrentUser());
        strings.add(jobApply.getPostJobId().getParseUser("userId"));

        ParseQuery<ChatMessage> query = new ParseQuery<ChatMessage>(ChatMessage.class);
        query.orderByAscending("createdAt");
        query.whereEqualTo("jobId", jobApply.getPostJobId().getObjectId());
        query.whereContainedIn("receiverId", strings);
        query.whereContainedIn("senderId", strings);
        query.findInBackground(new FindCallback<ChatMessage>() {
            @Override
            public void done(List<ChatMessage> list, ParseException e) {
                hideLoading();
                if (e == null) {
                    mMessagesList.clear();
                    if (list.size() != 0) {
                        btn_send.setEnabled(true);
                        mMessagesList.addAll(list);
                        mAdapter.notifyDataSetChanged();
                        if (mFirstLoad) {
                            lvChat.setSelection(mAdapter.getCount() - 1);
                            mFirstLoad = false;
                        }
                    } else {
                        chatData = false;
                        btn_send.setEnabled(false);
                        showAlertDialog(getResources().getString(R.string.app_name), "Sorry chat not initiated yet.");
                    }
                }
            }
        });
    }

    private void sendMessage(final String message, final ParseUser senderId, final ParseUser receiverId) {
        showLoading();
        ChatMessage chatMessage = new ChatMessage();
        chatMessage.setCommentText(message);
        chatMessage.setSenderId(senderId);
        chatMessage.setJobId(jobApply.getPostJobId().getObjectId());
        chatMessage.setReceiverId(receiverId);
        chatMessage.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                hideLoading();
                if (e == null) {
                    updateNotificationCounter();
                }
            }
        });
    }

    private void updateNotificationCounter() {
        ParseQuery<NotificationCounter> query = new ParseQuery<NotificationCounter>(NotificationCounter.class);
        query.whereEqualTo("jobId", jobApply.getPostJobId().getObjectId());
        query.whereEqualTo("receiverId", jobApply.getPostJobId().getParseUser("userId").getObjectId());
        query.findInBackground(new FindCallback<NotificationCounter>() {
            @Override
            public void done(List<NotificationCounter> objects, ParseException e) {
                //hideLoading();
                if (e == null) {
                    if (!TextUtils.isNullOrEmpty(objects)) {
                        try {
                            NotificationCounter counter = objects.get(0);
                            counter.setBedgeCount((Integer.valueOf(counter.getBedgeCount()) + 1) + "");
                            counter.saveInBackground();
                            receiveMessage();
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    } else {
//                        NotificationCounter counter = new NotificationCounter();
//                        counter.setBedgeCount("1");
//                        counter.setSenderId(ParseUser.getCurrentUser().getObjectId());
//                        counter.setReceiverId(jobApply.getPostJobId().getParseUser("userId").getObjectId());
//                        counter.setJobId(jobApply.getPostJobId().getObjectId());
//                        counter.saveInBackground();
                    }
                }
            }
        });
    }
}