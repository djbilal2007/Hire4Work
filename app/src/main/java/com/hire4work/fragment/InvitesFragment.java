package com.hire4work.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.hire4work.R;
import com.hire4work.activity.JobDescriptionActivity;
import com.hire4work.base.BaseListFragment;
import com.hire4work.bean.InvitesNotification;
import com.hire4work.bean.PostJob;
import com.hire4work.utills.Constant;
import com.hire4work.utills.TextUtils;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kailash on 25-Sep-15.
 */
public class InvitesFragment extends BaseListFragment {

    private TextView tv_title;
    private ImageView iv_back;
    private List<InvitesNotification> notificationList = new ArrayList<InvitesNotification>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup root = (ViewGroup) inflater.inflate(R.layout.fragment_invites, null);

        tv_title = (TextView) root.findViewById(R.id.tv_title);
        tv_title.setText(getActivity().getResources().getString(R.string.Invite));

        iv_back = (ImageView) root.findViewById(R.id.iv_back);
        iv_back.setVisibility(View.GONE);

        getInvitesNotification();
        return root;
    }

    private void getInvitesNotification() {
        showLoading(getString(R.string.please_wait));
        ParseUser parseUser = ParseUser.getCurrentUser();
        ParseQuery<InvitesNotification> parseQuery = new ParseQuery<InvitesNotification>(InvitesNotification.class);
        parseQuery.whereEqualTo("recieverUserId", parseUser.getObjectId());
        parseQuery.whereNotEqualTo("isStatus", "3");
        parseQuery.include("jobId");
        parseQuery.include("senderUserId");
        parseQuery.findInBackground(new FindCallback<InvitesNotification>() {
            @Override
            public void done(List<InvitesNotification> list, ParseException e) {
                try {
                    hideLoading();
                    if (e == null) {
                        notificationList.clear();
                        if (!TextUtils.isNullOrEmpty(list)) {
                            for (int i = 0; i < list.size(); i++) {
                                if (!list.get(i).getIsStatus().equalsIgnoreCase("3")) {
                                    InvitesNotification notification = list.get(i);
                                    notificationList.add(notification);
                                }
                            }
                            setListAdapter(new InvitesAdapter(getActivity(), notificationList));
                        } else {
                            showToast("No Invites.");
                        }
                    } else {
                        showToast("Error loading data. Try after sometime.");
                    }
                } catch (Exception ex) {
                    hideLoading();
                    ex.printStackTrace();
                    showToast("Error loading data. Try after sometime.");
                }
            }
        });
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        if (isInternetConnected()) {
            showLoading();
            ParseQuery<PostJob> query = new ParseQuery<PostJob>(PostJob.class);
            query.getInBackground(notificationList.get(position).getJobId().getObjectId(), new GetCallback<PostJob>() {
                @Override
                public void done(PostJob postJob, ParseException e) {
                    hideLoading();
                    if (e == null) {
                        if (postJob.getjobAssignStatus().equalsIgnoreCase("NO")) {
                            List<ParseUser> appliedUsers = postJob.getAppliedUser();
                            if (!TextUtils.isNullOrEmpty(appliedUsers)) {
                                if (isAlreadyApplied(appliedUsers)) {
                                    showAlertDialog("You have already applied for this job.");
                                } else {
                                    Intent intent = new Intent(getActivity(), JobDescriptionActivity.class);
                                    intent.putExtra(Constant.DATA, postJob.getObjectId());
                                    startActivity(intent);
                                }
                            } else {
                                Intent intent = new Intent(getActivity(), JobDescriptionActivity.class);
                                intent.putExtra(Constant.DATA, postJob.getObjectId());
                                startActivity(intent);
                            }
                        } else {
                            showAlertDialog("Job no longer exist.");
                        }

//                        JobDescriptionFragment fragment = new JobDescriptionFragment();
//                        Bundle bundle = new Bundle();
//                        bundle.putSerializable(Constant.DATA, postJob);
//                        fragment.setArguments(bundle);
//                        FragmentManager fragmentManager = getFragmentManager();
//                        fragmentManager.beginTransaction().replace(R.id.main, fragment).addToBackStack(null).commit();// no back
                    } else {
                        showToast("Detail not found.");
                    }
                }
            });
        } else {
            showAlertDialog(getString(R.string.internet));
        }
    }

    public class InvitesAdapter extends ArrayAdapter<InvitesNotification> {

        public InvitesAdapter(Context context, List<InvitesNotification> list) {
            super(context, R.layout.item_invites, list);
        }

        @Override
        public View getView(int pos, View convertView, ViewGroup parent) {
            ViewHolder holder;
            final InvitesNotification invitesNotification = getItem(pos);

            if (convertView == null) {
                holder = new ViewHolder();

                convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_invites, parent, false);

                holder.tv_invites_message = (TextView) convertView.findViewById(R.id.tv_invites_message);

                holder.iv_accept_invites = (ImageView) convertView.findViewById(R.id.iv_accept_invites);
                holder.iv_accept_invites.setVisibility(View.GONE);

                holder.iv_reject_invites = (ImageView) convertView.findViewById(R.id.iv_reject_invites);
                holder.iv_reject_invites.setVisibility(View.GONE);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            holder.tv_invites_message.setText(invitesNotification.getNotificationMessage());

            if (invitesNotification.getIsStatus().equalsIgnoreCase("2")) {
                holder.iv_accept_invites.setVisibility(View.GONE);
                holder.iv_reject_invites.setVisibility(View.GONE);
            } else {
                List<ParseUser> appliedUsers = invitesNotification.getParseObject("jobId").getList("appliedUser");
                if (!TextUtils.isNullOrEmpty(appliedUsers)) {
                    if (isAlreadyApplied(appliedUsers)) {
                        holder.iv_accept_invites.setVisibility(View.VISIBLE);
                        holder.iv_reject_invites.setVisibility(View.GONE);
                    } else {
                        holder.iv_reject_invites.setVisibility(View.VISIBLE);
                        holder.iv_accept_invites.setVisibility(View.GONE);
                    }
                } else {
                    holder.iv_reject_invites.setVisibility(View.VISIBLE);
                    holder.iv_accept_invites.setVisibility(View.GONE);
                }
            }

//            holder.iv_accept_invites.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    if (isInternetConnected()) {
//                        UpdateNotification("2", invitesNotification.getobjId());
//                    } else {
//                        showAlertDialog(getString(R.string.internet));
//                    }
//                }
//            });
//
//            holder.iv_reject_invites.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    if (isInternetConnected()) {
//                        UpdateNotification("3", invitesNotification.getobjId());
//                    } else {
//                        showAlertDialog(getString(R.string.internet));
//                    }
//                }
//            });

            return convertView;
        }

        class ViewHolder {
            private TextView tv_invites_message;
            private ImageView iv_reject_invites, iv_accept_invites;
        }

        public void UpdateNotification(final String isStatus, String s) {
            showLoading(getString(R.string.please_wait));
            ParseQuery<InvitesNotification> parseQuery = new ParseQuery<InvitesNotification>(InvitesNotification.class);
            parseQuery.getInBackground(s, new GetCallback<InvitesNotification>() {
                @Override
                public void done(InvitesNotification invitesNotification, ParseException e) {
                    invitesNotification.setIsStatus(isStatus);
                    invitesNotification.saveInBackground();
                    hideLoading();
                    // reload data
                    getInvitesNotification();
                }
            });
        }
    }

    public boolean isAlreadyApplied(List<ParseUser> appliedUser) {
        boolean b = false;
        try {
            for (int i = 0; i < appliedUser.size(); i++) {
                if (appliedUser.get(i).getObjectId().equalsIgnoreCase(ParseUser.getCurrentUser().getObjectId())) {
                    b = true;
                    break;
                } else {
                    b = false;
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return b;
    }
}
