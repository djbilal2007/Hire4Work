package com.hire4work.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.hire4work.R;
import com.hire4work.base.BaseListFragment;
import com.hire4work.bean.JobApply;
import com.hire4work.bean.PostJob;
import com.hire4work.fragment.employer.EmployerHomeFragment;
import com.hire4work.fragment.employer.EmployerTimerFragment;
import com.hire4work.fragment.worker.WorkerHomeFragment;
import com.hire4work.fragment.worker.WorkerTimerFragment;
import com.hire4work.utills.Constant;
import com.hire4work.utills.TextUtils;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kailash on 14-Sep-15.
 */

public class MyJobFragment extends BaseListFragment {

    private TextView tv_title;
    private ImageView iv_back;
    private List<PostJob> postJobList = new ArrayList<PostJob>();
    private List<JobApply> applyJobList = new ArrayList<JobApply>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup root = (ViewGroup) inflater.inflate(R.layout.fragment_my_job, null);

        tv_title = (TextView) root.findViewById(R.id.tv_title);
        tv_title.setText(getActivity().getString(R.string.myjob));

        iv_back = (ImageView) root.findViewById(R.id.iv_back);
        iv_back.setVisibility(View.GONE);

        getMyJob();

        return root;
    }

    public void getMyJob() {
        showLoading(getActivity().getString(R.string.please_wait));
        if (isEmployer()) {
            ParseQuery<PostJob> parseQuery = new ParseQuery<PostJob>(PostJob.class);
            parseQuery.orderByDescending("createdAt");
            parseQuery.include("userId");
            parseQuery.whereEqualTo("userId", ParseUser.getCurrentUser());
            parseQuery.findInBackground(new FindCallback<PostJob>() {
                @Override
                public void done(List<PostJob> list, ParseException e) {
                    hideLoading();
                    if (e == null) {
                        if (list != null) {
                            if (list.size() == 0) {
                                showToast("You haven't posted any job.");
                            } else {
                                applyJobList.clear();
                                postJobList.clear();
                                postJobList.addAll(list);
                                setListAdapter(new MyJobAdapter(getActivity(), postJobList));
                            }
                        }
                    }
                }
            });
        } else {
            ParseQuery<JobApply> applyParseQuery = new ParseQuery<JobApply>(JobApply.class);
            applyParseQuery.orderByDescending("createdAt");
            applyParseQuery.include("postjobId");
            applyParseQuery.include("postUserId");
            applyParseQuery.include("applyUserId");
            applyParseQuery.whereEqualTo("applyUserId", ParseUser.getCurrentUser());
            applyParseQuery.findInBackground(new FindCallback<JobApply>() {
                @Override
                public void done(List<JobApply> list, ParseException e) {
                    hideLoading();
                    if (list != null) {
                        if (list.size() == 0) {
                            showToast("You haven't applied any job.");
                        } else {
                            postJobList.clear();
                            applyJobList.clear();
                            applyJobList.addAll(list);
                            setListAdapter(new ApplyJobAdapter(getActivity(), applyJobList));
                        }
                    }
                }
            });
        }
    }/*

    public boolean isEmployer() {
        boolean b = false;
        ParseUser parseUser = ParseUser.getCurrentUser();
        List<String> stringCategoryList = parseUser.getList(PROFESSION);
        for (int i = 0; i < stringCategoryList.size(); i++) {
            if (stringCategoryList.get(i).equalsIgnoreCase(getActivity().getResources().getString(R.string.employer))) {
                b = true;
                break;
            } else {
                b = false;
            }
        }
        return b;
    }*/

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        if (isInternetConnected()) {
            if (isEmployer()) {
                PostJob postJob = postJobList.get(position);
                Boolean isLive = postJob.getLiveJobStatus().equalsIgnoreCase("YES");
                if (isLive) {
                    EmployerTimerFragment fragment = new EmployerTimerFragment();
                    Bundle bundle = new Bundle();
                    bundle.putSerializable(Constant.DATA, postJob);
                    fragment.setArguments(bundle);
                    FragmentManager fragmentManager = getFragmentManager();
                    fragmentManager.beginTransaction().replace(R.id.main, fragment).addToBackStack(null).commit();
                } else {
                    EmployerHomeFragment fragment = new EmployerHomeFragment();
                    Bundle bundle = new Bundle();
                    bundle.putSerializable(Constant.DATA, postJob);
                    fragment.setArguments(bundle);
                    FragmentManager fragmentManager = getFragmentManager();
                    fragmentManager.beginTransaction().replace(R.id.main, fragment).addToBackStack(null).commit();
                }
            } else {
                JobApply jobApply = applyJobList.get(position);
                Boolean isLive = jobApply.getPostJobId().getString("liveJobStatus").equalsIgnoreCase("YES");
                if (isLive) {
                    WorkerTimerFragment fragment = new WorkerTimerFragment();
                    Bundle bundle = new Bundle();
                    bundle.putSerializable(Constant.DATA, jobApply);
                    fragment.setArguments(bundle);
                    FragmentManager fragmentManager = getFragmentManager();
                    fragmentManager.beginTransaction().replace(R.id.main, fragment).addToBackStack(null).commit();
                } else {
                    WorkerHomeFragment fragment = new WorkerHomeFragment();
                    Bundle bundle = new Bundle();
                    bundle.putSerializable(Constant.DATA, jobApply);
                    fragment.setArguments(bundle);
                    FragmentManager fragmentManager = getFragmentManager();
                    fragmentManager.beginTransaction().replace(R.id.main, fragment).addToBackStack(null).commit();
                }
            }
        } else {
            showAlertDialog(getString(R.string.internet));
        }
    }

    public class MyJobAdapter extends ArrayAdapter<PostJob> {
        Context context;
        PostJob beanMyJob;

        public MyJobAdapter(Context context, List<PostJob> items) {
            super(context, R.layout.item_my_job, items);
            this.context = context;
        }

        public class ViewHolder {
            TextView jobCategory, tv_view_count, tv_jobTitle, tv_worker_cat;
            TextView address, tv_applicant_name;
            ImageView iv_live_status;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            ViewHolder holder = null;
            beanMyJob = getItem(position);

            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(getActivity().LAYOUT_INFLATER_SERVICE);
            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.item_my_job, null);
                holder = new ViewHolder();
                holder.jobCategory = (TextView) convertView.findViewById(R.id.tv_jobCategory);
                holder.address = (TextView) convertView.findViewById(R.id.address);
                holder.tv_applicant_name = (TextView) convertView.findViewById(R.id.tv_applicant_name);
                holder.tv_view_count = (TextView) convertView.findViewById(R.id.tv_view_count);
                holder.iv_live_status = (ImageView) convertView.findViewById(R.id.iv_live_status);
                holder.tv_jobTitle = (TextView) convertView.findViewById(R.id.tv_jobTitle);
                holder.tv_worker_cat = (TextView) convertView.findViewById(R.id.tv_worker_cat);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            try {

                if (isEmployer()) {
                    holder.tv_worker_cat.setText("Worker : ");
                } else {
                    holder.tv_worker_cat.setText("Applicant : ");
                }

                holder.jobCategory.setText(beanMyJob.getjobCategory());

                holder.address.setText(beanMyJob.getjobAddress());

                if (!TextUtils.isNullOrEmpty(beanMyJob.getAppliedUserName())) {
                    String s = "";
                    for (int i = 0; i < beanMyJob.getAppliedUserName().size(); i++) {
                        s = s + beanMyJob.getAppliedUserName().get(i) + ", ";
                    }
                    holder.tv_applicant_name.setText(TextUtils.removeComma(s.trim()));
                }

                //holder.tv_applicant_name.setText(ParseUser.getCurrentUser().getString(UserConstant.NAME));

                if (!TextUtils.isNullOrEmpty(beanMyJob.getjobTitle())) {
                    holder.tv_jobTitle.setText(beanMyJob.getjobTitle());
                }

                holder.tv_view_count.setText(beanMyJob.getviewsCount() + " Views");

                Boolean isJobDone = beanMyJob.getjobDone().equalsIgnoreCase("YES");

                if (isJobDone) {
                    holder.iv_live_status.setImageResource(R.drawable.job_done_icon);
                } else {
                    holder.iv_live_status.setImageResource(R.drawable.ic_live_status);
                }

                Boolean isLive = beanMyJob.getLiveJobStatus().equalsIgnoreCase("YES");

                if (isLive) {
                    holder.iv_live_status.setVisibility(View.VISIBLE);
                } else {
                    holder.iv_live_status.setVisibility(View.INVISIBLE);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return convertView;
        }
    }

    public class ApplyJobAdapter extends ArrayAdapter<JobApply> {
        Context context;
        JobApply beanMyJob;

        public ApplyJobAdapter(Context context, List<JobApply> items) {
            super(context, R.layout.item_my_job, items);
            this.context = context;
        }

        public class ViewHolder {
            TextView jobCategory, tv_view_count, tv_jobTitle;
            TextView address, tv_applicant_name;
            ImageView iv_live_status;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            ViewHolder holder = null;
            beanMyJob = getItem(position);

            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(getActivity().LAYOUT_INFLATER_SERVICE);
            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.item_my_job, null);
                holder = new ViewHolder();
                holder.jobCategory = (TextView) convertView.findViewById(R.id.tv_jobCategory);
                holder.address = (TextView) convertView.findViewById(R.id.address);
                holder.tv_applicant_name = (TextView) convertView.findViewById(R.id.tv_applicant_name);
                holder.tv_view_count = (TextView) convertView.findViewById(R.id.tv_view_count);
                holder.iv_live_status = (ImageView) convertView.findViewById(R.id.iv_live_status);
                holder.tv_jobTitle = (TextView) convertView.findViewById(R.id.tv_jobTitle);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            try {

                if (!TextUtils.isNullOrEmpty(beanMyJob.getPostJobId().getString("jobTitle"))) {
                    holder.tv_jobTitle.setText(beanMyJob.getPostJobId().getString("jobTitle"));
                }

                if (!TextUtils.isNullOrEmpty(beanMyJob.getPostJobId().getString("jobCategory"))) {
                    holder.jobCategory.setText(beanMyJob.getPostJobId().getString("jobCategory"));
                }

                if (!TextUtils.isNullOrEmpty(beanMyJob.getPostJobId().getString("jobAddress"))) {
                    holder.address.setText(beanMyJob.getPostJobId().getString("jobAddress"));
                }

                if (!TextUtils.isNullOrEmpty(beanMyJob.getPostJobId().getList("appliedUserName"))) {
                    String s = "";
                    for (int i = 0; i < beanMyJob.getPostJobId().getList("appliedUserName").size(); i++) {
                        s = s + beanMyJob.getPostJobId().getList("appliedUserName").get(i) + ", ";
                    }
                    holder.tv_applicant_name.setText(TextUtils.removeComma(s.trim()));
                }

                holder.tv_view_count.setText(beanMyJob.getPostJobId().getNumber("viewsCount") + " Views");

                Boolean isJobDone = beanMyJob.getPostJobId().getString("jobDone").equalsIgnoreCase("YES");

                if (isJobDone) {
                    holder.iv_live_status.setImageResource(R.drawable.job_done_icon);
                } else {
                    holder.iv_live_status.setImageResource(R.drawable.ic_live_status);
                }

                Boolean isLive = beanMyJob.getPostJobId().getString("liveJobStatus").equalsIgnoreCase("YES");

                if (isLive) {
                    holder.iv_live_status.setVisibility(View.VISIBLE);
                } else {
                    holder.iv_live_status.setVisibility(View.INVISIBLE);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            return convertView;
        }
    }
}
