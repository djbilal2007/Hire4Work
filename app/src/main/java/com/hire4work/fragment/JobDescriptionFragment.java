package com.hire4work.fragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.hire4work.R;
import com.hire4work.activity.ImageDisplayActivity;
import com.hire4work.activity.LoginActivity;
import com.hire4work.base.BaseFragment;
import com.hire4work.bean.JobApply;
import com.hire4work.bean.PostJob;
import com.hire4work.bean.PushNotification;
import com.hire4work.bean.appenum.LoginWay;
import com.hire4work.customewidgets.BezelImageView;
import com.hire4work.utills.Constant;
import com.hire4work.utills.Debug;
import com.hire4work.utills.Keys;
import com.hire4work.utills.SharedPrefrenceManager;
import com.hire4work.utills.TextUtils;
import com.hire4work.utills.UserConstant;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParsePush;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.parse.SendCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.hire4work.utills.Constant.JOB_APPLY;
import static com.hire4work.utills.UserConstant.NAME;
import static com.hire4work.utills.UserConstant.PICTURE;

/**
 * Created by Ratufa.Mahesh on 9/1/2015.
 */
public class JobDescriptionFragment extends BaseFragment implements View.OnClickListener {

    private String TAG = getClass().getSimpleName();
    private GoogleMap googleMap;
    private TextView cancelBtn, applyBtn, tv_job_title, job_category_address, tv_job_description, tv_image_attached;
    private PostJob postJob;
    private EditText edt_job_price;
    private BezelImageView profileImage;
    private SharedPrefrenceManager sharedPrefrenceManager = SharedPrefrenceManager.getInstance();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup root = (ViewGroup) inflater.inflate(R.layout.fragment_job_description, null);

        cancelBtn = (TextView) root.findViewById(R.id.tvLeft);
        cancelBtn.setText(getActivity().getResources().getString(R.string.cancel));
        cancelBtn.setOnClickListener(this);

        applyBtn = (TextView) root.findViewById(R.id.tvRight);
        applyBtn.setText(getActivity().getResources().getString(R.string.apply));
        applyBtn.setOnClickListener(this);

        tv_job_title = (TextView) root.findViewById(R.id.tv_job_title);
        tv_job_title.setText(postJob.getjobTitle());

        job_category_address = (TextView) root.findViewById(R.id.job_category_address);
        job_category_address.setText(postJob.getjobCategory() + " | " + postJob.getjobAddress());

        tv_job_description = (TextView) root.findViewById(R.id.tv_job_description);
        tv_job_description.setText(postJob.getjobDescription());

        tv_image_attached = (TextView) root.findViewById(R.id.tv_image_attached);
        tv_image_attached.setOnClickListener(this);

        edt_job_price = (EditText) root.findViewById(R.id.edt_job_price);
        edt_job_price.setText(ParseUser.getCurrentUser().getString(UserConstant.RATE));
        edt_job_price.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable mEdit) {
                if (mEdit.toString().length() > 0 && mEdit.toString().charAt(0) == '0') {
                    showToast("First letter must not be 0");
                    edt_job_price.setText("");
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });

        profileImage = (BezelImageView) root.findViewById(R.id.profileImage);
        try {
            if (postJob.getuserId().getParseFile(PICTURE) != null) {
                displayImage(getActivity(), profileImage, postJob.getuserId().getParseFile(PICTURE).getUrl());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        UpdateViewsCount();
        addGoogleMap();
        return root;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = this.getArguments();
        postJob = (PostJob) bundle.getSerializable(Constant.DATA);
        Debug.d(TAG, "getjobTitle : " + postJob.getjobTitle());
    }

    private void UpdateViewsCount() {
        //int count = Integer.valueOf(postJob.getviewsCount()) + 1;
        postJob.increment("viewsCount");
        postJob.saveInBackground();
    }

    private void addGoogleMap() {
        // check if we have got the googleMap already
        if (googleMap == null) {
            ((SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map)).getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(GoogleMap map) {
                    googleMap = map;
                }
            });

            LatLng latLng = new LatLng(postJob.getjobLatLong().getLatitude(), postJob.getjobLatLong().getLongitude());
            googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
            googleMap.animateCamera(CameraUpdateFactory.zoomTo(14), 2000, null);
            googleMap.getUiSettings().setAllGesturesEnabled(false);
            googleMap.addMarker(new MarkerOptions()
                    .position(new LatLng(postJob.getjobLatLong().getLatitude(), postJob.getjobLatLong().getLongitude()))
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.location_icon))
                    .title(postJob.getjobTitle()));
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            // Cancel Button
            case R.id.tvLeft:
                displayView(Constant.FIND_JOB_FRAGMENT, false);
                break;

            // Apply Job Button
            case R.id.tvRight:
                if (isInternetConnected()) {
                    if (sharedPrefrenceManager.getSavedPrefrenceValue(getActivity(), "", Keys.IS_LOGIN).equalsIgnoreCase("true")) {
                        if (TextUtils.isNullOrEmpty(postJob.getAppliedUser())) {
                            applyJob();
                        } else {
                            if (postJob.getAppliedUser().contains(ParseUser.getCurrentUser())) {
                                showAlertDialog("You have already applied.");
                                return;
                            }
                            applyJob();
                        }
                    } else {
                        showLoginAlert();
                    }
                } else {
                    showAlertDialog(getString(R.string.internet));
                }

                break;

            case R.id.tv_image_attached:
                if (isInternetConnected()) {
                    startActivity(new Intent(getActivity(), ImageDisplayActivity.class).putExtra(Constant.DATA, postJob.getImage()));
                } else {
                    showAlertDialog(getString(R.string.internet));
                }
                break;

            default:
                break;
        }
    }

    private void showLoginAlert() {
        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
        alert.setTitle("Login Required");
        alert.setMessage("Please register with Hire4Hour App to explore new job opportunities and make a job profile for yourself.");
        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                startActivity(new Intent(getActivity(), LoginActivity.class).putExtra(Constant.DATA, LoginWay.IN_APP_LOGIN));
            }
        });
        alert.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        alert.show();
    }

    private void applyJob() {
        if (TextUtils.isNullOrEmpty(edt_job_price.getText().toString())) {
            showAlertDialog("Required", "Enter your work charges in USD/Hr");
        } else if (Integer.valueOf(edt_job_price.getText().toString()) <= 15) {
            showAlertDialog("Please enter numeric values and Job price should be more than 15 $.");
        } else {
            showLoading();

            ParseUser parseUser = ParseUser.getCurrentUser();

            final List<ParseUser> userList = new ArrayList<>();
            userList.add(parseUser);

            final List<String> appliedUserNist = new ArrayList<String>();
            appliedUserNist.add(parseUser.getString(NAME));

            JobApply jobApply = new JobApply();
            jobApply.setApplyUserId(parseUser);
            jobApply.setJobAppliedStatus("YES");
            jobApply.setAssignedUserStatus("NO");
            jobApply.setWorkRate(edt_job_price.getText().toString() + " $");
            jobApply.setPostJobId(postJob);
            jobApply.setPostUserId(postJob.getuserId());
            jobApply.saveInBackground(new SaveCallback() {
                @Override
                public void done(ParseException e) {
                    if (e == null) {
                        postJob.setAppliedUser(userList);
                        postJob.setAppliedUserName(appliedUserNist);
                        postJob.saveInBackground();
                        sendNotification();
                    }
                }
            });
        }
    }

    private void sendNotification() {
        ParseQuery pushQuery = ParseInstallation.getQuery();
        pushQuery.whereEqualTo("channels", "global");
        pushQuery.whereEqualTo("installedUserId", postJob.getuserId().getObjectId());

        JSONObject jsonObject = new JSONObject();
        try {
            JSONObject data = new JSONObject();
            data.put("badge", "0");
            data.put("alert", ParseUser.getCurrentUser().getString(UserConstant.NAME) + " applied on job " + postJob.getjobTitle());

            jsonObject.put("type", JOB_APPLY);
            jsonObject.put("jobId", postJob.getObjectId());
            jsonObject.put("aps", data);
            jsonObject.put("workerId", ParseUser.getCurrentUser().getObjectId());
            jsonObject.put("userId", postJob.getuserId().getObjectId());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        // Send push notification to query
        ParsePush push = new ParsePush();
        push.setQuery(pushQuery); // Set our Installation query
        push.setData(jsonObject);
        push.sendInBackground(new SendCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null) {
                    updatePushToServer();
                }
            }
        });
    }

    private void updatePushToServer() {
        PushNotification pushNotification = new PushNotification();
        pushNotification.setJobId(postJob.getObjectId());
        pushNotification.setAlertMessage(ParseUser.getCurrentUser().getString(UserConstant.NAME) + " applied on job " + postJob.getjobTitle());
        pushNotification.setEmployerId(ParseUser.getCurrentUser().getObjectId());
        pushNotification.setType(JOB_APPLY);
        pushNotification.setWorkerId(ParseUser.getCurrentUser().getObjectId());
        if (isEmployer()) {
            pushNotification.setuserType("employee");
        } else {
            pushNotification.setuserType("employer");
        }
        pushNotification.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null) {
                    hideLoading();
                    showToast("Your application has been successfully submitted.");
                }
            }
        });
    }
}
