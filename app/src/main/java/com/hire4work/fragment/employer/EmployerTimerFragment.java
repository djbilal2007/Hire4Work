package com.hire4work.fragment.employer;

import android.content.DialogInterface;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.RatingBar;
import android.widget.TextView;

import com.hire4work.R;
import com.hire4work.base.BaseFragment;
import com.hire4work.bean.Disputes;
import com.hire4work.bean.FundTransection;
import com.hire4work.bean.PostJob;
import com.hire4work.bean.PushNotification;
import com.hire4work.bean.UserRatting;
import com.hire4work.customewidgets.CircleTimerView;
import com.hire4work.utills.Constant;
import com.hire4work.utills.Debug;
import com.hire4work.utills.TextUtils;
import com.hire4work.utills.Utils;
import com.parse.FindCallback;
import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParsePush;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.parse.SendCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static com.hire4work.utills.Constant.PAUSED_WORK_TIMER;
import static com.hire4work.utills.Constant.PAUSE_TIMER_REQUEST;
import static com.hire4work.utills.Constant.RESUMED_WORK_TIMER;
import static com.hire4work.utills.Constant.RESUME_TIMER_REQUEST;
import static com.hire4work.utills.Constant.STARTS_WORK_TIMER;
import static com.hire4work.utills.Constant.STOPPED_WORK_TIMER;
import static com.hire4work.utills.Constant.STOP_TIMER_REQUEST;
import static com.hire4work.utills.Constant.TIMER_START;
import static com.hire4work.utills.UserConstant.NAME;

/**
 * Created by Kailash on 06-Oct-15.
 */
public class EmployerTimerFragment extends BaseFragment implements CircleTimerView.CircleTimerListener, View.OnClickListener {

    private CircleTimerView timer;
    private ImageButton ib_pause, ib_play, ib_stop;
    private TextView tv_working_user_name;
    private TextView markJobCompleteBtn, disputeBtn;
    private TextView tv_title;
    private PostJob postJob;
    private ImageView iv_back;
    private Boolean isTimerPlay = false;

    // Create a handler which can run code periodically
    private Handler handler = new Handler();
    private int REFRESH_INTERVAL = 60000;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = this.getArguments();
        postJob = (PostJob) bundle.getSerializable(Constant.DATA);
        Boolean isJobDone = postJob.getjobDone().equalsIgnoreCase("YES");
        if (isJobDone) {
            showAlertDialog("This job has been completed.");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup root = (ViewGroup) inflater.inflate(R.layout.fragment_timer, null);

        tv_title = (TextView) root.findViewById(R.id.tv_title);
        tv_title.setText(postJob.getjobTitle());

        markJobCompleteBtn = (TextView) root.findViewById(R.id.tvLeft);
        markJobCompleteBtn.setText(getActivity().getResources().getString(R.string.mark_job_complete));
        markJobCompleteBtn.setOnClickListener(this);
        markJobCompleteBtn.setEnabled(false);

        disputeBtn = (TextView) root.findViewById(R.id.tvRight);
        disputeBtn.setText(getActivity().getResources().getString(R.string.dispute));
        disputeBtn.setVisibility(View.GONE);
        disputeBtn.setOnClickListener(this);

        timer = (CircleTimerView) root.findViewById(R.id.circular_timer_view);
        timer.setCircleTimerListener(this);

        iv_back = (ImageView) root.findViewById(R.id.iv_back);
        iv_back.setOnClickListener(this);

        ib_pause = (ImageButton) root.findViewById(R.id.ib_pause);
        ib_pause.setOnClickListener(this);
        ib_pause.setEnabled(false);

        ib_play = (ImageButton) root.findViewById(R.id.ib_play);
        ib_play.setVisibility(View.VISIBLE);
        ib_play.setOnClickListener(this);
        ib_play.setEnabled(false);

        ib_stop = (ImageButton) root.findViewById(R.id.ib_stop);
        ib_stop.setOnClickListener(this);
        ib_stop.setEnabled(false);

        tv_working_user_name = (TextView) root.findViewById(R.id.tv_working_user_name);
        if (!TextUtils.isNullOrEmpty(postJob.getAppliedUserName())) {
            String s = "";
            for (int i = 0; i < postJob.getAppliedUserName().size(); i++) {
                s = s + postJob.getAppliedUserName().get(i);
            }
            tv_working_user_name.setText(TextUtils.removeComma(s.trim()));
        }

        getPush();

        // Run the runnable object defined every 1000ms
        handler.postDelayed(runnable, REFRESH_INTERVAL);

        return root;
    }

    // Defines a runnable which is run every 100ms
    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            getPush();
            handler.postDelayed(this, REFRESH_INTERVAL);
        }
    };

    @Override
    public void onStop() {
        super.onStop();
        handler.removeCallbacks(runnable);
    }

    private void getPush() {
        showLoading();
        ParseQuery<PushNotification> parseQuery = new ParseQuery<PushNotification>(PushNotification.class);
        parseQuery.whereEqualTo("jobId", postJob.getObjectId());
        parseQuery.whereEqualTo("employerId", ParseUser.getCurrentUser().getObjectId());
        parseQuery.whereEqualTo("workerId", postJob.getWorkerId());
        parseQuery.orderByDescending("createdAt");
        parseQuery.findInBackground(new FindCallback<PushNotification>() {
            @Override
            public void done(List<PushNotification> list, ParseException e) {
                hideLoading();
                if (e == null) {
                    if (list.size() == 0) {
                        ib_play.setEnabled(false);
                        ib_stop.setEnabled(false);
                        ib_pause.setEnabled(false);
                    } else {
                        PushNotification pushNotification = list.get(0);
                        if (pushNotification.getType().equalsIgnoreCase(Constant.TIMER_REQUEST)) {
                            ib_play.setEnabled(true);
                            ResumeTimer();
                        } else {
                            ResumeTimer();
                        }
                    }
                }
            }
        });
    }

    private void ResumeTimer() {
        showLoading();
        ParseQuery<FundTransection> query = new ParseQuery<FundTransection>(FundTransection.class);
        query.whereEqualTo("jobId", postJob.getObjectId());
        query.whereEqualTo("employerId", ParseUser.getCurrentUser().getObjectId());
        query.whereEqualTo("workerId", postJob.getWorkerId());
        query.findInBackground(new FindCallback<FundTransection>() {
            @Override
            public void done(List<FundTransection> list, ParseException e) {
                hideLoading();
                if (e == null) {
                    if (list.size() != 0) {
                        final FundTransection fundTransection = list.get(0);

                        if (TextUtils.isNullOrEmpty(fundTransection.getDuration())) {
                            isTimerPlay = false;
                        } else {
                            isTimerPlay = true;
                        }

                        if (!TextUtils.isNullOrEmpty(fundTransection.getDuration())) {
                            timer.setRadian(getRadianFromInt(Integer.valueOf(fundTransection.getDuration())));
                        }

                        if (fundTransection.isPlay()) {
                            showLoading();
                            ParseCloud.callFunctionInBackground("getUTCDate", new HashMap<String, Object>(), new FunctionCallback<Date>() {
                                @Override
                                public void done(Date object, ParseException e) {
                                    hideLoading();
                                    if (e == null) {
                                        long mills = Utils.getTimeDifference(object, fundTransection.getStartDate());
                                        int seconds = (int) TimeUnit.MILLISECONDS.toSeconds(mills);
                                        seconds = seconds + Integer.valueOf(fundTransection.getDuration());
                                        timer.setRadian(getRadianFromInt(seconds));
                                        timer.startTimer();
                                        ib_play.setEnabled(false);
                                        ib_stop.setEnabled(true);
                                        ib_pause.setEnabled(true);
                                    }
                                }
                            });
                        }

                        if (fundTransection.isPause()) {

                            timer.pauseTimer();
                            ib_play.setEnabled(true);
                            ib_stop.setEnabled(true);
                            ib_pause.setEnabled(false);
                        }

                        if (fundTransection.isStop()) {

                            timer.pauseTimer();
                            ib_play.setEnabled(false);
                            ib_stop.setEnabled(false);
                            ib_pause.setEnabled(false);
                            markJobCompleteBtn.setEnabled(true);
                        }
                    }
                }
            }
        });
    }

    private Float getRadianFromInt(int i) {
        return (float) (i / ((60 / (2 * Math.PI) * 60)));
    }

    @Override
    public void onTimerStop() {
        showToast("Timer Stopped");
    }

    @Override
    public void onTimerStart(int time) {
        showToast("Timer Started");
    }

    @Override
    public void onTimerPause(int time) {
        showToast("Timer Paused");
    }

    public void pushNotificationData(JSONObject jsonObject) {
        Debug.e(getClass().getSimpleName(), "" + jsonObject.toString());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.iv_back:
                Debug.d(getClass().getSimpleName(), "Current time " + timer.getCurrentTime());
                displayView(Constant.MY_JOB_FRAGMENT, false);
                break;

            // pause timer
            case R.id.ib_pause:
                if (isInternetConnected()) {
//                    sendNotification(PAUSE_TIMER_REQUEST);
                    ConfirmationAlert("Do you want to pause timer?", PAUSE_TIMER_REQUEST);
                } else {
                    showToast(getString(R.string.internet));
                }
                break;

            // start timer
            case R.id.ib_play:
                if (isInternetConnected()) {
                    if (isTimerPlay) {
//                        sendNotification(RESUME_TIMER_REQUEST);
                        ConfirmationAlert("Do you want to resume timer?", RESUME_TIMER_REQUEST);
                    } else {
//                        sendNotification(TIMER_START);
                        ConfirmationAlert("Do you want to start timer ?", TIMER_START);
                    }
                } else {
                    showToast(getString(R.string.internet));
                }
                break;

            // stop timer
            case R.id.ib_stop:
                if (isInternetConnected()) {

//                    sendNotification(STOP_TIMER_REQUEST);
                    ConfirmationAlert("Do you want to stop the timer, process can't be resumed again!", STOP_TIMER_REQUEST);
                } else {
                    showToast(getString(R.string.internet));
                }
                break;

            // Mark job as complete
            case R.id.tvLeft:
                try {
                    if (TextUtils.isNullOrEmpty(postJob.getRated())) {
                        showReviewDialog(markJobCompleteBtn);
                    } else {
                        if (postJob.getRated().equalsIgnoreCase("YES")) {
                            showAlertDialog("You have already rated.");
                        } else {
                            showReviewDialog(markJobCompleteBtn);
                        }
                    }
                } catch (Exception e) {
                    showReviewDialog(markJobCompleteBtn);
                    e.printStackTrace();
                }
                break;

            // Dispute
            case R.id.tvRight:
                showDisputeDialog(disputeBtn);
                break;

            default:
                break;
        }
    }

    public void ConfirmationAlert(String message, final String value) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_NEGATIVE:
                        dialog.dismiss();
//                        jobApply.getPostJobId().
                        sendNotification(value);
                        break;
                    case DialogInterface.BUTTON_POSITIVE:
                        dialog.dismiss();
                        break;
                }
            }
        };
        builder.setTitle(getString(R.string.app_name))
                .setMessage(message)
                .setPositiveButton(getResources().getString(R.string.no), dialogClickListener)
                .setNegativeButton(getResources().getString(R.string.yes), dialogClickListener)
                .show();
    }

    private void sendNotification(final String value) {
        showLoading();

        // Create our Installation query
        ParseQuery pushQuery = ParseInstallation.getQuery();
        pushQuery.whereEqualTo("channels", "global");
        pushQuery.whereEqualTo("installedUserId", postJob.getString("workerId"));

        JSONObject jsonObject = new JSONObject();
        try {
            JSONObject data = new JSONObject();
            data.put("badge", "0");

            if (value.equalsIgnoreCase(TIMER_START)) {
                data.put("alert", ParseUser.getCurrentUser().getString(NAME) + STARTS_WORK_TIMER + postJob.getjobTitle());
                jsonObject.put("type", TIMER_START);
            } else if (value.equalsIgnoreCase(PAUSE_TIMER_REQUEST)) {
                data.put("alert", ParseUser.getCurrentUser().getString(NAME) + PAUSED_WORK_TIMER + postJob.getjobTitle());
                jsonObject.put("type", PAUSE_TIMER_REQUEST);
            } else if (value.equalsIgnoreCase(RESUME_TIMER_REQUEST)) {
                data.put("alert", ParseUser.getCurrentUser().getString(NAME) + RESUMED_WORK_TIMER + postJob.getjobTitle());
                jsonObject.put("type", RESUME_TIMER_REQUEST);
            } else if (value.equalsIgnoreCase(STOP_TIMER_REQUEST)) {
                data.put("alert", ParseUser.getCurrentUser().getString(NAME) + STOPPED_WORK_TIMER + postJob.getjobTitle());
                jsonObject.put("type", STOP_TIMER_REQUEST);
            }

            jsonObject.put("jobId", postJob.getObjectId());
            jsonObject.put("aps", data);
            jsonObject.put("workerId", postJob.getString("workerId"));
            jsonObject.put("userId", ParseUser.getCurrentUser().getObjectId());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        // Send push notification to query
        ParsePush push = new ParsePush();
        push.setQuery(pushQuery); // Set our Installation query
        push.setData(jsonObject);
        push.sendInBackground(new SendCallback() {
            @Override
            public void done(ParseException e) {
                hideLoading();
                if (e == null) {
                    if (value.equalsIgnoreCase(TIMER_START)) {
//                        isTimerPlay = true;
                        ib_play.setEnabled(false);
                        ib_stop.setEnabled(true);
                        ib_pause.setEnabled(true);
                        timer.startTimer();
                    } else if (value.equalsIgnoreCase(PAUSE_TIMER_REQUEST)) {
                        ib_play.setEnabled(true);
                        ib_stop.setEnabled(true);
                        ib_pause.setEnabled(false);
                        timer.pauseTimer();
                    } else if (value.equalsIgnoreCase(RESUME_TIMER_REQUEST)) {
                        timer.startTimer();
                        ib_play.setEnabled(false);
                        ib_stop.setEnabled(true);
                        ib_pause.setEnabled(true);
                    } else if (value.equalsIgnoreCase(STOP_TIMER_REQUEST)) {
                        timer.pauseTimer();
                        markJobCompleteBtn.setEnabled(true);
                        ib_play.setEnabled(false);
                        ib_stop.setEnabled(false);
                        ib_pause.setEnabled(false);
                    }
                    updatePushToServer(value);
                }
            }
        });
    }

    private void updatePushToServer(final String value) {
        PushNotification pushNotification = new PushNotification();
        pushNotification.setJobId(postJob.getObjectId());

        if (value.equalsIgnoreCase(TIMER_START)) {
            pushNotification.setAlertMessage(STARTS_WORK_TIMER + postJob.getjobTitle());
        } else if (value.equalsIgnoreCase(PAUSE_TIMER_REQUEST)) {
            pushNotification.setAlertMessage(PAUSED_WORK_TIMER + postJob.getjobTitle());
        } else if (value.equalsIgnoreCase(RESUME_TIMER_REQUEST)) {
            pushNotification.setAlertMessage(RESUMED_WORK_TIMER + postJob.getjobTitle());
        } else if (value.equalsIgnoreCase(STOP_TIMER_REQUEST)) {
            pushNotification.setAlertMessage(STOPPED_WORK_TIMER + postJob.getjobTitle());
        }

//        pushNotification.setAlertMessage("Timer is started as per your request");
        pushNotification.setEmployerId(ParseUser.getCurrentUser().getObjectId());
        pushNotification.setType(value);
        pushNotification.setWorkerId(postJob.getWorkerId());
        if (isEmployer()) {
            pushNotification.setuserType("employer");
        } else {
            pushNotification.setuserType("employee");
        }
        pushNotification.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null) {
                    updateFundTransaction(value);
                }
            }
        });
    }

    Date convertedDate = new Date();

    private void updateFundTransaction(final String value) {
        ParseQuery<FundTransection> query = new ParseQuery<FundTransection>(FundTransection.class);
        query.whereEqualTo("jobId", postJob.getObjectId());
        query.whereEqualTo("employerId", ParseUser.getCurrentUser().getObjectId());
        query.whereEqualTo("workerId", postJob.getWorkerId());
        query.findInBackground(new FindCallback<FundTransection>() {
            @Override
            public void done(List<FundTransection> list, ParseException e) {

                if (e == null) {
                    if (list.size() != 0) {
                        //Calendar c = Calendar.getInstance();

                        ParseCloud.callFunctionInBackground("getUTCDate", new HashMap<String, Object>(), new FunctionCallback<Date>() {
                            @Override
                            public void done(Date object, ParseException e) {
                                if (e == null) {
                                    System.out.println("Parse DB time => " + object);

                                    SimpleDateFormat df = new SimpleDateFormat("MMM dd, yyyy, hh:mm");
                                    //convertedDate = df.format(object);

                                    //SimpleDateFormat df1 = new SimpleDateFormat("MMM dd, yyyy, hh:mm");
                                    try {
                                        convertedDate = df.parse(String.valueOf(object));
                                    } catch (java.text.ParseException e1) {
                                        e1.printStackTrace();
                                    }
                                }
                            }
                        });

                        FundTransection fundTransection = list.get(0);

                        if (value.equalsIgnoreCase(TIMER_START)) {
                            fundTransection.setStartDate(convertedDate);
                            fundTransection.setDuration("0");
                            fundTransection.isPlay(true);
                            fundTransection.isStop(false);
                            fundTransection.isPause(false);
                        } else if (value.equalsIgnoreCase(PAUSE_TIMER_REQUEST)) {
                            fundTransection.setStartDate(convertedDate);
                            fundTransection.setDuration(String.valueOf(timer.getCurrentTime()));
                            fundTransection.isPlay(false);
                            fundTransection.isStop(false);
                            fundTransection.isPause(true);
                        } else if (value.equalsIgnoreCase(RESUME_TIMER_REQUEST)) {
                            fundTransection.setStartDate(convertedDate);
                            fundTransection.isPlay(true);
                            fundTransection.isStop(false);
                            fundTransection.isPause(false);
                        } else if (value.equalsIgnoreCase(STOP_TIMER_REQUEST)) {
                            fundTransection.setEndDate(convertedDate);
                            fundTransection.isPlay(false);
                            fundTransection.isStop(true);
                            fundTransection.isPause(false);
                        }
                        fundTransection.saveInBackground();
                        StopJob(value);
                    }
                }
            }
        });
    }

    private void StopJob(String value) {
        if (value.equalsIgnoreCase(STOP_TIMER_REQUEST)) {
            postJob.setjobDone("YES");
            postJob.saveInBackground();
        }
    }

    private void showReviewDialog(View view) {
        final PopupWindow popup = new PopupWindow(getActivity());
        View layout = getActivity().getLayoutInflater().inflate(R.layout.review_rating, null);
        popup.setContentView(layout);

        // Set content width and height
        popup.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        popup.setWidth(Utils.getDeviceWidth(getActivity()) - 80);
        // Closes the popup window when touch outside of it - when looses focus
        popup.setOutsideTouchable(true);
        popup.setFocusable(true);
        // Show anchored to button
        popup.setBackgroundDrawable(new BitmapDrawable());
        popup.showAtLocation(view, Gravity.CENTER, 0, 0);

        final EditText edt_review = (EditText) layout.findViewById(R.id.edt_review);

        final RatingBar rating_bar = (RatingBar) layout.findViewById(R.id.rating_bar);

        Button btn_post = (Button) layout.findViewById(R.id.btn_post);
        btn_post.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isInternetConnected()) {
                    if (rating_bar.getRating() == 0.0) {
                        showToast("Give rating to submit review");
                    } else {
                        showLoading();

                        postJob.setRated("YES");
                        postJob.saveInBackground();

                        UserRatting userRatting = new UserRatting();
                        userRatting.setWorkerId(postJob.getWorkerId());
                        userRatting.setuserId(ParseUser.getCurrentUser());

                        if (!TextUtils.isNullOrEmpty(edt_review.getText().toString())) {
                            userRatting.setRatingDescription(edt_review.getText().toString());
                        }
                        userRatting.setRatingPoint(rating_bar.getRating());
                        userRatting.saveInBackground(new SaveCallback() {
                            @Override
                            public void done(ParseException e) {
                                hideLoading();
                                if (e == null) {
                                    popup.dismiss();
                                    showToast("Review submitted");
                                }
                            }
                        });

                    }
                } else {
                    showToast(getString(R.string.internet));
                }
            }
        });

        Button btn_cancel = (Button) layout.findViewById(R.id.btn_cancel);
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
            }
        });
    }

    private void showDisputeDialog(View view) {
        final PopupWindow popup = new PopupWindow(getActivity());
        View layout = getActivity().getLayoutInflater().inflate(R.layout.dispute_layout, null);
        popup.setContentView(layout);

        // Set content width and height
        popup.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        popup.setWidth(Utils.getDeviceWidth(getActivity()) - 80);
        // Closes the popup window when touch outside of it - when looses focus
        popup.setOutsideTouchable(true);
        popup.setFocusable(true);
        // Show anchored to button
        popup.setBackgroundDrawable(new BitmapDrawable());
        popup.showAtLocation(view, Gravity.CENTER, 0, 0);

        final EditText edt_dispute = (EditText) layout.findViewById(R.id.edt_dispute);
        Button btn_post = (Button) layout.findViewById(R.id.btn_post);
        btn_post.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isNullOrEmpty(edt_dispute.getText().toString())) {
                    showToast("Enter dispute reason");
                } else {
                    popup.dismiss();
                    addDisputeNote(edt_dispute.getText().toString());
                }
            }
        });

        Button btn_cancel = (Button) layout.findViewById(R.id.btn_cancel);
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
            }
        });
    }

    private void addDisputeNote(String note) {
        showLoading();
        Disputes disputes = new Disputes();
        disputes.setWorkerId(postJob.getWorkerId());
        disputes.setEmployId(ParseUser.getCurrentUser().getObjectId());
        disputes.setPostJobId(postJob.getObjectId());
        disputes.setDisputeNote(note);
        disputes.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                hideLoading();
                if (e == null) {
                    showToast("Dispute submitted.");
                }
            }
        });
    }
}
