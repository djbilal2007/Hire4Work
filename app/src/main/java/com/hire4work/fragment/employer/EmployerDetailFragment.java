package com.hire4work.fragment.employer;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Paint;
import android.graphics.Point;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hire4work.R;
import com.hire4work.activity.GetAddressActivity;
import com.hire4work.activity.ImageDisplayActivity;
import com.hire4work.base.BaseFragment;
import com.hire4work.bean.PostJob;
import com.hire4work.customewidgets.BetterPopupWindow;
import com.hire4work.utills.AppUtils;
import com.hire4work.utills.Constant;
import com.hire4work.utills.Debug;
import com.hire4work.utills.TextUtils;
import com.hire4work.utills.image.ImageIntentHandler;
import com.parse.DeleteCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseGeoPoint;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Kailash Chouhan (Ratufa Technologies) Email: kailash.chouhan@ratufa.com.
 */
public class EmployerDetailFragment extends BaseFragment implements View.OnClickListener {

    private TextView tv_history;
    private EditText edt_job_description, edt_job_price;
    private EditText edt_job_address;
    private PostJob postJob;
    private ImageView image;
    private String jobFullDesc;

    // for handling image
    private ImageIntentHandler.ImagePair mImagePair;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = this.getArguments();
        postJob = (PostJob) bundle.getSerializable(Constant.DATA);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_employer_detail, container, false);

        TextView tv_save_changes = (TextView) v.findViewById(R.id.tvRight);
        tv_save_changes.setText(getActivity().getResources().getString(R.string.save_changes));
        tv_save_changes.setOnClickListener(this);

        TextView tv_remove = (TextView) v.findViewById(R.id.tvLeft);
        tv_remove.setText(getActivity().getResources().getString(R.string.remove));
        tv_remove.setOnClickListener(this);

        LinearLayout layout_price = (LinearLayout) v.findViewById(R.id.layout_price);
        layout_price.setVisibility(View.GONE);

        TextView tv_attach_image = (TextView) v.findViewById(R.id.tv_attach_image);
        tv_attach_image.setOnClickListener(this);

        TextView tv_old_attachment = (TextView) v.findViewById(R.id.tv_old_attachment);
        tv_old_attachment.setOnClickListener(this);

        tv_history = (TextView) v.findViewById(R.id.tv_history);
        // underline text
        tv_history.setPaintFlags(tv_history.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        tv_history.setOnClickListener(this);

        image = (ImageView) v.findViewById(R.id.image);

        edt_job_address = (EditText) v.findViewById(R.id.edt_job_address);
        edt_job_address.setText(postJob.getjobAddress());

        ImageView map_icon = (ImageView) v.findViewById(R.id.map_icon);
        map_icon.setOnClickListener(this);

        edt_job_price = (EditText) v.findViewById(R.id.edt_job_price);
        edt_job_price.setText(postJob.getjobPrice());
        edt_job_price.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable mEdit) {
                if (mEdit.toString().length() > 0 && mEdit.toString().charAt(0) == '0') {
                    showToast("First letter must not be 0");
                    edt_job_price.setText("");
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });

        edt_job_description = (EditText) v.findViewById(R.id.edt_job_description);

        jobFullDesc = postJob.getjobDescription();
        String[] DescParts = jobFullDesc.split("\\?@");
        if (DescParts.length > 0) {
            edt_job_description.setText(DescParts[DescParts.length - 1]);
        }

        return v;
    }


    @Override
    public void onClick(View view) {
        hideKeyBoard(view);
        switch (view.getId()) {

            case R.id.map_icon:
                if (isInternetConnected()) {
                    startActivityForResult(new Intent(getActivity(), GetAddressActivity.class), 111);
                } else {
                    showAlertDialog(getString(R.string.internet));
                }
                break;

            // remove click
            case R.id.tvLeft:
                confirmDialog();
                break;

            // save changes click
            case R.id.tvRight:
                if (isInternetConnected()) {
                    showLoading();

                    final String imagePath = mImagePair == null ? null : mImagePair.imagePath;
                    if (imagePath != null) {
                        postJob.setImage(getParseFile(imagePath));
                    }
                    if (!TextUtils.isNullOrEmpty(edt_job_address.getText().toString())) {
                        postJob.setjobAddress(edt_job_address.getText().toString());
                    }
                    if (!TextUtils.isNullOrEmpty(edt_job_description.getText().toString())) {
                        String jobDescSend = jobFullDesc + "?@" + edt_job_description.getText().toString();
                        postJob.setjobDescription(jobDescSend);
                    }
                    if (!TextUtils.isNullOrEmpty(edt_job_price.getText().toString())) {
                        postJob.setjobPrice(edt_job_price.getText().toString());
                    }
                    if (parseGeoPoint != null) {
                        postJob.setjobLatLong(parseGeoPoint);
                    }
                    postJob.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                            hideLoading();
                            if (e == null) {
                                showAlertDialog("Job updated.");
                            }
                        }
                    });
                } else {
                    showAlertDialog(getString(R.string.internet));
                }
                break;

            case R.id.tv_attach_image:
                showImageDialog();
                break;

            case R.id.tv_old_attachment:
                if (isInternetConnected()) {
                    if (postJob.getImage() != null) {
                        startActivity(new Intent(getActivity(), ImageDisplayActivity.class).putExtra(Constant.DATA, postJob.getImage()));
                    } else {
                        showToast("Image not available.");
                    }
                } else {
                    showToast(getString(R.string.internet));
                }
                break;

            case R.id.tv_history:
                displayPopupWindow(tv_history);
                break;

            default:
                break;
        }
    }

    private void confirmDialog() {
        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
        alert.setTitle("Remove Job");
        alert.setMessage("Are you sure, you want to remove this job?");
        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                if (isInternetConnected()) {
                    postJob.deleteInBackground(new DeleteCallback() {
                        @Override
                        public void done(ParseException e) {
                            if (e == null) {
                                displayView(Constant.MY_JOB_FRAGMENT, false);
                            }
                        }
                    });
                } else {
                    showAlertDialog(getString(R.string.internet));
                }
            }
        });
        alert.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        alert.show();
    }

//    private void confirmDialog() {
//        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
//        alert.setTitle("Remove Job");
//        alert.setMessage("Are you sure, you want to remove this job?");
//        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                dialog.dismiss();
//                if (isInternetConnected()) {
//                    //showLoading();
//                    postJob.deleteInBackground(new DeleteCallback() {
//                        @Override
//                        public void done(ParseException e) {
//                            if (e == null) {
//                                ParseQuery<JobApply> query = new ParseQuery<JobApply>(JobApply.class);
//                                query.whereEqualTo("postjobId", postJob);
//                                query.findInBackground(new FindCallback<JobApply>() {
//                                    @Override
//                                    public void done(List<JobApply> objects, ParseException e) {
//                                        if (e == null) {
//                                            for (int i = 0; i < objects.size(); i++) {
//                                                JobApply jobApply = objects.get(i);
//                                                jobApply.deleteInBackground(new DeleteCallback() {
//                                                    @Override
//                                                    public void done(ParseException e) {
//                                                        if (e == null) {
//                                                            ParseQuery<InvitesNotification> query1 = new ParseQuery<InvitesNotification>(InvitesNotification.class);
//                                                            query1.whereEqualTo("jobId", postJob);
//                                                            query1.findInBackground(new FindCallback<InvitesNotification>() {
//                                                                @Override
//                                                                public void done(List<InvitesNotification> objects, ParseException e) {
//                                                                    if (e == null) {
//                                                                        for (int i = 0; i < objects.size(); i++) {
//                                                                            InvitesNotification notification = objects.get(i);
//                                                                            notification.deleteInBackground(new DeleteCallback() {
//                                                                                @Override
//                                                                                public void done(ParseException e) {
//                                                                                    if (e == null) {
//                                                                                        displayView(Constant.MY_JOB_FRAGMENT, false);
//                                                                                    }
//                                                                                }
//                                                                            });
//                                                                        }
//                                                                    }
//                                                                }
//                                                            });
//                                                        }
//                                                    }
//                                                });
//                                            }
//                                        }
//                                    }
//                                });
//                            }
//                        }
//                    });
//                } else {
//                    showAlertDialog(getString(R.string.internet));
//                }
//            }
//        });
//        alert.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                dialog.dismiss();
//            }
//        });
//        alert.show();
//    }

    private void showImageDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(getString(R.string.add_image_from));
        builder.setItems(new String[]{getString(R.string.capture_image), getString(R.string.from_gallery)}, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (which == 0) {
                    Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    File f = AppUtils.createImageFile();
                    if ((f != null) && f.exists()) {
                        mImagePair = new ImageIntentHandler.ImagePair(image, f.getAbsolutePath());
                        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
                        startActivityForResult(takePictureIntent, ImageIntentHandler.REQUEST_CAPTURE);
                    } else {
                        Toast.makeText(getActivity(), "Camera error", Toast.LENGTH_LONG).show();
                    }
                    dialog.dismiss();
                } else if (which == 1) {
                    mImagePair = new ImageIntentHandler.ImagePair(image, null);
                    Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(i, ImageIntentHandler.REQUEST_GALLERY);
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    ParseGeoPoint parseGeoPoint;

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        WindowManager wm = (WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point point = new Point();
        display.getSize(point);
        int wid = point.x;
        int ht = point.y;

        Log.i("HeightandWidth", ""+wid);
        Log.i("HeightandWidth", ""+ht);

        ImageIntentHandler intentHandler =
                new ImageIntentHandler(getActivity(), mImagePair)
                        .folder(getString(R.string.app_name))
                        .sizePx(wid, ht);

        intentHandler.handleIntent(requestCode, resultCode, data);

        // Check which request we're responding to
        if (requestCode == 111) {
            // Make sure the request was successful
            if (resultCode == Activity.RESULT_OK) {
                // The user picked a contact.
                // The Intent's data Uri identifies which contact was selected.

                Debug.d(getClass().getSimpleName(), data.getStringExtra("ADDRESS"));
                edt_job_address.setText(data.getStringExtra("ADDRESS"));
                edt_job_address.setSelection(data.getStringExtra("ADDRESS").length());
                parseGeoPoint = new ParseGeoPoint(data.getDoubleExtra("LATITUDE", 0.00), data.getDoubleExtra("LONGITUDE", 0.00));
            }
        }
    }

    private void displayPopupWindow(View anchorView) {
        BetterPopupWindow betterPopupWindow = new BetterPopupWindow(anchorView);
        View view = getActivity().getLayoutInflater().inflate(R.layout.popup_content, null);
        betterPopupWindow.setContentView(view);
        betterPopupWindow.showLikeQuickAction();
    }

    private byte[] readInFile(String path) throws IOException {
        // TODO Auto-generated method stub
        byte[] data;
        File file = new File(path);
        InputStream input_stream = new BufferedInputStream(new FileInputStream(file));
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
        data = new byte[16384]; // 16K
        int bytes_read;
        while ((bytes_read = input_stream.read(data, 0, data.length)) != -1) {
            buffer.write(data, 0, bytes_read);
        }
        input_stream.close();
        return buffer.toByteArray();
    }

    private ParseFile getParseFile(String path) {
        ParseFile file = null;
        try {
            byte[] imageByte = readInFile(path);
            long time = System.currentTimeMillis();
            String sImageName = ParseUser.getCurrentUser().getObjectId() + time + ".jpeg";
            // Create the ParseFile
            file = new ParseFile(sImageName, imageByte);
            // Upload the image into Parse Cloud
            file.saveInBackground();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return file;
    }
}
