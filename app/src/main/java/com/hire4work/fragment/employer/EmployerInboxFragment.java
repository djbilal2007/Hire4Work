package com.hire4work.fragment.employer;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.hire4work.R;
import com.hire4work.activity.ChatActivity;
import com.hire4work.base.BaseFragment;
import com.hire4work.bean.JobApply;
import com.hire4work.bean.NotificationCounter;
import com.hire4work.bean.PersonData;
import com.hire4work.bean.PostJob;
import com.hire4work.customewidgets.BadgeView;
import com.hire4work.utills.Constant;
import com.hire4work.utills.Debug;
import com.hire4work.utills.TextUtils;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.List;

import static com.hire4work.utills.UserConstant.NAME;

/**
 * Created by Kailash on 15/09/2015.
 */
public class EmployerInboxFragment extends BaseFragment {

    private PostJob postJob;
    private ArrayList<JobApply> arrayList = new ArrayList<JobApply>();
    private WorkListAdapter workerAdapter;
    private Spinner spinner_worker_list;
    public static BadgeView view1;

    public static EmployerInboxFragment newInstance(BadgeView badgeView) {
        view1 = badgeView;
        Bundle args = new Bundle();

        EmployerInboxFragment fragment = new EmployerInboxFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = this.getArguments();
        postJob = (PostJob) bundle.getSerializable(Constant.DATA);
        Debug.d(getClass().getSimpleName(), postJob.getjobTitle());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_inbox, container, false);

        getWorker();

        workerAdapter = new WorkListAdapter(getActivity(), arrayList);
        spinner_worker_list = (Spinner) v.findViewById(R.id.spinner_worker_list);
        spinner_worker_list.setAdapter(workerAdapter);
        spinner_worker_list.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                JobApply workerBean = arrayList.get(position);
                if (isInternetConnected()) {
                    if (!workerBean.getApplyUserId().getString(NAME).equalsIgnoreCase("Select User")) {
                        updateNotificationCounter(workerBean);
                    }
                } else {
                    showToast(getString(R.string.internet));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        return v;
    }

    private void updateNotificationCounter(final JobApply workerBean) {
        ParseQuery<NotificationCounter> query = new ParseQuery<NotificationCounter>(NotificationCounter.class);
        query.whereEqualTo("jobId", postJob.getObjectId());
        query.whereEqualTo("receiverId", ParseUser.getCurrentUser().getObjectId());
        query.findInBackground(new FindCallback<NotificationCounter>() {
            @Override
            public void done(List<NotificationCounter> objects, ParseException e) {
                if (e == null) {
                    try {
                        if (!TextUtils.isNullOrEmpty(objects)) {
                            NotificationCounter counter = objects.get(0);
                            counter.setBedgeCount("0");
                            counter.saveInBackground();
                            view1.setText("0");
                        }
                        startActivity(new Intent(getActivity(), ChatActivity.class)
                                .putExtra(Constant.ID, workerBean.getApplyUserId().getObjectId())
                                .putExtra(Constant.NAME, workerBean.getPostJobId().getObjectId()));
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }
        });
    }

    private void getWorker() {
        showLoading();
        ParseQuery<JobApply> parseQuery = new ParseQuery<JobApply>(JobApply.class);
        parseQuery.include("postjobId");
        parseQuery.include("applyUserId");
        parseQuery.include("postUserId");
        parseQuery.whereEqualTo("postjobId", postJob);
        parseQuery.findInBackground(new FindCallback<JobApply>() {
            @Override
            public void done(final List<JobApply> list, ParseException e) {
                if (e == null) {

                    // spinner default value
                    PersonData personData = new PersonData();
                    personData.put(NAME, "Select User");
                    JobApply jobApply = new JobApply();
                    jobApply.setAssignedUserStatus(getString(R.string.app_name));
                    jobApply.setWorkRate("45");
                    jobApply.setApplyUserId(personData);
                    jobApply.setJobAppliedStatus(getString(R.string.app_name));
                    jobApply.setPostJobId(postJob);
                    jobApply.setPostUserId(ParseUser.getCurrentUser());

                    arrayList.add(0, jobApply);
                    arrayList.addAll(list);
                    workerAdapter.notifyDataSetChanged();
                    hideLoading();
                }
            }
        });
    }

    public class WorkListAdapter extends ArrayAdapter<JobApply> {

        List<JobApply> categoryList;
        Context context;

        public WorkListAdapter(Context ctx, List<JobApply> objects) {
            super(ctx, R.layout.spinner_text, objects);
            this.categoryList = objects;
            this.context = ctx;
        }

        @Override
        public View getDropDownView(int position, View cnvtView, ViewGroup prnt) {
            return getCustomView(position, cnvtView, prnt);
        }

        @Override
        public View getView(int pos, View cnvtView, ViewGroup prnt) {
            return getCustomView(pos, cnvtView, prnt);
        }

        public View getCustomView(int position, View convertView, ViewGroup parent) {
            JobApply jobApply = getItem(position);

            LayoutInflater inflater = LayoutInflater.from(getContext());
            View mySpinner = inflater.inflate(R.layout.spinner_text, parent, false);
            TextView main_text = (TextView) mySpinner.findViewById(R.id.spinner);
            if (!TextUtils.isNullOrEmpty(jobApply.getApplyUserId().getString(NAME))) {
                main_text.setText(jobApply.getApplyUserId().getString(NAME));
            }
            return mySpinner;
        }
    }
}