package com.hire4work.fragment.employer;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.hire4work.R;
import com.hire4work.activity.ProfileActivity;
import com.hire4work.base.BaseListFragment;
import com.hire4work.bean.FundTransection;
import com.hire4work.bean.JobApply;
import com.hire4work.bean.PostJob;
import com.hire4work.bean.PushNotification;
import com.hire4work.bean.UserRatting;
import com.hire4work.customewidgets.DontPressWithParentButton;
import com.hire4work.utills.Constant;
import com.hire4work.utills.Debug;
import com.hire4work.utills.TextUtils;
import com.hire4work.utills.UserConstant;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParsePush;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.parse.SendCallback;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;

import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static com.hire4work.utills.UserConstant.NAME;
import static com.hire4work.utills.UserConstant.PAYPAL_USER_ID;

/**
 * Created by Kailash on 25-Sep-15.
 */
public class WorkerFragment extends BaseListFragment implements View.OnClickListener {

    private PostJob postJob;
    private ArrayList<JobApply> arrayList = new ArrayList<JobApply>();
    private WorkerAdapter workerAdapter;
    private TextView tv_assign_job;
    private ImageView iv_back;

    private String TAG = getClass().getSimpleName();

    private static final String CONFIG_ENVIRONMENT = PayPalConfiguration.ENVIRONMENT_NO_NETWORK;

    // note that these credentials will differ between live & sandbox environments.
    private static final String CONFIG_CLIENT_ID = "AQB2tx-ak_BRKlF40fNnJFCyT4Tf-eKDW60mV21wSeA8bRfD21lLJWwZCnsg5hgJDP8uSHe1__5tHYh-";

    private static final int REQUEST_CODE_PAYMENT = 1;

    private JobApply beanAssignedWorker;
    private int perHourCharges, fundRelease, extraCharges;



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = this.getArguments();
        postJob = (PostJob) bundle.getSerializable(Constant.DATA);
        Debug.d(getClass().getSimpleName(), postJob.getjobTitle());

        Intent intent = new Intent(getActivity(), PayPalService.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        getActivity().startService(intent);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_worker, container, false);

        tv_assign_job = (TextView) v.findViewById(R.id.tv_assign_job);
        tv_assign_job.setEnabled(false);
        tv_assign_job.setOnClickListener(this);

        getWorker();

        workerAdapter = new WorkerAdapter(getActivity(), arrayList);
        setListAdapter(workerAdapter);

        return v;
    }

    private void getWorker() {
        showLoading();
        ParseQuery<JobApply> parseQuery = new ParseQuery<JobApply>(JobApply.class);
        parseQuery.whereEqualTo("postjobId", postJob);
        parseQuery.include("postjobId");
        parseQuery.include("applyUserId");
        parseQuery.include("postUserId");
        parseQuery.findInBackground(new FindCallback<JobApply>() {
            @Override
            public void done(final List<JobApply> list, ParseException e) {
                if (e == null) {
                    arrayList.addAll(list);
                    workerAdapter.notifyDataSetChanged();
                    hideLoading();
                }
            }
        });
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);

        l.setItemChecked(position, true);
        l.setSelection(position);
        tv_assign_job.setEnabled(true);
        beanAssignedWorker = arrayList.get(position);

        String[] separated = beanAssignedWorker.getWorkRate().split(" ");

        perHourCharges = Integer.parseInt(separated[0].trim());
        fundRelease = perHourCharges * 8;
        extraCharges = (fundRelease * 10) / 100;
        Debug.d(TAG, "Per Hour Charges : " + perHourCharges + "\n Fund Release : " + fundRelease + "\n Extra Charges : " + extraCharges);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_assign_job:
                if (isInternetConnected()) {
                /*
                * PAYMENT_INTENT_SALE will cause the payment to complete immediately.
                * Change PAYMENT_INTENT_SALE to
                *   - PAYMENT_INTENT_AUTHORIZE to only authorize payment and capture funds later.
                *   - PAYMENT_INTENT_ORDER to create a payment for authorization and capture
                *     later via calls from your server.
                *
                * Also, to include additional payment details and an item list, see getStuffToBuy() below.
                */
                    PayPalPayment thingToBuy = getThingToBuy(PayPalPayment.PAYMENT_INTENT_SALE);

                /*
                * See getStuffToBuy(..) for examples of some available payment options.
                */

                    Intent intent = new Intent(getActivity(), PaymentActivity.class);

                    // send the same configuration for restart resiliency
                    intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);

                    intent.putExtra(PaymentActivity.EXTRA_PAYMENT, thingToBuy);

                    startActivityForResult(intent, REQUEST_CODE_PAYMENT);
                } else {
                    showAlertDialog(getString(R.string.internet));
                }
                break;

            default:
                break;
        }
    }

    private PayPalPayment getThingToBuy(String paymentIntent) {
        return new PayPalPayment(new BigDecimal(fundRelease), "USD", "Assign Job",
                paymentIntent);
    }

    private static PayPalConfiguration config = new PayPalConfiguration()
            .environment(CONFIG_ENVIRONMENT)
            .clientId(CONFIG_CLIENT_ID)
                    // The following are only used in PayPalFuturePaymentActivity.
            .merchantName("Example Merchant")
            .merchantPrivacyPolicyUri(Uri.parse("https://www.example.com/privacy"))
            .merchantUserAgreementUri(Uri.parse("https://www.example.com/legal"));

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_PAYMENT) {
            if (resultCode == Activity.RESULT_OK) {
                PaymentConfirmation confirm =
                        data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
                if (confirm != null) {
                    try {
                        Debug.i(TAG, confirm.toJSONObject().toString(4));
                        Debug.i(TAG, confirm.getPayment().toJSONObject().toString(4));
                        /**
                         *  TODO: send 'confirm' (and possibly confirm.getPayment() to your server for verification
                         * or consent completion.
                         * See https://developer.paypal.com/webapps/developer/docs/integration/mobile/verify-mobile-payment/
                         * for more details.
                         *
                         * For sample mobile backend interactions, see
                         * https://github.com/paypal/rest-api-sdk-python/tree/master/samples/mobile_backend
                         */

                        showToast("PaymentConfirmation info received from PayPal");

                        // update transaction detail to Server
                        updateFundTransection();

                    } catch (JSONException e) {
                        Debug.e(TAG, "an extremely unlikely failure occurred: " + e);
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Debug.i(TAG, "The user canceled.");
            } else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {
                Debug.i(TAG,
                        "An invalid Payment or PayPalConfiguration was submitted. Please see the docs.");
            }
        }
    }

    private void updateFundTransection() {
        showLoading();
        FundTransection fundTransection = new FundTransection();
        fundTransection.setStatus("approved");
        fundTransection.setEmployerId(ParseUser.getCurrentUser().getObjectId());
        fundTransection.setEmployerPaypal(ParseUser.getCurrentUser().getString(PAYPAL_USER_ID));
        fundTransection.setPerHourCharges(String.valueOf(beanAssignedWorker.getWorkRate()));
        fundTransection.setJobId(beanAssignedWorker.getPostJobId().getObjectId());
        fundTransection.setTotalAmount(String.valueOf(fundRelease));
        fundTransection.setWorkerId(beanAssignedWorker.getApplyUserId().getObjectId());
        fundTransection.setWorkerPaypal(beanAssignedWorker.getApplyUserId().getString(PAYPAL_USER_ID));

        fundTransection.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null) {
                    updatePostJobData();
                }
            }
        });
    }

    private void updatePostJobData() {
        ArrayList<ParseUser> assignedUser = new ArrayList<ParseUser>();
        assignedUser.add(beanAssignedWorker.getApplyUserId());

        postJob.setjobAssignStatus("YES");
        postJob.setLiveJobStatus("YES");
        postJob.setWorkerId(beanAssignedWorker.getApplyUserId().getObjectId());
        postJob.setAppliedUser(assignedUser);
        postJob.saveInBackground();

        beanAssignedWorker.setAssignedUserStatus("YES");
        beanAssignedWorker.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null) {

                    sendNotification();
                }
            }
        });
    }

    private WorkerAdapter.ViewHolder holder;

    public class WorkerAdapter extends ArrayAdapter<JobApply> {

        public WorkerAdapter(Context context, List<JobApply> list) {
            super(context, R.layout.item_worker, list);
        }

        @Override
        public View getView(int pos, View convertView, ViewGroup parent) {

            final JobApply jobApply = getItem(pos);

            if (convertView == null) {
                holder = new ViewHolder();

                convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_worker, parent, false);

                holder.tv_user_name = (TextView) convertView.findViewById(R.id.tv_user_name);

                holder.rating_bar = (RatingBar) convertView.findViewById(R.id.rating_bar);

                holder.iv_info = (DontPressWithParentButton) convertView.findViewById(R.id.iv_info);

                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            try {
                holder.tv_user_name.setText(jobApply.getApplyUserId().getString(NAME) + " $" + jobApply.getWorkRate() + "/hr");
                getUserRating(jobApply.getApplyUserId().getObjectId());
                //holder.rating_bar.setRating(ratingNumber);
            } catch (Exception e) {
                e.printStackTrace();
            }

            holder.iv_info.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    showToast("info button click");
                    startActivity(new Intent(getActivity(), ProfileActivity.class).putExtra(Constant.DATA, jobApply.getApplyUserId().getObjectId()));
                }
            });

            return convertView;
        }

        class ViewHolder {
            TextView tv_user_name;
            RatingBar rating_bar;
            DontPressWithParentButton iv_info;
        }
    }

    float ratingNumber = 0;

    private void getUserRating(String workerId) {
        ParseQuery<UserRatting> rattingParseQuery = new ParseQuery<UserRatting>(UserRatting.class);
        rattingParseQuery.whereEqualTo("workerId", workerId);
        rattingParseQuery.findInBackground(new FindCallback<UserRatting>() {
            @Override
            public void done(List<UserRatting> list, ParseException e) {
                if (e == null) {
                    try {
                        if (!TextUtils.isNullOrEmpty(list)) {
                            for (int i = 0; i < list.size(); i++) {
                                ratingNumber = ratingNumber + list.get(i).getRatingPoint().floatValue();
                            }
                            ratingNumber = ratingNumber / list.size();
                            holder.rating_bar.setRating(ratingNumber);
                            Log.d("WorkerFragment","Rating: " + ratingNumber);
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }
        });
    }

    private void sendNotification() {
        showLoading();
        // Create our Installation query
        ParseQuery pushQuery = ParseInstallation.getQuery();
        pushQuery.whereEqualTo("channels", "global");
        pushQuery.whereEqualTo("installedUserId", beanAssignedWorker.getApplyUserId().getObjectId());

        JSONObject jsonObject = new JSONObject();
        try {
            JSONObject data = new JSONObject();
            data.put("badge", "0");
            data.put("alert", "Job has been assigned to you by " + ParseUser.getCurrentUser().getString(NAME));
            jsonObject.put("type", "JobAssigned");
            jsonObject.put("jobId", beanAssignedWorker.getPostJobId().getObjectId());
            jsonObject.put("aps", data);
            jsonObject.put("workerId", beanAssignedWorker.getApplyUserId().getObjectId());
            jsonObject.put("userId", ParseUser.getCurrentUser().getObjectId());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        // Send push notification to query
        ParsePush push = new ParsePush();
        push.setQuery(pushQuery); // Set our Installation query
        push.setData(jsonObject);
        push.sendInBackground(new SendCallback() {
            @Override
            public void done(ParseException e) {
                hideLoading();
                if (e == null) {
                    updatePushToServer();
                }
            }
        });
    }

    private void updatePushToServer() {
        PushNotification pushNotification = new PushNotification();
        pushNotification.setJobId(postJob.getObjectId());
        pushNotification.setAlertMessage("Job has been assigned to you by " + ParseUser.getCurrentUser().getString(UserConstant.NAME));
        pushNotification.setEmployerId(ParseUser.getCurrentUser().getObjectId());
        pushNotification.setType("Job Assign Status");
        pushNotification.setWorkerId(beanAssignedWorker.getApplyUserId().getObjectId());
        if (isEmployer()) {
            pushNotification.setuserType("employer");
        } else {
            pushNotification.setuserType("employee");
        }
        pushNotification.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null) {
                    hideLoading();
//                    updateFundTransaction(value);
                    displayView(Constant.MY_JOB_FRAGMENT, false);
                    showAlertDialog("Job assigned successfully.");
                }
            }
        });
    }
}
