package com.hire4work.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.hire4work.R;
import com.hire4work.activity.ProfileActivity;
import com.hire4work.base.BaseListFragment;
import com.hire4work.bean.InvitesNotification;
import com.hire4work.bean.PersonData;
import com.hire4work.bean.PostJob;
import com.hire4work.bean.PushNotification;
import com.hire4work.bean.UserRatting;
import com.hire4work.utills.Constant;
import com.hire4work.utills.TextUtils;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.ParseInstallation;
import com.parse.ParsePush;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.parse.SendCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.hire4work.utills.Constant.INVITATION_REQUEST;
import static com.hire4work.utills.UserConstant.CURRENT_LOCATION;
import static com.hire4work.utills.UserConstant.NAME;
import static com.hire4work.utills.UserConstant.PROFESSION;

/**
 * Created by Kailash on 24-Sep-15.
 */
public class EmployeeListFragment extends BaseListFragment implements View.OnClickListener {

    private TextView tvLeft, tvRight, tv_select_user;
    private TextView tv_title;
    private ImageView iv_back;
    private List<PersonData> userList = new ArrayList<PersonData>();
    private List<String> categoryStringList = new ArrayList<>();
    private PostJob postJob;

    private InviteUserAdapter inviteUserAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup root = (ViewGroup) inflater.inflate(R.layout.fragment_employee_list, null);

        tv_title = (TextView) root.findViewById(R.id.tv_title);
        tv_title.setText("List of employee");

        iv_back = (ImageView) root.findViewById(R.id.iv_back);
        iv_back.setOnClickListener(this);

        tv_select_user = (TextView) root.findViewById(R.id.tv_select_user);
        tv_select_user.setVisibility(View.VISIBLE);
        tv_select_user.setText("Select");
        tv_select_user.setOnClickListener(this);

        tvLeft = (TextView) root.findViewById(R.id.tvLeft);
        tvLeft.setText(getString(R.string.cancel));
        tvLeft.setOnClickListener(this);

        tvRight = (TextView) root.findViewById(R.id.tvRight);
        tvRight.setText(getString(R.string.invite));
        tvRight.setOnClickListener(this);

        inviteUserAdapter = new InviteUserAdapter(getActivity(), userList);
        setListAdapter(inviteUserAdapter);

        getInvitesUser();

        return root;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = this.getArguments();
        postJob = (PostJob) bundle.getSerializable(Constant.DATA);
        categoryStringList.add(postJob.getjobCategory());
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        if (isInternetConnected()) {
            startActivity(new Intent(getActivity(), ProfileActivity.class).putExtra(Constant.DATA, userList.get(position).getObjectId()));
        } else {
            showAlertDialog(getString(R.string.internet));
        }
    }

    private void getInvitesUser() {
        ParseQuery<PersonData> query = new ParseQuery<PersonData>(PersonData.class);
        ParseGeoPoint parseGeoPoint = new ParseGeoPoint(getMyLocation().latitude, getMyLocation().longitude);
        query.whereWithinKilometers(CURRENT_LOCATION, parseGeoPoint, 5);
        query.whereContainedIn(PROFESSION, categoryStringList);
        query.findInBackground(new FindCallback<PersonData>() {
            public void done(List<PersonData> objects, ParseException e) {
                if (e == null) {
                    // The query was successful.
                    if (TextUtils.isNullOrEmpty(objects)) {
                        showToast("No user found for posted job category.");
                    } else {
                        userList.clear();
                        userList.addAll(objects);
                        inviteUserAdapter.notifyDataSetChanged();
                    }
                } else {
                    // Something went wrong.
                    showAlertDialog(e.getMessage());
                }
            }
        });
    }

    boolean showCheckBox = false;
    List<PersonData> selectedUserList = new ArrayList<PersonData>();

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            // cancel button
            case R.id.tvLeft:
                displayView(Constant.POST_JOB_FRAGMENT, false);
                break;

            // invite button
            case R.id.tvRight:
                if (isInternetConnected()) {
                    for (int i = 0; i < userList.size(); i++) {
                        if (userList.get(i).isSelected()) {
                            selectedUserList.add(userList.get(i));
                        }
                    }

                    if (TextUtils.isNullOrEmpty(selectedUserList)) {
                        showAlertDialog("Select User to invite.");
                    } else {
                        showLoading();
                        recursion();
                    }
                } else {
                    showAlertDialog(getString(R.string.internet));
                }
                break;

            case R.id.iv_back:
                displayView(Constant.POST_JOB_FRAGMENT, false);
                break;

            case R.id.tv_select_user:
                showCheckBox = true;
                inviteUserAdapter.notifyDataSetChanged();
                break;

            default:
                break;
        }
    }

    static int k = 0;

    private void recursion() {
        if (k < selectedUserList.size()) {
            PersonData personData = selectedUserList.get(k);
            if (personData.isSelected()) {
                sendInvite(personData);
            }
        } else {
            hideLoading();
            showToast("Invite Send.");
            displayView(Constant.POST_JOB_FRAGMENT, false);
        }
    }

    private void sendInvite(final PersonData personData) {
        InvitesNotification invitesNotification = new InvitesNotification();
        invitesNotification.setIsStatus("1");
        invitesNotification.setJobCategory(postJob.getjobCategory());
        invitesNotification.setJobId(postJob);
        invitesNotification.setNotificationMessage(ParseUser.getCurrentUser().getString(NAME) + " invited you to apply for their job.");
        invitesNotification.setRecieverUserId(personData.getObjectId());
        invitesNotification.setSenderUserId(ParseUser.getCurrentUser());
        invitesNotification.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null) {
                    Log.d("EmployeeListFrag", "EmployeeListFrag: e-InviteNotification is null");
                    sendNotification(personData.getObjectId());
                }
                else {
                    Log.d("EmployeeListFrag", "EmployeeListFrag: InviteNotification:Exception: " + e.getMessage());
                }
            }
        });
    }

    private void sendNotification(final String workerId) {
        ParseQuery pushQuery = ParseInstallation.getQuery();
        pushQuery.whereEqualTo("channels", "global");
        pushQuery.whereEqualTo("installedUserId", workerId);

        JSONObject jsonObject = new JSONObject();
        try {
            JSONObject data = new JSONObject();
            data.put("badge", "0");
            data.put("alert", "One job invitation in your inbox");

            jsonObject.put("type", INVITATION_REQUEST);
            jsonObject.put("jobId", postJob.getObjectId());
            jsonObject.put("aps", data);
            jsonObject.put("workerId", workerId);
            jsonObject.put("userId", ParseUser.getCurrentUser().getObjectId());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        // Send push notification to query
        ParsePush push = new ParsePush();
        push.setQuery(pushQuery); // Set our Installation query
        push.setData(jsonObject);
        push.sendInBackground(new SendCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null) {
                    Log.d("EmployeeListFrag", "EmployeeListFrag: e-ParsePush is null");
                    updatePushToServer(workerId);
                }
                else {
                    updatePushToServer(workerId);
                 //   k++;
                    Log.d("EmployeeFrag", "ParsePush :" + e.getMessage() + "k: " + k);
                }
            }
        });
    }

    private void updatePushToServer(final String workerId) {
        PushNotification pushNotification = new PushNotification();
        pushNotification.setJobId(postJob.getObjectId());
        pushNotification.setAlertMessage("One job invitation in your inbox");
        pushNotification.setEmployerId(ParseUser.getCurrentUser().getObjectId());
        pushNotification.setType(INVITATION_REQUEST);
        pushNotification.setWorkerId(workerId);
        if (isEmployer()) {
            pushNotification.setuserType("employee");
        } else {
            pushNotification.setuserType("employer");
        }
        pushNotification.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null) {
                    k++;
                    Log.d("EmployeeListFrag", "EmployeeListFrag: e-PushNotification is null");
                    recursion();
                }
                else {
                    Log.d("EmployeeListFrag", "EmployeeListFrag: e-PushNotification is not null" + e.getMessage());
                }
            }
        });
    }

    public class InviteUserAdapter extends ArrayAdapter<PersonData> {
        List<PersonData> inviteList;

        public InviteUserAdapter(Context context, List<PersonData> list) {
            super(context, R.layout.item_employee_list, list);
            this.inviteList = new ArrayList<PersonData>();
            this.inviteList.addAll(list);
        }

        @Override
        public View getView(int pos, View convertView, ViewGroup parent) {
            final ViewHolder holder;

            if (convertView == null) {
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_employee_list, parent, false);

                holder = new ViewHolder();

                holder.tv_user_name = (TextView) convertView.findViewById(R.id.tv_user_name);

                holder.tv_category = (TextView) convertView.findViewById(R.id.tv_category);

                holder.tv_description = (TextView) convertView.findViewById(R.id.tv_description);

                holder.rating_bar = (RatingBar) convertView.findViewById(R.id.rating_bar);

                holder.checkBox = (CheckBox) convertView.findViewById(R.id.checkBox);

                convertView.setTag(holder);

                holder.checkBox.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        CheckBox cb = (CheckBox) v;
                        PersonData personData = (PersonData) cb.getTag();
                        personData.setSelected(cb.isChecked());
                    }
                });

            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            PersonData parseUser = getItem(pos);

            holder.tv_user_name.setText(parseUser.getName());

            List<String> stringArrayList = parseUser.getProffesion();
            if (!TextUtils.isNullOrEmpty(stringArrayList)) {
                String s = "";
                for (int i = 0; i < stringArrayList.size(); i++) {
                    s = s + stringArrayList.get(i) + ", ";
                }
                holder.tv_category.setText(s);
            }

            holder.tv_description.setText(parseUser.getSkills());

            if (showCheckBox) {
                holder.checkBox.setVisibility(View.VISIBLE);
                holder.checkBox.setChecked(parseUser.isSelected());
                holder.checkBox.setTag(parseUser);
            } else {
                holder.checkBox.setVisibility(View.GONE);
            }

            holder.rating_bar.setRating(getUserRating(parseUser.getObjectId()));

            return convertView;
        }

        class ViewHolder {
            TextView tv_user_name, tv_category, tv_description;
            RatingBar rating_bar;
            CheckBox checkBox;
        }
    }

    float ratingNumber = 0;

    private float getUserRating(String workerId) {
        ParseQuery<UserRatting> rattingParseQuery = new ParseQuery<UserRatting>(UserRatting.class);
        rattingParseQuery.whereEqualTo("workerId", workerId);
        rattingParseQuery.findInBackground(new FindCallback<UserRatting>() {
            @Override
            public void done(List<UserRatting> list, ParseException e) {
                if (e == null) {
                    try {
                        if (!TextUtils.isNullOrEmpty(list)) {
                            for (int i = 0; i < list.size(); i++) {
                                ratingNumber = ratingNumber + list.get(i).getRatingPoint().floatValue();
                            }
                            ratingNumber = ratingNumber / list.size();
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }
        });
        return ratingNumber;
    }
}
