package com.hire4work.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.hire4work.R;
import com.hire4work.base.BaseListFragment;
import com.hire4work.bean.PushNotification;
import com.hire4work.utills.Keys;
import com.hire4work.utills.SharedPrefrenceManager;
import com.hire4work.utills.TextUtils;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 * Created by Kailash on 21-Sep-2015.
 */
public class NewsFragment extends BaseListFragment {

    private TextView tv_title, tv_empty;
    private ImageView iv_back;
    private SharedPrefrenceManager sharedPrefrenceManager = SharedPrefrenceManager.getInstance();
    ViewStub stub;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup root = (ViewGroup) inflater.inflate(R.layout.fragment_news, null);

        tv_title = (TextView) root.findViewById(R.id.tv_title);
        tv_title.setText(getActivity().getResources().getString(R.string.news));

        iv_back = (ImageView) root.findViewById(R.id.iv_back);
        iv_back.setVisibility(View.GONE);

        stub = (ViewStub) root.findViewById(android.R.id.empty);
        View inflated = stub.inflate();

        tv_empty = (TextView) inflated.findViewById(R.id.tv_empty);

        //getNews();

        if (sharedPrefrenceManager.getSavedPrefrenceValue(getActivity(), "", Keys.IS_LOGIN).equalsIgnoreCase("true")) {
            getPushNotification();
            tv_empty.setText("Welcome to Hire4Hour app. Here you can explore a lot of job opportunities as an Employer and as an Employee.");
        } else {
            tv_empty.setText("Please register with STAFFING APP to explore new job opportunities and make a job profile for yourself.");
        }

        return root;
    }

    private void getPushNotification() {
        showLoading();
        ParseQuery<PushNotification> query = new ParseQuery<PushNotification>(PushNotification.class);
        if (isEmployer()) {
            query.whereEqualTo("userType", "employer");
            query.whereEqualTo("employerId", ParseUser.getCurrentUser().getObjectId());
        } else {
            query.whereEqualTo("userType", "employee");
            query.whereEqualTo("workerId", ParseUser.getCurrentUser().getObjectId());
        }
        query.addDescendingOrder("createdAt");
        query.findInBackground(new FindCallback<PushNotification>() {
            @Override
            public void done(List<PushNotification> list, ParseException e) {
                try {
                    hideLoading();
                    if (e == null) {
                        if (TextUtils.isNullOrEmpty(list)) {
                            stub.setVisibility(View.VISIBLE);
                            showToast("Nothing to display.");
                        } else {
                            stub.setVisibility(View.INVISIBLE);
                            setListAdapter(new NewsAdapter(getActivity(), list));
                        }
                    } else {
                        showToast("Error fetching data. Try after some time.");
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        });
    }

//    public void getNews() {
//        showLoading(getString(R.string.please_wait));
//        ParseQuery<News> query = new ParseQuery<News>(News.class);
//        query.findInBackground(new FindCallback<News>() {
//            @Override
//            public void done(List<News> list, ParseException e) {
//                hideLoading();
//                if (e == null) {
////                    setListAdapter(new NewsAdapter(getActivity(), list));
//                } else {
//                    showToast("Error fetching data. Try after some time.");
//                }
//            }
//        });
//    }

    public class NewsAdapter extends ArrayAdapter<PushNotification> {

        public NewsAdapter(Context ctx, List<PushNotification> objects) {
            super(ctx, R.layout.item_news, objects);
        }

        @Override
        public View getView(int pos, View convertView, ViewGroup parent) {
            ViewHolder holder;
            PushNotification news = getItem(pos);

            if (convertView == null) {
                holder = new ViewHolder();

                convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_news, parent, false);

                holder.tv_news_title = (TextView) convertView.findViewById(R.id.tv_news_title);
                holder.tv_news_content = (TextView) convertView.findViewById(R.id.tv_news_content);
                holder.tv_posted_date = (TextView) convertView.findViewById(R.id.tv_posted_date);
//                holder.tv_posted_date.setVisibility(View.GONE);
                holder.invite_to_bid = (TextView) convertView.findViewById(R.id.invite_to_bid);
                holder.invite_to_bid.setVisibility(View.GONE);

                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            holder.tv_news_title.setText(news.getType());
            holder.tv_news_content.setText(news.getAlertMessage());

            Format formatter = new SimpleDateFormat("dd MMM yyyy");
            String s = formatter.format(news.getCreatedAt());

//            Log.d(getClass().getSimpleName(), "date :" + news.getDate());
            holder.tv_posted_date.setText(s);

            return convertView;
        }

        class ViewHolder {
            TextView tv_news_title, tv_news_content, tv_posted_date, invite_to_bid;
        }
    }
}
