package com.hire4work.fragment;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.SpannableString;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hire4work.R;
import com.hire4work.base.BaseFragment;
import com.hire4work.utills.Debug;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;

import org.json.JSONException;

import java.math.BigDecimal;

/**
 * Created by Kailash on 14-Sep-15.
 */
public class MembershipFragment extends BaseFragment {

    private TextView tv_paypal;
    private LinearLayout layout_paypal;
    private String TAG = getClass().getSimpleName();

    private static final String CONFIG_ENVIRONMENT = PayPalConfiguration.ENVIRONMENT_SANDBOX;

    // note that these credentials will differ between live & sandbox environments.
    private static final String CONFIG_CLIENT_ID = "AQB2tx-ak_BRKlF40fNnJFCyT4Tf-eKDW60mV21wSeA8bRfD21lLJWwZCnsg5hgJDP8uSHe1__5tHYh-";

    private static final int REQUEST_CODE_PAYMENT = 1;
//    private static final int REQUEST_CODE_FUTURE_PAYMENT = 2;
//    private static final int REQUEST_CODE_PROFILE_SHARING = 3;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_membership, container, false);

        tv_paypal = (TextView) v.findViewById(R.id.tv_paypal);
        SpannableString styledString = new SpannableString("PAYPAL");   // index 103 - 112
        // make text italic
        styledString.setSpan(new StyleSpan(Typeface.ITALIC), 0, styledString.length(), 0);
        tv_paypal.setText(styledString);

        layout_paypal = (LinearLayout) v.findViewById(R.id.layout_paypal);
        layout_paypal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*
                * PAYMENT_INTENT_SALE will cause the payment to complete immediately.
                * Change PAYMENT_INTENT_SALE to
                *   - PAYMENT_INTENT_AUTHORIZE to only authorize payment and capture funds later.
                *   - PAYMENT_INTENT_ORDER to create a payment for authorization and capture
                *     later via calls from your server.
                *
                * Also, to include additional payment details and an item list, see getStuffToBuy() below.
                */
                PayPalPayment thingToBuy = getThingToBuy(PayPalPayment.PAYMENT_INTENT_SALE);

                /*
                * See getStuffToBuy(..) for examples of some available payment options.
                */

                Intent intent = new Intent(getActivity(), PaymentActivity.class);

                // send the same configuration for restart resiliency
                intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);

                intent.putExtra(PaymentActivity.EXTRA_PAYMENT, thingToBuy);

                startActivityForResult(intent, REQUEST_CODE_PAYMENT);
            }
        });
        return v;
    }

    private PayPalPayment getThingToBuy(String paymentIntent) {
        return new PayPalPayment(new BigDecimal("1.75"), "USD", "sample item",
                paymentIntent);
    }

    private static PayPalConfiguration config = new PayPalConfiguration()
            .environment(CONFIG_ENVIRONMENT)
            .clientId(CONFIG_CLIENT_ID)
                    // The following are only used in PayPalFuturePaymentActivity.
            .merchantName("Example Merchant")
            .merchantPrivacyPolicyUri(Uri.parse("https://www.example.com/privacy"))
            .merchantUserAgreementUri(Uri.parse("https://www.example.com/legal"));

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = new Intent(getActivity(), PayPalService.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        getActivity().startService(intent);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_PAYMENT) {
            if (resultCode == Activity.RESULT_OK) {
                PaymentConfirmation confirm =
                        data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
                if (confirm != null) {
                    try {
                        Debug.i(TAG, confirm.toJSONObject().toString(4));
                        Debug.i(TAG, confirm.getPayment().toJSONObject().toString(4));
                        /**
                         *  TODO: send 'confirm' (and possibly confirm.getPayment() to your server for verification
                         * or consent completion.
                         * See https://developer.paypal.com/webapps/developer/docs/integration/mobile/verify-mobile-payment/
                         * for more details.
                         *
                         * For sample mobile backend interactions, see
                         * https://github.com/paypal/rest-api-sdk-python/tree/master/samples/mobile_backend
                         */

                        showToast("PaymentConfirmation info received from PayPal");

                    } catch (JSONException e) {
                        Debug.e(TAG, "an extremely unlikely failure occurred: " + e);
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Debug.i(TAG, "The user canceled.");
            } else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {
                Debug.i(
                        TAG,
                        "An invalid Payment or PayPalConfiguration was submitted. Please see the docs.");
            }
        }
    }
}
