package com.hire4work.fragment;


import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.hire4work.R;
import com.hire4work.activity.SelectCategoryActivity;
import com.hire4work.base.BaseFragment;
import com.hire4work.customewidgets.BezelImageView;
import com.hire4work.utills.AppUtils;
import com.hire4work.utills.Constant;
import com.hire4work.utills.Debug;
import com.hire4work.utills.PermissionsUtils;
import com.hire4work.utills.TextUtils;
import com.hire4work.utills.image.ImageIntentHandler;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseGeoPoint;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.List;

import static com.hire4work.utills.UserConstant.CITY;
import static com.hire4work.utills.UserConstant.CONTACT_NUMBER;
import static com.hire4work.utills.UserConstant.COUNTRY;
import static com.hire4work.utills.UserConstant.CURRENT_LOCATION;
import static com.hire4work.utills.UserConstant.NAME;
import static com.hire4work.utills.UserConstant.PAYPAL_USER_ID;
import static com.hire4work.utills.UserConstant.PICTURE;
import static com.hire4work.utills.UserConstant.PROFESSION;
import static com.hire4work.utills.UserConstant.RATE;
import static com.hire4work.utills.UserConstant.SKILLS;
import static com.hire4work.utills.UserConstant.ZIP_CODE;

/**
 * Created by Kailash on 19-Aug-15.
 */
public class EditProfileFragment extends BaseFragment implements View.OnClickListener {

    private String TAG = getClass().getSimpleName();
    private EditText fullnameEdt, contactNumberEdt, yourRateEdt, edt_country, descriptionEdt, edt_city, edt_pin_code, edt_paypal_id, edt_category;
    private String fullName, contactNumber, yourRate, country, description, city, zipCode, paypalId;
    private TextView DoneBtn, cancelBtn;
    private ImageView iv_background;
    private BezelImageView profileImage;
    private ParseUser currentUser;

    // for handling image
    private ImageIntentHandler.ImagePair mImagePair;

    List<String> stringArrayList;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        ViewGroup root = (ViewGroup) inflater.inflate(R.layout.fragment_edit_profile, null);

        currentUser = ParseUser.getCurrentUser();

        DoneBtn = (TextView) root.findViewById(R.id.DoneBtn);
        DoneBtn.setOnClickListener(this);

        cancelBtn = (TextView) root.findViewById(R.id.cancelBtn);
        cancelBtn.setOnClickListener(this);

        iv_background = (ImageView) root.findViewById(R.id.iv_background);

        fullnameEdt = (EditText) root.findViewById(R.id.fullnameEdt);
        if (!TextUtils.isNullOrEmpty(currentUser.getString(NAME))) {
            fullnameEdt.setText(currentUser.getString(NAME));
            fullnameEdt.setSelection(currentUser.getString(NAME).length());
        }

        contactNumberEdt = (EditText) root.findViewById(R.id.contactNumberEdt);
        if (!TextUtils.isNullOrEmpty(currentUser.getString(CONTACT_NUMBER))) {
            contactNumberEdt.setText(currentUser.getString(CONTACT_NUMBER));
            contactNumberEdt.setSelection(currentUser.getString(CONTACT_NUMBER).length());
        }

        yourRateEdt = (EditText) root.findViewById(R.id.yourRateEdt);
        if (!TextUtils.isNullOrEmpty(currentUser.getString(RATE))) {
            String charges;
            if (currentUser.getString(RATE).contains("$")) {
                charges = currentUser.getString(RATE).replace("$", "").trim();
            } else {
                charges = currentUser.getString(RATE).trim();
            }
            yourRateEdt.setText(charges);
            yourRateEdt.setSelection(charges.length());
        }
        yourRateEdt.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable mEdit) {
                if (mEdit.toString().length() > 0 && mEdit.toString().charAt(0) == '0') {
                    showToast("First letter must not be 0");
                    yourRateEdt.setText("");
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });

        edt_country = (EditText) root.findViewById(R.id.edt_country);
        if (!TextUtils.isNullOrEmpty(currentUser.getString(COUNTRY))) {
            edt_country.setText(currentUser.getString(COUNTRY));
            edt_country.setSelection(currentUser.getString(COUNTRY).length());
        }

        edt_city = (EditText) root.findViewById(R.id.edt_city);
        if (!TextUtils.isNullOrEmpty(currentUser.getString(CITY))) {
            edt_city.setText(currentUser.getString(CITY));
            edt_city.setSelection(currentUser.getString(CITY).length());
        }

        edt_pin_code = (EditText) root.findViewById(R.id.edt_pin_code);
        if (!TextUtils.isNullOrEmpty(currentUser.getString(ZIP_CODE))) {
            edt_pin_code.setText(currentUser.getString(ZIP_CODE));
            edt_pin_code.setSelection(currentUser.getString(ZIP_CODE).length());
        }

        descriptionEdt = (EditText) root.findViewById(R.id.descriptionEdt);
        if (!TextUtils.isNullOrEmpty(currentUser.getString(SKILLS))) {
            descriptionEdt.setText(currentUser.getString(SKILLS));
            descriptionEdt.setSelection(currentUser.getString(SKILLS).length());
        }

        edt_category = (EditText) root.findViewById(R.id.edt_category);
        edt_category.setOnClickListener(this);

        stringArrayList = currentUser.getList(PROFESSION);
        if (!TextUtils.isNullOrEmpty(stringArrayList)) {
            String s = "";
            for (int i = 0; i < stringArrayList.size(); i++) {
                s = s + stringArrayList.get(i) + ", ";
            }
            edt_category.setText(TextUtils.removeComma(s.trim()));
        }

        edt_paypal_id = (EditText) root.findViewById(R.id.edt_paypal_id);
        if (!TextUtils.isNullOrEmpty(currentUser.getString(PAYPAL_USER_ID))) {
            edt_paypal_id.setText(currentUser.getString(PAYPAL_USER_ID));
        }

        profileImage = (BezelImageView) root.findViewById(R.id.profileImage);
        profileImage.setOnClickListener(this);

        Log.d("CurrentUser",""+currentUser.getParseFile(PICTURE));

        if (currentUser.getParseFile(PICTURE) != null) {
            displayImage(getActivity(), profileImage, currentUser.getParseFile(PICTURE).getUrl());
            displayImage(getActivity(), iv_background, currentUser.getParseFile(PICTURE).getUrl());
        } else {
            iv_background.setImageResource(R.drawable.user_pic);
        }

        return root;
    }


    private void setPic() {
        boolean flag = false;
        if (Build.VERSION.SDK_INT >= PermissionsUtils.SDK_INT_MARSHMALLOW) {
            if (!PermissionsUtils.getInstance(getContext()).isPermissionGranted(getContext(), Manifest.permission.CAMERA, "Camera")) {
                return;
            }
        }
        if (Build.VERSION.SDK_INT >= PermissionsUtils.SDK_INT_MARSHMALLOW) {
            if (!PermissionsUtils.getInstance(getContext()).isPermissionGranted(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE, "Write External Storage")) {
                return;
            }
        }
        showImageDialog();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.edt_category:
                if (isInternetConnected()) {
                    startActivityForResult(new Intent(getActivity(), SelectCategoryActivity.class).putExtra(Constant.DATA, (Serializable) stringArrayList), 2222);
                } else {
                    showAlertDialog(getString(R.string.internet));
                }
                break;

            case R.id.DoneBtn:
                hideKeyBoard(v);
                if (isInternetConnected()) {
                    uiValidation();
                } else {
                    showAlertDialog(getString(R.string.internet));
                }
                break;

            case R.id.cancelBtn:
                hideKeyBoard(v);
                displayView(Constant.NEWS_FRAGMENT, false);
                break;

            case R.id.profileImage:
                setPic();
                break;

            default:
                break;
        }
    }

    private void uiValidation() {
        if (TextUtils.isNullOrEmpty(fullnameEdt.getText().toString())) {
            showAlertDialog("The name cannot be empty");
        } else if (!TextUtils.isValidMobile(contactNumberEdt.getText().toString())) {
            showAlertDialog("Please enter minimum 8 digit mobile number");
        } else if (TextUtils.isNullOrEmpty(edt_paypal_id.getText().toString())) {
            showAlertDialog("Please enter Paypal Login ID");
        } else if (TextUtils.isNullOrEmpty(yourRateEdt.getText().toString())) {
            showAlertDialog("Enter Rate");
        } else if (TextUtils.isNullOrEmpty(edt_country.getText().toString())) {
            showAlertDialog("Please enter the name of your country");
        } else if (TextUtils.isNullOrEmpty(edt_city.getText().toString())) {
            showAlertDialog("Please enter the name of your city");
        } else if (TextUtils.isNullOrEmpty(edt_pin_code.getText().toString())) {
            showAlertDialog("Please enter your zip code");
        } else if (TextUtils.isNullOrEmpty(descriptionEdt.getText().toString())) {
            showAlertDialog("Please enter your Skills");
        } else {
            editProfileTask();
        }
    }

    private void showImageDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(getString(R.string.add_image_from));
        builder.setItems(new String[]{getString(R.string.capture_image), getString(R.string.from_gallery)}, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (which == 0) {
                    Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    File f = AppUtils.createImageFile();
                    if ((f != null) && f.exists()) {
                        mImagePair = new ImageIntentHandler.ImagePair(profileImage, iv_background, f.getAbsolutePath());
                        //mImagePair = new ImageIntentHandler.ImagePair(iv_background, f.getAbsolutePath());
                        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
                        startActivityForResult(takePictureIntent, ImageIntentHandler.REQUEST_CAPTURE);
                    } else {
                        Toast.makeText(getActivity(), "Camera error", Toast.LENGTH_LONG).show();
                    }
                    dialog.dismiss();
                } else if (which == 1) {
                    mImagePair = new ImageIntentHandler.ImagePair(profileImage, iv_background, null);
                    //mImagePair = new ImageIntentHandler.ImagePair(iv_background, null);
                    Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(i, ImageIntentHandler.REQUEST_GALLERY);
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }
    public void Gallery() {
        if (Build.VERSION.SDK_INT < 19) {
            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
            intent.setType("image/*");
            startActivityForResult(intent, REQUEST_CODE_ALBUM);
        } else {
            Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(i, REQUEST_CODE_ALBUM);
        }
    }
    private Uri mImageCaptureUri;
    public String PicturePath = "";
    String strProfImg = "";

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        ImageIntentHandler intentHandler =
                new ImageIntentHandler(getActivity(), mImagePair)
                        .folder(getString(R.string.app_name))
                        .sizePx(300);
        intentHandler.handleIntent(requestCode, resultCode, data);

        // Check which request we're responding to
        if (requestCode == 2222) {
            // Make sure the request was successful
            if (resultCode == Activity.RESULT_OK) {
//                Log.d(getClass().getSimpleName(), data.getStringExtra(Constant.DATA));
                stringArrayList = (List<String>) data.getSerializableExtra(Constant.DATA);
                if (!TextUtils.isNullOrEmpty(stringArrayList)) {
                    String s = "";
                    for (int i = 0; i < stringArrayList.size(); i++) {
                        s = s + stringArrayList.get(i) + ", ";
                    }
                    edt_category.setText(TextUtils.removeComma(s.trim()));
                }
            }
        }
    }
private static final int REQUEST_CODE_CAMERA = 1;
    private static final int REQUEST_CODE_ALBUM = 2;
//    @Override
//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        if (resultCode == RESULT_OK) {
//
//
//            switch (requestCode) {
////                case REQUEST_CODE_CAMERA:
////                    try {
////                        ByteArrayProf = null;
////                        PicturePath = Environment.getExternalStorageDirectory() + "/Sportor.png";
////                        //   Bitmap bm = BitmapFactory.decodeFile(picturePath);
////                        if (!PicturePath.trim().isEmpty()) {
////
////                            ByteArray = null;
////                            Bitmap bm = BitmapFactory.decodeFile(PicturePath);
////                            bm = ConvetBitmap.Mytransform(bm);
////                            bm = Utility.rotateImage(bm, new File(PicturePath));
////                            ByteArrayOutputStream datasecond = new ByteArrayOutputStream();
////                            bm.compress(Bitmap.CompressFormat.JPEG, 100, datasecond);
////                            ByteArray = datasecond.toByteArray();
////
////
////                            if (picSelection == PROFILE_PIC) {
////                                strProfImg = base64String(datasecond.toByteArray());
////                                imgProfile.setImageBitmap(bm);
////                            } else if (picSelection == COVER_PIC) {
////                                strCoverImg = base64String(datasecond.toByteArray());
////                                imgCoverpic.setImageBitmap(bm);
////                            }
////
////
////                        }
////                    } catch (Exception e) {
////                        e.printStackTrace();
////                    }
////
////
////                    break;
//
//                case REQUEST_CODE_ALBUM:
//                    try {
//                        mImageCaptureUri = data.getData();
//                        // ((ImageView)findViewById(R.id.imvUser_Edit_profile)).setImageURI(mImageCaptureUri);
//
//                        String[] projection = {MediaStore.Images.Media.DATA};
//                        Cursor cursor = getContext().getContentResolver().query(mImageCaptureUri, projection, null, null, null);
//                        cursor.moveToFirst();
//                        PicturePath = cursor.getString(cursor.getColumnIndex(projection[0]));
//
//                        if (!PicturePath.trim().isEmpty()) {
//
//                            Bitmap bm = BitmapFactory.decodeFile(PicturePath);
//                            bm = ConvetBitmap.Mytransform(bm);
//                            bm = Methods.rotateImage(bm, new File(PicturePath));
//                            ByteArrayOutputStream datasecond = new ByteArrayOutputStream();
//                            bm.compress(Bitmap.CompressFormat.JPEG, 50, datasecond);
//
//                            profileImage.setImageBitmap(bm);
//
////                            if (picSelection == PROFILE_PIC) {
////                                ByteArrayProf = null;
//////                                ByteArrayProf = datasecond.toByteArray();
////                                strProfImg = base64String(datasecond.toByteArray());
////                                imgProfile.setImageBitmap(bm);
////                            }
////                            if (picSelection == COVER_PIC) {
////                                ByteArrayCover = null;
//////                                ByteArrayCover = datasecond.toByteArray();
////                                strCoverImg = base64String(datasecond.toByteArray());
//////                                imgCoverpic.setImageBitmap(bm);
//////                                BitmapDrawable ob = new BitmapDrawable(getResources(), bm);
////                                imgCoverpic.setImageBitmap(bm);
//////                                imgCoverpic.setBackgroundDrawable(ob);
////                            }
//
//
//                        }
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                    //  dialog.dismiss();
//                    break;
//
//
//                default:
//                    break;
//
//
//            }
//        }
//    }

    public void editProfileTask() {
        showLoading();

        final String imagePath = mImagePair == null ? null : mImagePair.imagePath;
        if (imagePath == null) {
            Debug.d(TAG, "Image was null.");
        } else {
            Debug.d(TAG, "Image Path " + imagePath);
        }

        fullName = fullnameEdt.getText().toString();
        contactNumber = contactNumberEdt.getText().toString();
        yourRate = yourRateEdt.getText().toString();
        country = edt_country.getText().toString();
        description = descriptionEdt.getText().toString();
        city = edt_city.getText().toString();
        zipCode = edt_pin_code.getText().toString();
        paypalId = edt_paypal_id.getText().toString();

        final ParseUser user = ParseUser.getCurrentUser();
        if (!TextUtils.isNullOrEmpty(fullName)) {
            user.put(NAME, fullName);
        }

        if (!TextUtils.isNullOrEmpty(contactNumber)) {
            user.put(CONTACT_NUMBER, contactNumber);
        }

        if (!TextUtils.isNullOrEmpty(yourRate)) {
            user.put(RATE, yourRate);
        }

        if (!TextUtils.isNullOrEmpty(country)) {
            user.put(COUNTRY, country);
        }

        if (!TextUtils.isNullOrEmpty(city)) {
            user.put(CITY, city);
        }

        if (!TextUtils.isNullOrEmpty(zipCode)) {
            user.put(ZIP_CODE, zipCode);
        }

        if (!TextUtils.isNullOrEmpty(description)) {
            user.put(SKILLS, description);
        }

        if (!TextUtils.isNullOrEmpty(stringArrayList)) {
            user.put(PROFESSION, stringArrayList);
        }

        if (!TextUtils.isNullOrEmpty(paypalId)) {
            user.put(PAYPAL_USER_ID, paypalId);
        }

        if (imagePath != null) {
            try {
                user.put(PICTURE, getParseFile(imagePath));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        try {
            ParseGeoPoint parseGeoPoint = new ParseGeoPoint(getMyLocation().latitude, getMyLocation().longitude);
            if (parseGeoPoint != null) {
                user.put(CURRENT_LOCATION, parseGeoPoint);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        user.put("deviceType", "Android");
        user.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                hideLoading();
                if (e == null) {
                    showAlertDialog("Profile successfully updated");
                    displayView(Constant.NEWS_FRAGMENT, false);
                }
            }
        });
    }

    private byte[] readInFile(String path) throws IOException {
        // TODO Auto-generated method stub
        byte[] data = null;
        File file = new File(path);
        InputStream input_stream = new BufferedInputStream(new FileInputStream(file));
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
        data = new byte[16384]; // 16K
        int bytes_read;
        while ((bytes_read = input_stream.read(data, 0, data.length)) != -1) {
            buffer.write(data, 0, bytes_read);
        }
        input_stream.close();
        return buffer.toByteArray();
    }

    private ParseFile getParseFile(String path) {
        ParseFile file = null;
        try {
            byte[] imageByte = readInFile(path);
            long time = System.currentTimeMillis();
            String sImageName = currentUser.getObjectId() + time + ".jpeg";
            // Create the ParseFile
            file = new ParseFile(sImageName, imageByte);
            // Upload the image into Parse Cloud
            file.saveInBackground();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return file;
    }
}
