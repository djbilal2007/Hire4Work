package com.hire4work.bean;

import com.parse.ParseClassName;
import com.parse.ParseObject;

/**
 * Created by Kailash Chouhan (Ratufa Technologies)
 * Email: kailash.chouhan@ratufa.com
 */
@ParseClassName("PushNotification")
public class PushNotification extends ParseObject {

    public PushNotification() {
    }

    public PushNotification(String theClassName) {
        super(theClassName);
    }

    // Use getString and others to access fields
    public String getWorkerId() {
        return getString("workerId");
    }

    // Use put to modify field values
    public void setWorkerId(String value) {
        put("workerId", value);
    }

    // Use getString and others to access fields
    public String getAlertMessage() {
        return getString("alertMessage");
    }

    // Use put to modify field values
    public void setAlertMessage(String value) {
        put("alertMessage", value);
    }

    // Use getString and others to access fields
    public String getType() {
        return getString("type");
    }

    // Use put to modify field values
    public void setType(String value) {
        put("type", value);
    }

    public String getJobId() {
        return getString("jobId");
    }

    public void setJobId(String value) {
        put("jobId", value);
    }

    public String geEmployerId() {
        return getString("employerId");
    }

    public void setEmployerId(String value) {
        put("employerId", value);
    }

    public String userType() {
        return getString("userType");
    }

    public void setuserType(String value) {
        put("userType", value);
    }
}
