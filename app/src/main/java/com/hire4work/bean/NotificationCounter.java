package com.hire4work.bean;

import com.parse.ParseClassName;
import com.parse.ParseObject;

import java.io.Serializable;

/**
 * Created by Kailash Chouhan (Ratufa Technologies)
 * Email: kailash.chouhan@ratufa.com
 */
@ParseClassName("NotificationCounter")
public class NotificationCounter extends ParseObject implements Serializable {


    public NotificationCounter() {
    }

    public NotificationCounter(String theClassName) {
        super(theClassName);
    }

    public String getSenderId() {
        return getString("senderId");
    }

    public void setSenderId(String content) {
        put("senderId", content);
    }

    public String getReceiverId() {
        return getString("receiverId");
    }

    public void setReceiverId(String content) {
        put("receiverId", content);
    }

    public String getJobId() {
        return getString("jobId");
    }

    public void setJobId(String content) {
        put("jobId", content);
    }

    public String getBedgeCount() {
        return getString("bedgeCount");
    }

    public void setBedgeCount(String content) {
        put("bedgeCount", content);
    }
}
