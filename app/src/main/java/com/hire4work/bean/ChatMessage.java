package com.hire4work.bean;

import com.parse.ParseClassName;
import com.parse.ParseObject;
import com.parse.ParseUser;

import java.text.Format;
import java.text.SimpleDateFormat;

@ParseClassName("Chat")
public class ChatMessage extends ParseObject {
    //    private boolean isImage, isMine;
    private String commentText, senderId, receiverId;

    public ChatMessage() {
    }

    public ChatMessage(String theClassName) {
        super(theClassName);
    }

    public ChatMessage(String commentText, String senderId, String receiverId) {
        this.commentText = commentText;
        this.senderId = senderId;
        this.receiverId = receiverId;
    }

//    public ChatMessage(String theClassName, String commentText, String senderId, String receiverId) {
//        super(theClassName);
//        this.commentText = commentText;
//        this.senderId = senderId;
//        this.receiverId = receiverId;
//    }

    public String getCommentText() {
        return getString("commentText");
    }

    public void setCommentText(String content) {
        put("commentText", content);
    }

    public ParseUser getSenderId() {
        return getParseUser("senderId");
    }

    public void setSenderId(ParseUser content) {
        put("senderId", content);
    }

    public ParseUser getReceiverId() {
        return getParseUser("receiverId");
    }

    public void setReceiverId(ParseUser content) {
        put("receiverId", content);
    }

    public String getJobId() {
        return getString("jobId");
    }

    public void setJobId(String content) {
        put("jobId", content);
    }

    public String getDateTime() {
        Format formatter = new SimpleDateFormat("dd-MMM-yyyy hh:mm");
        String s = formatter.format(getCreatedAt());
        return s;
    }
}
