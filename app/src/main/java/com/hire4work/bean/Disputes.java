package com.hire4work.bean;

import com.parse.ParseClassName;
import com.parse.ParseObject;

/**
 * Created by Kailash Chouhan (Ratufa Technologies)
 * Email: kailash.chouhan@ratufa.com
 */
@ParseClassName("Disputes")
public class Disputes extends ParseObject {

    public Disputes() {
    }

    public Disputes(String theClassName) {
        super(theClassName);
    }

    public String getEmployId() {
        return getString("employId");
    }

    public void setEmployId(String value) {
        put("employId", value);
    }

    public String getPostJobId() {
        return getString("postjobId");
    }

    public void setPostJobId(String value) {
        put("postjobId", value);
    }

    public String getWorkerId() {
        return getString("workerId");
    }

    public void setWorkerId(String value) {
        put("workerId", value);
    }

    public String getDisputeNote() {
        return getString("disputeNote");
    }

    public void setDisputeNote(String value) {
        put("disputeNote", value);
    }

}
