package com.hire4work.bean.appenum;

/**
 * Created by Kailash Chouhan (Ratufa Technologies)
 * Email: kailash.chouhan@ratufa.com
 */
public enum LoginType {
    LOGIN, LOGIN_AS_GUEST
}
