package com.hire4work.bean.appenum;

/**
 * Created by Kailash Chouhan (Ratufa Technologies)
 * Email: kailash.chouhan@ratufa.com
 */
public enum LoginWay {
    INITIAL_LOGIN, IN_APP_LOGIN
}
