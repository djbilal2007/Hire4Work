package com.hire4work.bean;

import com.parse.ParseClassName;
import com.parse.ParseObject;
import com.parse.ParseUser;

/**
 * Created by Kailash Chouhan (Ratufa Technologies)
 * Email: kailash.chouhan@ratufa.com
 */
@ParseClassName("UserRatting")
public class UserRatting extends ParseObject {

    public UserRatting() {
    }

    public UserRatting(String theClassName) {
        super(theClassName);
    }

    public ParseUser getuserId() {
        return getParseUser("userId");
    }

    public void setuserId(ParseUser value) {
        put("userId", value);
    }

    public String getWorkerId() {
        return getString("workerId");
    }

    public void setWorkerId(String value) {
        put("workerId", value);
    }

    public String getRatingDescription() {
        return getString("ratingDescription");
    }

    public void setRatingDescription(String value) {
        put("ratingDescription", value);
    }

    public Number getRatingPoint() {
        return getNumber("ratingPoint");
    }

    public void setRatingPoint(Number value) {
        put("ratingPoint", value);
    }

}
