package com.hire4work.bean;

import com.parse.ParseClassName;
import com.parse.ParseObject;

import java.io.Serializable;
import java.text.Format;
import java.text.SimpleDateFormat;

/**
 * Created by Kailash Chouhan (Ratufa Technologies)
 * Email: kailash.chouhan@ratufa.com
 */
@ParseClassName("News")
public class News extends ParseObject implements Serializable {

    public News() {
    }

    public News(String theClassName) {
        super(theClassName);
    }

    public String getobjId() {
        return getObjectId();
    }

    // Use getString and others to access fields
    public String getnewscontent() {
        return getString("newscontent");
    }

    // Use put to modify field values
    public void setnewscontent(String value) {
        put("newscontent", value);
    }

    public String getDate() {
        Format formatter = new SimpleDateFormat("dd MMM yyyy");
        String s = formatter.format(getCreatedAt());
        return s;
    }
}
