package com.hire4work.bean;

import com.parse.ParseClassName;
import com.parse.ParseObject;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Kailash Chouhan (Ratufa Technologies)
 * Email: kailash.chouhan@ratufa.com
 */
@ParseClassName("FundTransection")
public class FundTransection extends ParseObject implements Serializable {

    public FundTransection() {
    }

    public FundTransection(String theClassName) {
        super(theClassName);
    }

    public String getEmployerId() {
        return getString("employerId");
    }

    public void setEmployerId(String value) {
        put("employerId", value);
    }

    public String getJobId() {
        return getString("jobId");
    }

    public void setJobId(String value) {
        put("jobId", value);
    }

    public String getWorkerId() {
        return getString("workerId");
    }

    public void setWorkerId(String value) {
        put("workerId", value);
    }

    public String getWorkerPaypal() {
        return getString("workerPaypal");
    }

    public void setWorkerPaypal(String value) {
        put("workerPaypal", value);
    }

    public String getEmployerPaypal() {
        return getString("employerPaypal");
    }

    public void setEmployerPaypal(String value) {
        put("employerPaypal", value);
    }

    public String getTotalAmount() {
        return getString("totalAmount");
    }

    public void setTotalAmount(String value) {
        put("totalAmount", value);
    }

    public String getPerHourCharges() {
        return getString("perHourCharges");
    }

    public void setPerHourCharges(String value) {
        put("perHourCharges", value);
    }

    public String getStatus() {
        return getString("status");
    }

    public void setStatus(String value) {
        put("status", value);
    }

    public String getDuration() {
        return getString("duration");
    }

    public void setDuration(String value) {
        put("duration", value);
    }

    public Date getStartDate() {
        return getDate("startDate");
    }

    public void setStartDate(Date value) {
        put("startDate", value);
    }

    public Date getEndDate() {
        return getDate("endDate");
    }

    public void setEndDate(Date value) {
        put("endDate", value);
    }

    public Date getStopTime() {
        return getDate("stopTime");
    }

    public void setStopTime(Date value) {
        put("stopTime", value);
    }

    public Date getStopDate() {
        return getDate("stopDate");
    }

    public void setStopDate(Date value) {
        put("stopDate", value);
    }

    public Boolean isPlay() {
        return getBoolean("isPlay");
    }

    public void isPlay(Boolean value) {
        put("isPlay", value);
    }

    public Boolean isPause() {
        return getBoolean("isPause");
    }

    public void isPause(Boolean value) {
        put("isPause", value);
    }

    public Boolean isStop() {
        return getBoolean("isStop");
    }

    public void isStop(Boolean value) {
        put("isStop", value);
    }
}
