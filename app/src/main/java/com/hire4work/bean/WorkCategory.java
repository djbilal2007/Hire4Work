package com.hire4work.bean;

import com.parse.ParseClassName;
import com.parse.ParseObject;

/**
 * Created by Kailash Chouhan (Ratufa Technologies)
 * Email: kailash.chouhan@ratufa.com
 */
@ParseClassName("WorkCategory")
public class WorkCategory extends ParseObject {

    private boolean selected;

    public WorkCategory() {
    }

    public WorkCategory(String theClassName) {
        super(theClassName);
    }

    public String getobjId() {
        return getObjectId();
    }

    // Use getString and others to access fields
    public String getjobCategory() {
        return getString("jobCategory");
    }

    // Use put to modify field values
    public void setjobCategory(String value) {
        put("jobCategory", value);
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}
