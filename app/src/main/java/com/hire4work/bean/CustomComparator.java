package com.hire4work.bean;

import java.util.Comparator;

/**
 * Created by Kailash Chouhan (Ratufa Technologies)
 * Email: kailash.chouhan@ratufa.com
 */
public class CustomComparator implements Comparator<JobApply> {
    @Override
    public int compare(JobApply o1, JobApply o2) {
        return o1.getPostUserId().getObjectId().compareTo(o2.getPostUserId().getObjectId());
    }
}
