package com.hire4work.bean;

import com.parse.ParseClassName;
import com.parse.ParseObject;
import com.parse.ParseUser;

import java.io.Serializable;

/**
 * Created by Kailash Chouhan (Ratufa Technologies)
 * Email: kailash.chouhan@ratufa.com
 */
@ParseClassName("JobApply")
public class JobApply extends ParseObject implements Serializable {

    public JobApply() {
    }

    public JobApply(String theClassName) {
        super(theClassName);
    }

    public String getobjId() {
        return getObjectId();
    }

    public ParseUser getApplyUserId() {
        return getParseUser("applyUserId");
    }

    public void setApplyUserId(ParseUser value) {
        put("applyUserId", value);
    }

    public ParseObject getPostJobId() {
        return getParseObject("postjobId");
    }

    public void setPostJobId(ParseObject value) {
        put("postjobId", value);
    }

    public ParseUser getPostUserId() {
        return getParseUser("postUserId");
    }

    public void setPostUserId(ParseUser value) {
        put("postUserId", value);
    }

    public String getJobAppliedStatus() {
        return getString("jobAppliedStatus");
    }

    public void setJobAppliedStatus(String value) {
        put("jobAppliedStatus", value);
    }

    public String getAssignedUserStatus() {
        return getString("assignedUserStatus");
    }

    public void setAssignedUserStatus(String value) {
        put("assignedUserStatus", value);
    }

    public String getWorkRate() {
        return getString("workRate");
    }

    public void setWorkRate(String value) {
        put("workRate", value);
    }
}
