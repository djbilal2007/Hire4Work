package com.hire4work.bean;

import com.parse.ParseClassName;
import com.parse.ParseFile;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseUser;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Kailash Chouhan (Ratufa Technologies)
 * Email: kailash.chouhan@ratufa.com
 */
@ParseClassName("PostJob")
public class PostJob extends ParseObject implements Serializable {

    public PostJob() {
    }

    public PostJob(String theClassName) {
        super(theClassName);
    }

    public ParseUser getuserId() {
        return getParseUser("userId");
    }

    public void setuserId(ParseUser value) {
        put("userId", value);
    }

    public String getjobPrice() {
        return getString("jobPrice");
    }

    public void setjobPrice(String value) {
        put("jobPrice", value);
    }

    public String getjobTitle() {
        return getString("jobTitle");
    }

    public void setjobTitle(String value) {
        put("jobTitle", value);
    }

    public String getjobAddress() {
        return getString("jobAddress");
    }

    public void setjobAddress(String value) {
        put("jobAddress", value);
    }

    public String getjobCategory() {
        return getString("jobCategory");
    }

    public void setjobCategory(String value) {
        put("jobCategory", value);
    }

    public String getjobDescription() {
        return getString("jobDescription");
    }

    public void setjobDescription(String value) {
        put("jobDescription", value);
    }

    public String getImage() {
        if (getParseFile("imageOrFile") != null) {
            return getParseFile("imageOrFile").getUrl();
        } else {
            return null;
        }
    }

    // Use put to modify field values
    public void setImage(ParseFile value) {
        put("imageOrFile", value);
    }

    public String getjobAssignStatus() {
        return getString("jobAssignStatus");
    }

    public void setjobAssignStatus(String value) {
        put("jobAssignStatus", value);
    }

    public ParseGeoPoint getjobLatLong() {
        return getParseGeoPoint("jobLatLong");
    }

    public void setjobLatLong(ParseGeoPoint value) {
        put("jobLatLong", value);
    }

    public int getviewsCount() {
        return getInt("viewsCount");
    }

    public void setviewsCount(int value) {
        put("viewsCount", value);
    }

    public String getjobDone() {
        return getString("jobDone");
    }

    public void setjobDone(String value) {
        put("jobDone", value);
    }

    public String getLiveJobStatus() {
        return getString("liveJobStatus");
    }

    public void setLiveJobStatus(String value) {
        put("liveJobStatus", value);
    }

    public List<ParseUser> getAppliedUser() {
        return getList("appliedUser");
    }

    public void setAppliedUser(List<ParseUser> value) {
        put("appliedUser", value);
    }

    public List<String> getAppliedUserName() {
        return getList("appliedUserName");
    }

    public void setAppliedUserName(List<String> value) {
        put("appliedUserName", value);
    }

    public String getWorkerId() {
        return getString("workerId");
    }

    public void setWorkerId(String value) {
        put("workerId", value);
    }

    public String getRated() {
        return getString("rated");
    }

    public void setRated(String value) {
        put("rated", value);
    }
}
