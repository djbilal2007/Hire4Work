package com.hire4work.bean;

import com.parse.ParseClassName;
import com.parse.ParseObject;
import com.parse.ParseUser;

import java.io.Serializable;

/**
 * Created by Kailash Chouhan (Ratufa Technologies)
 * Email: kailash.chouhan@ratufa.com
 */
@ParseClassName("InvitesNotification")
public class InvitesNotification extends ParseObject implements Serializable {

    public InvitesNotification() {
    }

    public InvitesNotification(String theClassName) {
        super(theClassName);
    }

    public String getobjId() {
        return getObjectId();
    }

    public ParseUser getSenderUserId() {
        return getParseUser("senderUserId");
    }

    public void setSenderUserId(ParseUser value) {
        put("senderUserId", value);
    }

    public String getRecieverUserId() {
        return getString("recieverUserId");
    }

    public void setRecieverUserId(String value) {
        put("recieverUserId", value);
    }

    public String getNotificationMessage() {
        return getString("notificationMessage");
    }

    public void setNotificationMessage(String value) {
        put("notificationMessage", value);
    }

    public ParseObject getJobId() {
        return getParseObject("jobId");
    }

    public void setJobId(ParseObject value) {
        put("jobId", value);
    }

    public String getJobCategory() {
        return getString("jobCategory");
    }

    public void setJobCategory(String value) {
        put("jobCategory", value);
    }

    public String getIsStatus() {
        return getString("isStatus");
    }

    public void setIsStatus(String value) {
        put("isStatus", value);
    }
}
