package com.hire4work.bean;

import com.parse.ParseClassName;
import com.parse.ParseFile;
import com.parse.ParseGeoPoint;
import com.parse.ParseUser;

import java.io.Serializable;
import java.util.List;

import static com.hire4work.utills.UserConstant.ADDRESS;
import static com.hire4work.utills.UserConstant.CITY;
import static com.hire4work.utills.UserConstant.CONTACT_NUMBER;
import static com.hire4work.utills.UserConstant.COUNTRY;
import static com.hire4work.utills.UserConstant.CURRENT_LOCATION;
import static com.hire4work.utills.UserConstant.EMAIL;
import static com.hire4work.utills.UserConstant.NAME;
import static com.hire4work.utills.UserConstant.PICTURE;
import static com.hire4work.utills.UserConstant.PROFESSION;
import static com.hire4work.utills.UserConstant.RATE;
import static com.hire4work.utills.UserConstant.SKILLS;

/**
 * Created by Kailash Chouhan (Ratufa Technologies)
 * Email: kailash.chouhan@ratufa.com
 */
@ParseClassName("_User")
public class PersonData extends ParseUser implements Serializable {

    private boolean selected;

    public PersonData() {
    }

    public String getobjId() {
        return getObjectId();
    }

    public String getUName() {
        return getUsername();
    }

    public void setUserName(String value) {
        put("username", value);
    }

    public String getEmail() {
        return getString(EMAIL);
    }

    public void setEmail(String value) {
        put(EMAIL, value);
    }

    public String getDeviceType() {
        return getString("deviceType");
    }

    public void setDeviceType(String value) {
        put("deviceType", value);
    }

    public String getName() {
        return getString(NAME);
    }

    public void setName(String value) {
        put(NAME, value);
    }

    public String getAddress() {
        return getString(ADDRESS);
    }

    public void setAddress(String value) {
        put(ADDRESS, value);
    }

    public String getContactNumber() {
        return getString(CONTACT_NUMBER);
    }

    public void setContactNumber(String value) {
        put(CONTACT_NUMBER, value);
    }

    public String getImage() {
        String sImage = "";
        if (getParseFile(PICTURE) != null) {
            sImage = getParseFile(PICTURE).getUrl();
        }
        return sImage;
    }

    public void setImage(ParseFile value) {
        put(PICTURE, value);
    }

    public String getRate() {
        return getString(RATE);
    }

    public void setRate(String value) {
        put(RATE, value);
    }

    public List<String> getProffesion() {
        return getList(PROFESSION);
    }

    public void setProffesion(List<String> value) {
        put(PROFESSION, value);
    }

    public String getSkills() {
        return getString(SKILLS);
    }

    public void setSkills(String value) {
        put(SKILLS, value);
    }

    public String getCity() {
        return getString(CITY);
    }

    public void setCity(String value) {
        put(CITY, value);
    }

    public String getCountry() {
        return getString(COUNTRY);
    }

    public void setCountry(String value) {
        put(COUNTRY, value);
    }

    public ParseGeoPoint getCurrentLocation() {
        return getParseGeoPoint(CURRENT_LOCATION);
    }

    public void setCurrentLocation(ParseGeoPoint value) {
        put(CURRENT_LOCATION, value);
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}
