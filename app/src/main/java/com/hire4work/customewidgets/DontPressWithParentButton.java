package com.hire4work.customewidgets;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageButton;

/**
 * Created by Kailash Chouhan (Ratufa Technologies)
 * Email: kailash.chouhan@ratufa.com
 */
public class DontPressWithParentButton extends ImageButton {

    public DontPressWithParentButton(Context context) {
        super(context);
    }

    public DontPressWithParentButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public DontPressWithParentButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public void setPressed(boolean pressed) {
        if (pressed && getParent() instanceof View && ((View) getParent()).isPressed()) {
            return;
        }
        super.setPressed(pressed);
    }
}
