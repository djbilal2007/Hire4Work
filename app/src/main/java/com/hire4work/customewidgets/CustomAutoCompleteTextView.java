package com.hire4work.customewidgets;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.AutoCompleteTextView;

import com.hire4work.bean.PlacesSuggestionsBean;

/**
 * Created by Kailash Chouhan (Ratufa Technologies)
 * Email: kailash.chouhan@ratufa.com
 */
public class CustomAutoCompleteTextView extends AutoCompleteTextView {

    public CustomAutoCompleteTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected CharSequence convertSelectionToString(Object selectedItem) {
        /** Each item in the autocompetetextview suggestion list is a hashmap object */
//        HashMap<String, String> hm = (HashMap<String, String>) selectedItem;
        PlacesSuggestionsBean hm = (PlacesSuggestionsBean) selectedItem;
        return hm.getDescription();
    }
}