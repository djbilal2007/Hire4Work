package com.hire4work.notification;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.NotificationCompat;

import com.hire4work.R;
import com.hire4work.activity.HomeActivity;
import com.hire4work.utills.Constant;
import com.hire4work.utills.Debug;
import com.parse.ParsePushBroadcastReceiver;

import org.json.JSONException;
import org.json.JSONObject;

public class ParsePushReceiver extends ParsePushBroadcastReceiver {

    private final String TAG = ParsePushReceiver.class.getSimpleName();

    private NotificationUtils notificationUtils;

    private Intent parseIntent;

    public ParsePushReceiver() {
        super();
    }

    @Override
    protected void onPushReceive(Context context, Intent intent) {
        super.onPushReceive(context, intent);

        if (intent == null)
            return;

        try {
            JSONObject json = new JSONObject(intent.getExtras().getString("com.parse.Data"));

            Debug.e(TAG, "Push received: " + json);

            parseIntent = intent;

            parsePushJson(context, json);

        } catch (JSONException e) {
            Debug.e(TAG, "Push message json exception: " + e.getMessage());
        }
    }

    @Override
    protected void onPushDismiss(Context context, Intent intent) {
        super.onPushDismiss(context, intent);
    }

    @Override
    protected void onPushOpen(Context context, Intent intent) {
        super.onPushOpen(context, intent);
    }

    /**
     * Parses the push notification json
     *
     * @param context
     * @param json
     */
    private void parsePushJson(Context context, JSONObject json) {
        try {
            // {"push_hash":"f343a62c15d08121279aa545a23312e2","jobId":"W7R4Nc7Skp","alert":"Alia Bhatt paused work timer",
            // "name":"Alia Bhatt","userId":"p0Xu5CT8Jy","badge":"Increment","type":"pauseTimerRequest","workerId":"Rqqul6SAMx"}

            //{"push_hash":"5f97d6aacb8407588e80d604418a835c","aps":{"alert":"Ratufa Dev resumed work timer on job - Android App test","badge":"0"},"jobId":"NGfaj30fJV","type":"Resume Timer Request","workerId":"YAXVJgffex","userId":"cNTYp1yYpp"}

            String title = json.getString("type");
            String message = json.getJSONObject("aps").getString("alert");

            Intent intent = new Intent(Constant.NOTIFICATION);
            // You can also include some extra data.
            intent.putExtra(Constant.DATA, "" + json.toString());
            LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
            generateNotification(context, "" + title, context.getString(R.string.app_name), message);
            // if (!isBackground) {
//            Intent resultIntent = new Intent(context, HomeActivity.class);
//            showNotificationMessage(context, title, message, resultIntent);
            // }

        } catch (JSONException e) {
            Debug.e(TAG, "Push message json exception: " + e.getMessage());
        }
    }


    /**
     * Shows the notification message in the notification bar
     * If the app is in background, launches the app
     *
     * @param context
     * @param title
     * @param message
     * @param intent
     */
    private void showNotificationMessage(Context context, String title, String message, Intent intent) {

        notificationUtils = new NotificationUtils(context);

        intent.putExtras(parseIntent.getExtras());

        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        notificationUtils.showNotificationMessage(title, message, intent);
    }

    /**
     * Issues a notification to inform the user that server has sent a message.
     */
    private static void generateNotification(Context context, String ntype, String title, String message) {

        int icon = R.drawable.ic_launcher;
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        Notification notification = new NotificationCompat.Builder(context).setContentTitle(title).setContentText(message).setSmallIcon(icon).setStyle(new NotificationCompat.BigTextStyle().setBigContentTitle(title).bigText(message)).build();

        int id = 3584;

        Intent notificationIntent = new Intent();

        notificationIntent = new Intent(context, HomeActivity.class);
        // set intent so it does not start a new activity
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        notificationIntent.putExtra("ntype", "" + ntype);
        PendingIntent intent = PendingIntent.getActivity(context, 0, notificationIntent, 0);
        // notification.setLatestEventInfo(context, title, message, intent);
        notification.defaults |= Notification.DEFAULT_SOUND;
        notification.flags |= Notification.FLAG_AUTO_CANCEL;
        notification.contentIntent = intent;

        notificationManager.notify(id, notification);

    }
}