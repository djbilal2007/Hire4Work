package com.hire4work.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.hire4work.R;
import com.hire4work.bean.ChatMessage;
import com.hire4work.customewidgets.BezelImageView;
import com.hire4work.customewidgets.TextDrawable;
import com.parse.ParseUser;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import static com.hire4work.utills.UserConstant.NAME;
import static com.hire4work.utills.UserConstant.PICTURE;

/**
 * Created by Kailash Chouhan (Ratufa Technologies)
 * Email: kailash.chouhan@ratufa.com
 */
public class ChatMessageAdapter extends ArrayAdapter<ChatMessage> {
    private final int MY_MESSAGE = 0, OTHER_MESSAGE = 1;
    //    private String worker_name, worker_image;
    private Context mContext;
    ParseUser worker;
    private TextDrawable.IBuilder mDrawableBuilder;

    public ChatMessageAdapter(Context context, ParseUser workerUser, ArrayList<ChatMessage> data) {
        super(context, 0, data);
        this.mContext = context;
        this.worker = workerUser;
//        this.worker_image = sWorker_image;

        mDrawableBuilder = TextDrawable.builder().beginConfig()
                .withBorder(4)
                .endConfig()
                .round();
    }

    @Override
    public int getViewTypeCount() {
        // my message, other message
        return 2;
    }

    @Override
    public int getItemViewType(int position) {
        ChatMessage item = getItem(position);
        if (item.getSenderId().getObjectId().equals(ParseUser.getCurrentUser().getObjectId()))
            return MY_MESSAGE;
        else return OTHER_MESSAGE;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        int viewType = getItemViewType(position);
        if (viewType == MY_MESSAGE) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_mine_message, parent, false);

            TextView textView = (TextView) convertView.findViewById(R.id.text);
            textView.setText(getItem(position).getCommentText());

            TextView tv_date_time = (TextView) convertView.findViewById(R.id.tv_date_time);
            // DateUtils.getRelativeTimeSpanString(data.get(position).timestamp, new Date().getTime(), 0, DateUtils.FORMAT_ABBREV_RELATIVE)
            tv_date_time.setText("" + getItem(position).getDateTime());

            TextView tv_uname = (TextView) convertView.findViewById(R.id.tv_uname);
            tv_uname.setText(ParseUser.getCurrentUser().getString(NAME));

            BezelImageView iv_user_image = (BezelImageView) convertView.findViewById(R.id.iv_user_image);
            TextDrawable drawable = mDrawableBuilder.build(String.valueOf(ParseUser.getCurrentUser().getString(NAME).charAt(0)), mContext.getResources().getColor(R.color.header_color));
            if (ParseUser.getCurrentUser().getParseFile(PICTURE) != null) {
                Picasso.with(mContext).load(ParseUser.getCurrentUser().getParseFile(PICTURE).getUrl()).placeholder(drawable).error(drawable).fit().into(iv_user_image);
            } else {
                iv_user_image.setImageDrawable(drawable);
            }

        } else if (viewType == OTHER_MESSAGE) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_other_message, parent, false);

            TextView textView = (TextView) convertView.findViewById(R.id.text);
            textView.setText(getItem(position).getCommentText());

            TextView tv_date_time = (TextView) convertView.findViewById(R.id.tv_date_time);
            tv_date_time.setText("" + getItem(position).getDateTime());

            TextView tv_uname = (TextView) convertView.findViewById(R.id.tv_uname);
            tv_uname.setText(worker.getString(NAME));

            BezelImageView iv_user_image = (BezelImageView) convertView.findViewById(R.id.iv_user_image);
            TextDrawable drawable = mDrawableBuilder.build(String.valueOf(worker.getString(NAME).charAt(0)), mContext.getResources().getColor(R.color.header_color));
            if (worker.getParseFile(PICTURE) != null) {
                Picasso.with(mContext).load(worker.getParseFile(PICTURE).getUrl()).placeholder(drawable).error(drawable).fit().into(iv_user_image);
            } else {
                iv_user_image.setImageDrawable(drawable);
            }
        }

        return convertView;
    }
}
