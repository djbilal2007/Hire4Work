package com.hire4work.adapter;

/**
 * Created by Kailash Chouhan (Ratufa Technologies)
 * Email: kailash.chouhan@ratufa.com
 */

import android.annotation.SuppressLint;
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.hire4work.R;
import com.hire4work.bean.WorkCategory;

import java.util.List;

public class SelectCategoryAdapter extends ArrayAdapter<WorkCategory> {

    private List<WorkCategory> list;
    private LayoutInflater inflator;

    public SelectCategoryAdapter(Activity context, List<WorkCategory> list) {
        super(context, R.layout.item_select_category, list);
        this.list = list;
        inflator = context.getLayoutInflater();
    }

    @SuppressLint("InflateParams")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder;
        if (convertView == null) {
            convertView = inflator.inflate(R.layout.item_select_category, null);
            holder = new ViewHolder();
            holder.title = (TextView) convertView.findViewById(R.id.title);
            holder.chk = (CheckBox) convertView.findViewById(R.id.checkbox);
            holder.chk.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton view, boolean isChecked) {
                    int getPosition = (Integer) view.getTag();
                    list.get(getPosition).setSelected(view.isChecked());
                }
            });
            convertView.setTag(holder);
            convertView.setTag(R.id.title, holder.title);
            convertView.setTag(R.id.checkbox, holder.chk);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.chk.setTag(position);

        holder.title.setText(list.get(position).getjobCategory());
        holder.chk.setChecked(list.get(position).isSelected());

        return convertView;
    }

    static class ViewHolder {
        protected TextView title;
        protected CheckBox chk;
    }
}
