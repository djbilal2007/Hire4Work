package com.hire4work.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.hire4work.R;
import com.hire4work.bean.WorkCategory;

import java.util.List;

/**
 * Created by Kailash Chouhan (Ratufa Technologies)
 * Email: kailash.chouhan@ratufa.com
 */
public class WorkCategoryAdapter extends ArrayAdapter<WorkCategory> {

    List<WorkCategory> categoryList;
    Context context;

    public WorkCategoryAdapter(Context ctx, int txtViewResourceId, List<WorkCategory> objects) {
        super(ctx, txtViewResourceId, objects);
        this.categoryList = objects;
        this.context = ctx;
    }

    @Override
    public View getDropDownView(int position, View cnvtView, ViewGroup prnt) {
        return getCustomView(position, cnvtView, prnt);
    }

    @Override
    public View getView(int pos, View cnvtView, ViewGroup prnt) {
        return getCustomView(pos, cnvtView, prnt);
    }

    public View getCustomView(int position, View convertView, ViewGroup parent) {
        WorkCategory workCategory = getItem(position);

        LayoutInflater inflater = LayoutInflater.from(getContext());
        View mySpinner = inflater.inflate(R.layout.spinner_text, parent, false);
        TextView main_text = (TextView) mySpinner.findViewById(R.id.spinner);
        main_text.setText(workCategory.getjobCategory());

        return mySpinner;
    }
}
